/**  
 * @file serdes_dss.js
 *
 * @brief 
 *  Java Script file for DSS synchronization of DIAGNOSTIC SERDES test.

    Example DSS script which runs a K2E SERDES TX FIR settings sweep with PRBS scan
    on two devices. 
    
    Example assumes a board scenario where two devices Lane0 and Lane1 Serdes lanes
    are hooked up to each other as in the below diagram:
    
    Device0 TX0 ----> Device1 RX0
    Device0 TX1 ----> Device1 RX1

    Device1 TX0 ----> Device0 RX0
    Device1 TX1 ----> Device0 RX1
    
    Each TX -> RX channel is assumed to be electrical backplane or backplane + cable. 
    No active, intermediary, devices (switches, or re-timers) are assumed in the channel. 
    
    DSS script loads binaries onto two SoC. Each binary is then synchronized at key
    steps such that the RX error count of a given TX setting set can be measured. 

    Run on a dual K2E platform being controlled by a single CCS emulation session
    Tested out on the 6630K2E dual VDB setup

Prerequisite: serdes_dss_prbs_k2e.bat invokes this .js file. Please use serdes_dss_prbs_k2e.bat to run PRBS test
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

importPackage(Packages.com.ti.debug.engine.scripting)
importPackage(Packages.com.ti.ccstudio.scripting.environment)
importPackage(Packages.java.lang)
importPackage(java.awt)
importPackage(java.io)
importPackage(java.net)
importPackage(java.util)

/* Major variables that will need to be modified to match a given CCS/DSS environment on the host PC */
/* serdes_dss_prbs_k2e.bat will set the %PDK_INSTALL_PATH% environment variable */ 
var CFG_LOADER_BASE   = java.lang.System.getenv("PDK_INSTALL_PATH");
var pathLog       = CFG_LOADER_BASE + "/ti/diag/serdes_diag/serdes_log_path.xml";
var pathXSL       = CFG_LOADER_BASE + "/ti/diag/serdes_diag/serdes_xsl_path.xsl";
/* Point to the Dual Board Target Configuration File that  you have created.
   Instructions on how to create Dual Board Target Config are shown in Section 9 of serdes_diag\docs\Serdes_Diag_User_Guide.pdf*/
var pathCCXML         = "C:/K2E_EVM2EVM.ccxml";
/* Point to the executables to run on Board 0 and Board 1 respectively */
var pathDSP0          = CFG_LOADER_BASE + "/MyExampleProjects/serdes_diag_prbs_K2EC66ExampleProject/Debug/serdes_diag_prbs_K2EC66ExampleProject.out";
var pathDSP1          = CFG_LOADER_BASE + "/MyExampleProjects/serdes_diag_prbs_K2EC66ExampleProject/Debug/serdes_diag_prbs_K2EC66ExampleProject.out";



/* DSP core synchronization variables */
var dss_lock_key        = 0xDEADBEEF;
var dss_unlock_key  = 0xBEEFFACE;

var dss_stall_flag_set      = 0xCAFECAFE;
var dss_stall_flag_clear    = 0xCAFEDEAD;
var dss_stall_flag_device0  = 0xCAFEDEAD; /* assume an unstalled device */
var dss_stall_flag_device1  = 0xCAFEDEAD; /*assume an unstalled device */

var dss_test_complete_flag_set      = 0xA5A5A5A5;

var dss_test_complete_flag_device0  = 0xDEADBEEF;   //assume an unfinished test cycle */
var dss_test_complete_flag_device1  = 0xDEADBEEF;   //assume an unfinished test cycle */

/* CCSv7 naming convention */
var sessionName0        = "Texas Instruments XDS2xx USB Onboard Debug Probe_0/C66xx_0";
var sessionName1        = "Texas Instruments XDS2xx USB Onboard Debug Probe_1/C66xx_0";

/* For CCSv5, note that the emulator naming convention will be different */
/* var sessionName0        = "Texas Instruments XDS2xx USB Onboard Emulator_0/C66xx_0"; */
/* var sessionName1        = "Texas Instruments XDS2xx USB Onboard Emulator_1/C66xx_0"; */


function MemRead(activeDS, addr, wordCnt)
{
    var result;
    var successful = 0;

   if (!activeDS.target.isHalted()){
        /* This command halts the target in order to allow memory reads */
       activeDS.target.halt();
   }
    
    while (! successful) {
       try {
         result = activeDS.memory.readData(0, addr, 32, wordCnt);
         successful = 1;
       }
       catch(err)
       {
         print ("Memory read failed! Sleep and retry.\n");
         java.lang.Thread.sleep(1000);
       }
    }
    
    activeDS.target.runAsynch();
    return result;
}


function MemWrite(activeDS, dst, src, wordCnt)
{
    var successful = 0;

    if (!activeDS.target.isHalted()){
        /* This command halts the target in order to allow memory writes */
        activeDS.target.halt();
    }
    
    while (! successful) {
       try {
         activeDS.memory.writeData(0, dst, src, wordCnt);
         successful = 1;
       }
       catch(err)
       {
         print ("Memory write failed! Sleep and retry.n");
         java.lang.Thread.sleep(1000);
       }
    }
    activeDS.target.runAsynch();
}



function Synchronized_PRBS_Test(script)
{
    script.traceWrite("DSS: Serdes PRBS testing...");
    
    /* Setup emulation session, load .out files and start execution of each DSP */
    script.traceWrite("DSS: Loading DSP program to 1st Keystone2 DSP core...");
    session0.memory.loadProgram(pathDSP0);
    script.traceWrite("DSS: Done!");

    script.traceWrite("DSS: Loading DSP program to 2nd Keystone2 DSP core...");
    session1.memory.loadProgram(pathDSP1);
    script.traceWrite("DSS: Done!");
    
    
    var dss_test_complete_flag_addr     = session1.symbol.getAddress("serdes_diag_dss_test_complete_flag");

    var dss_stall_flag3_addr_device0        = session0.symbol.getAddress("serdes_diag_dss_stall_flag3");    
    var dss_stall_flag3_addr_device1        = session1.symbol.getAddress("serdes_diag_dss_stall_flag3");

    var dss_stall_flag1_addr            = session1.symbol.getAddress("serdes_diag_dss_stall_flag1");
    var dss_stall_flag2_addr            = session1.symbol.getAddress("serdes_diag_dss_stall_flag2");
    var dss_stall_flag3_addr            = session1.symbol.getAddress("serdes_diag_dss_stall_flag3");    

    var dss_device_num_0_addr           = session1.symbol.getAddress("diag_serdes_device_num");
    var dss_device_num_1_addr           = session1.symbol.getAddress("diag_serdes_device_num");

    session0.memory.writeWord(0,dss_device_num_0_addr, 0);
    session1.memory.writeWord(0,dss_device_num_1_addr, 1);
    
    session0.breakpoint.removeAll();
    script.traceWrite("DSS: Removed all breakpoints on 1st Keystone2 DSPs...");

    session1.breakpoint.removeAll();
    script.traceWrite("DSS: Removed all breakpoints on 2nd Keystone2 DSPs...");
        
    script.traceWrite("DSS: Executing program on 2nd Keystone2 DSPs...");
    session1.target.runAsynch();
    
    script.traceWrite("DSS: Executing program on 1st Keystone2 DSPs...");
    session0.target.runAsynch();
    
    script.traceWrite("DSS: Execution started...");

    java.lang.Thread.sleep(1000);
    
    /*
    Start polling for the dss_stall_flag_set from both devices
    Start polling for the dss_test_complete_flag_set from both devices
        
    If test is complete, we are done with test loop
    If stall is detected on both devices, devices are synchronized and should be unlocked to run

    */
    
    
    while( (dss_test_complete_flag_device0 != dss_test_complete_flag_set) && (dss_test_complete_flag_device1 != dss_test_complete_flag_set) ){
    
        /* poll for the stall flag0 being set on both devices */
        dss_stall_flag3_device0 = session0.memory.readWord(0,dss_stall_flag3_addr_device0);
        dss_stall_flag3_device1 = session1.memory.readWord(0,dss_stall_flag3_addr_device1);

        if( (dss_stall_flag3_device0 == dss_stall_flag_set) && (dss_stall_flag3_device1 == dss_stall_flag_set) ){ /*stalled at flag3 */
            /* set the unlock key0 on both devices since they are both stalled waiting for the DSS to unlock */
            java.lang.Thread.sleep(1000);

            server.simultaneous.halt(); /* Halt all CPUs */
            
            session0.memory.writeWord(0,dss_stall_key0_addr, dss_unlock_key);
            session1.memory.writeWord(0,dss_stall_key0_addr, dss_unlock_key);
            
            dss_stall_flag_device0  = dss_stall_flag_clear; /*clear flag for next iteration */
            dss_stall_flag_device1  = dss_stall_flag_clear; /*clear flag for next iteration */

            server.simultaneous.runAsynch(); /* Run all CPUs */
        }
        
        /* poll for the test complete flag being set on both devices */
        dss_test_complete_flag_device0 = session0.memory.readWord(0,dss_test_complete_flag_addr);
        dss_test_complete_flag_device1 = session1.memory.readWord(0,dss_test_complete_flag_addr);
        
        script.traceWrite("dss_test_complete_flag_device0: 0x" + dss_test_complete_flag_device0.toString(16).toUpperCase());
        script.traceWrite("dss_test_complete_flag_device1: 0x" + dss_test_complete_flag_device1.toString(16).toUpperCase());
        
        java.lang.Thread.sleep(1000);
    }
}

var script  = ScriptingEnvironment.instance();
script.traceBegin(pathLog, pathXSL);
script.traceWrite("Serdes Diag Test Started");

var server = script.getServer("DebugServer.1")
server.setConfig(pathCCXML);
script.traceWrite("DebugServer configured");

script.traceWrite("Opening debug sessions for DSP cores...");
var session0 = server.openSession(sessionName0);
var session1 = server.openSession(sessionName1);
script.traceWrite("DebugSession opened");

script.traceWrite("Connecting to DSP cores...");
session0.target.connect();
session1.target.connect();
script.traceWrite("Target connected!");

script.traceWrite("Resetting target DSP cores!");
session0.target.reset();
session1.target.reset();

java.lang.Thread.sleep(2000);

/* As shown below, the PLL can be programmed to run at higher speeds by enabling the corresponding EVM GEL files */
session0.expression.evaluate("GEL_LoadGel(\"C:/ti/ccsv7/ccs_base/emulation/boards/evmk2e/gel/evmk2e.gel\")");
session1.expression.evaluate("GEL_LoadGel(\"C:/ti/ccsv7/ccs_base/emulation/boards/evmk2e/gel/evmk2e.gel\")");

//script.traceWrite("Executing GEL functions...");
session0.expression.evaluate("CORE_PLL_INIT_100MHZ_to_1GHz()");
session1.expression.evaluate("CORE_PLL_INIT_100MHZ_to_1GHz()");


Synchronized_PRBS_Test(script);

script.traceWrite("Serdes Diag Test Complete");
script.traceEnd();
quit();
