/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*  
 * This file contains the prototypes for functions that interface with 
 * Serdes Debug APIs.  They are common among all tests/examples.
 */

#include <ti/csl/csl_serdes.h>
#if defined (SOC_AM65XX)
#include <ti/diag/serdes_diag/src/am65xx/serdes_diag_k3.h>
#else
#include <ti/diag/serdes_diag/serdes_diag.h>
#include <ti/csl/csl_tsc.h>
#include <ti/csl/csl_cacheAux.h>
#endif
#include "serdes_diag_test.h"
#include "string.h"

void main()
{
    SERDES_DIAG_STAT diag_stat;
    
#if !defined (SOC_AM65XX)
    /* Enable the TSC */
    CSL_tscEnable();

    /* turning off all cache to avoid stale results in BER sweeps */
    printf("Turning off L1 Data Cache.\n");
    CACHE_setL1DSize(CACHE_L1_0KCACHE);
    printf("Turning off L2 Cache.\n");
    CACHE_setL2Size(CACHE_0KCACHE);
#endif

    /* serdes_diag_test_phy_type should be set in serdes_diag_platform.h */
    /* Initializes the peripheral based on the Platform(K2E/K2H/K2K/K2L) and phy type */
    serdes_diag_test_init();


#ifdef RUN_PRBS_ATT_BOOST_CALIBRATION
    /* PRBS test to calibrate the Attenuation and Boost in the receiver */
    diag_stat = Serdes_Example_PRBSTest();
    if(diag_stat != SERDES_DIAG_SETUP_OK)
    {
        printf("\n Invalid PHY Type for the selected platform");
    }
#endif

#ifdef RUN_EYE_SCAN_TEST
    /* On Die Scope/Eye Scan Test Initialization Parameters can be edited inside this API */
    diag_stat = Serdes_Example_EYETest();
    if(diag_stat != SERDES_DIAG_SETUP_OK)
    {
        printf("\n Invalid PHY Type for the selected platform");
    }
#endif

#ifdef RUN_BER_TEST
    /* BER Test Initialization Parameters can be edited inside this API */
    diag_stat = Serdes_Example_BERTest();
    if(diag_stat != SERDES_DIAG_SETUP_OK)
    {
        printf("\n Invalid PHY Type for the selected platform");
    }
#endif

}


