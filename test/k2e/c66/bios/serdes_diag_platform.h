/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*  
 * This file contains the prototypes for functions that interface with 
 * Serdes Diag APIs.  They are common among all tests/examples.
 */

#include <ti/csl/csl_serdes.h>

#define TEST_SERDES_10GE           0
#define TEST_SERDES_AIF2_B8        1
#define TEST_SERDES_AIF2_B4        2
#define TEST_SERDES_SRIO           3
#define TEST_SERDES_PCIe           4
#define TEST_SERDES_HYPERLINK      5
#define TEST_SERDES_SGMII          6
#define TEST_SERDES_DFE            7
#define TEST_SERDES_IQN            8

/* Set your serdes_diag_test_phy_type here */
#define serdes_diag_test_phy_type TEST_SERDES_HYPERLINK 		/* Enables Hyperlink serdes tests */
//#define serdes_diag_test_phy_type TEST_SERDES_SGMII 		/* Enables SGMII serdes tests */
//#define serdes_diag_test_phy_type TEST_SERDES_10GE 		/* Enables XGE serdes tests */
//#define serdes_diag_test_phy_type TEST_SERDES_PCIe 		/* Enables PCIe serdes tests */

#if defined(DEVICE_K2E)
 #if (serdes_diag_test_phy_type == TEST_SERDES_HYPERLINK)
  #define SERDES_DIAG_TEST_BASE_ADDR        	CSL_HYPERLINK_0_SERDES_CFG_REGS /* Serdes base address */
  #define SERDES_DIAG_TEST_REF_CLOCK        	CSL_SERDES_REF_CLOCK_312p5M /* Ref clock of serdes */
  #define SERDES_DIAG_TEST_LINK_RATE        	CSL_SERDES_LINK_RATE_12p5G /* Link rate of serdes */
  #define SERDES_DIAG_TEST_NUM_LANES        	4 /* Number of lanes to be tested */
  #define SERDES_DIAG_TEST_LANE_MASK        	0xF /* All 4 lanes are set */
  #define SERDES_DIAG_TEST_PHY_TYPE         	SERDES_HYPERLINK /* For running Hyperlink tests */
  #define SERDES_DIAG_TEST_LANE_RATE        	CSL_SERDES_LANE_FULL_RATE /* Set to run at full rate of the link rate */
  #define SERDES_DIAG_TEST_LOOPBACK_MODE    	CSL_SERDES_LOOPBACK_DISABLED /* For board-board tests */
  #define SERDES_DIAG_TEST_OPERATING_MODE   	CSL_SERDES_DIAGNOSTIC_MODE /* Should always be set to Diagnostic Mode for BER, EYE and PRBS tests */
  #define SERDES_DIAG_TEST_FORCEATTBOOST    	CSL_SERDES_FORCE_ATT_BOOST_DISABLED
 #elif (serdes_diag_test_phy_type == TEST_SERDES_SGMII)
  #define SERDES_DIAG_TEST_BASE_ADDR            CSL_NETCP_SERDES_0_CFG_REGS
  #define SERDES_DIAG_TEST_REF_CLOCK        	CSL_SERDES_REF_CLOCK_156p25M
  #define SERDES_DIAG_TEST_LINK_RATE        	CSL_SERDES_LINK_RATE_1p25G
  #define SERDES_DIAG_TEST_NUM_LANES        	2
  #define SERDES_DIAG_TEST_LANE_MASK        	0x3
  #define SERDES_DIAG_TEST_PHY_TYPE         	SERDES_SGMII
  #define SERDES_DIAG_TEST_LANE_RATE        	CSL_SERDES_LANE_QUARTER_RATE
  #define SERDES_DIAG_TEST_LOOPBACK_MODE    	CSL_SERDES_LOOPBACK_ENABLED /* SGMII only supports internal lpbk */
  #define SERDES_DIAG_TEST_OPERATING_MODE   	CSL_SERDES_DIAGNOSTIC_MODE
  #define SERDES_DIAG_TEST_FORCEATTBOOST    	CSL_SERDES_FORCE_ATT_BOOST_DISABLED
 #elif (serdes_diag_test_phy_type == TEST_SERDES_10GE)
  #define SERDES_DIAG_TEST_BASE_ADDR        	CSL_XGE_SERDES_CFG_REGS
  #define SERDES_DIAG_TEST_PERIPHERAL_BASE_ADDR (CSL_XGE_CFG_REGS + 0x600) /* Only needed to be set for 10GE, PCIe */
  #define SERDES_DIAG_TEST_REF_CLOCK        	CSL_SERDES_REF_CLOCK_156p25M
  #define SERDES_DIAG_TEST_LINK_RATE        	CSL_SERDES_LINK_RATE_10p3125G
  #define SERDES_DIAG_TEST_NUM_LANES        	2
  #define SERDES_DIAG_TEST_LANE_MASK        	0x3
  #define SERDES_DIAG_TEST_PHY_TYPE         	SERDES_10GE
  #define SERDES_DIAG_TEST_LANE_RATE        	CSL_SERDES_LANE_FULL_RATE
  #define SERDES_DIAG_TEST_LOOPBACK_MODE    	CSL_SERDES_LOOPBACK_DISABLED
  #define SERDES_DIAG_TEST_OPERATING_MODE   	CSL_SERDES_DIAGNOSTIC_MODE
  #define SERDES_DIAG_TEST_FORCEATTBOOST    	CSL_SERDES_FORCE_ATT_BOOST_DISABLED
 #elif (serdes_diag_test_phy_type == TEST_SERDES_PCIe)
  #define SERDES_DIAG_TEST_BASE_ADDR        	CSL_PCIE_0_SERDES_CFG_REGS
  #define SERDES_DIAG_TEST_PERIPHERAL_BASE_ADDR CSL_PCIE_0_SLV_CFG_REGS
  #define SERDES_DIAG_TEST_REF_CLOCK        	CSL_SERDES_REF_CLOCK_100M
  #define SERDES_DIAG_TEST_LINK_RATE        	CSL_SERDES_LINK_RATE_5G
  #define SERDES_DIAG_TEST_NUM_LANES        	2
  #define SERDES_DIAG_TEST_LANE_MASK        	0x3
  #define SERDES_DIAG_TEST_PHY_TYPE         	SERDES_PCIe
  #define SERDES_DIAG_TEST_LANE_RATE        	CSL_SERDES_LANE_FULL_RATE
  #define SERDES_DIAG_TEST_LOOPBACK_MODE    	CSL_SERDES_LOOPBACK_DISABLED
  #define SERDES_DIAG_TEST_OPERATING_MODE   	CSL_SERDES_DIAGNOSTIC_MODE
  #define SERDES_DIAG_TEST_FORCEATTBOOST    	CSL_SERDES_FORCE_ATT_BOOST_DISABLED
 #endif
#endif
