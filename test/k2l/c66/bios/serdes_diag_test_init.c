/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*  
 * This file contains the prototypes for functions that interface with 
 * Serdes Diag APIs.  They are common among all tests/examples.
 */

#include <ti/csl/csl_serdes.h>
#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_serdes_ethernet.h>
#include <ti/csl/csl_serdes_dfe.h>
#include <ti/csl/csl_serdes_pcie.h>
#include <ti/csl/csl_psc.h>
#include <ti/csl/csl_pscAux.h>
#include "serdes_diag_test.h"
#include "serdes_diag_platform.h"
#include "stdlib.h"

void serdes_diag_test_init()
{   

	CSL_SERDES_RESULT status;
	uint32_t i;
	CSL_SERDES_RX_COEFF_T pRxCoeff;
	CSL_SERDES_LANE_ENABLE_PARAMS_T serdes_lane_enable_params;
	CSL_SERDES_LANE_ENABLE_STATUS lane_retval = CSL_SERDES_LANE_ENABLE_NO_ERR;

	memset(&serdes_lane_enable_params, 0, sizeof(serdes_lane_enable_params));
	memset(&pRxCoeff, 0, sizeof(pRxCoeff));

	/* serdes_diag_test_phy_type should be set in serdes_diag_platform.h */
	/* Parameters are set in serdes_diag_platform.h based on phy type */
	serdes_lane_enable_params.base_addr = SERDES_DIAG_TEST_BASE_ADDR;
	serdes_lane_enable_params.ref_clock = SERDES_DIAG_TEST_REF_CLOCK;
	serdes_lane_enable_params.linkrate = SERDES_DIAG_TEST_LINK_RATE;
	serdes_lane_enable_params.num_lanes = SERDES_DIAG_TEST_NUM_LANES;
	serdes_lane_enable_params.lane_mask = SERDES_DIAG_TEST_LANE_MASK;
	serdes_lane_enable_params.phy_type = SERDES_DIAG_TEST_PHY_TYPE;
	serdes_lane_enable_params.operating_mode = SERDES_DIAG_TEST_OPERATING_MODE;

	/* Att and Boost values are obtained through Serdes Diagnostic PRBS calibration test */
	/* For higher speeds PHY-A, force attenuation and boost values  */
	serdes_lane_enable_params.forceattboost = SERDES_DIAG_TEST_FORCEATTBOOST;

	for(i=0; i< serdes_lane_enable_params.num_lanes; i++)
	{
	  serdes_lane_enable_params.lane_ctrl_rate[i] = SERDES_DIAG_TEST_LANE_RATE;
	  serdes_lane_enable_params.loopback_mode[i] = SERDES_DIAG_TEST_LOOPBACK_MODE;

	  /* When RX auto adaptation is on, these are the starting values used for att, boost adaptation */
	  serdes_lane_enable_params.rx_coeff.att_start[i] = 7;
	  serdes_lane_enable_params.rx_coeff.boost_start[i] = 5;

	  /* For higher speeds PHY-A, force attenuation and boost values  */
	  serdes_lane_enable_params.rx_coeff.force_att_val[i] = 1;
	  serdes_lane_enable_params.rx_coeff.force_boost_val[i] = 1;

	  /* CM, C1, C2, Att and Vreg are obtained through Serdes Diagnostic BER test */
	  serdes_lane_enable_params.tx_coeff.cm_coeff[i] = 0;
	  serdes_lane_enable_params.tx_coeff.c1_coeff[i] = 0;
	  serdes_lane_enable_params.tx_coeff.c2_coeff[i] = 0;
	  serdes_lane_enable_params.tx_coeff.tx_att[i] = 12;
	  serdes_lane_enable_params.tx_coeff.tx_vreg[i] = 4;
	}

#if (serdes_diag_test_phy_type == TEST_SERDES_SGMII)
      status = CSL_EthernetSerdesInit(serdes_lane_enable_params.base_addr,
                                      serdes_lane_enable_params.ref_clock,
                                      serdes_lane_enable_params.linkrate);
#elif (serdes_diag_test_phy_type == TEST_SERDES_DFE)
        status = CSL_DFESerdesInit(serdes_lane_enable_params.base_addr,
                                        serdes_lane_enable_params.ref_clock,
                                        serdes_lane_enable_params.linkrate);
#elif (serdes_diag_test_phy_type == TEST_SERDES_PCIe)
      serdes_lane_enable_params.peripheral_base_addr = SERDES_DIAG_TEST_PERIPHERAL_BASE_ADDR;
      status = CSL_PCIeSerdesInit(serdes_lane_enable_params.base_addr,
                                      serdes_lane_enable_params.ref_clock,
                                      serdes_lane_enable_params.linkrate);
#endif
      if (status != CSL_SERDES_NO_ERR)
      {
          printf("Invalid SERDES Init Params \n");
          exit(0);
      }

	  /* Common Init Mode */
	  /* Iteration Mode needs to be set to Common Init Mode first with a lane_mask value equal to the total number of lanes being configured */
	  /* For example, if there are a total of 4 lanes being configured, lane mask needs to be set to 0xF */
	  serdes_lane_enable_params.iteration_mode = CSL_SERDES_LANE_ENABLE_COMMON_INIT;
	  serdes_lane_enable_params.lane_mask = SERDES_DIAG_TEST_LANE_MASK;
	  lane_retval = CSL_SerdesLaneEnable(&serdes_lane_enable_params);
	  if (lane_retval != 0)
	  {
		  printf ("Invalid Serdes Common Init\n");
		  exit(0);
	  }
	  printf("Serdes Common Init Complete\n");

	  /* Lane Init Mode */
	  /* Once CSL_SerdesLaneEnable is called with iteration_mode = CSL_SERDES_LANE_ENABLE_COMMON_INIT, the lanes needs to be initialized by setting
	   * iteration_mode =  CSL_SERDES_LANE_ENABLE_LANE_INIT with the lane_mask equal to the specific lane being configured */
	  /* For example, if lane 0 is being configured, lane mask needs to be set to 0x1. if lane 2 is being configured, lane mask needs to be 0x4 etc */
	  serdes_lane_enable_params.iteration_mode = CSL_SERDES_LANE_ENABLE_LANE_INIT;
	  for(i=0; i< serdes_lane_enable_params.num_lanes; i++)
	  {
		  serdes_lane_enable_params.lane_mask = 1<<i;
		  lane_retval = CSL_SerdesLaneEnable(&serdes_lane_enable_params);
		  if (lane_retval != 0)
		  {
			  printf ("Invalid Serdes Lane Enable Init\n");
			  exit(0);
		  }
		  printf("Serdes Lane %d Init Complete\n", i);
	  }

	  printf("Serdes Init Complete\n");
}
