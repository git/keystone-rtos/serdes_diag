/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/


#include <ti/diag/serdes_diag/serdes_diag.h>
#include <ti/csl/csl_serdes.h>
#include "serdes_diag_test.h"
#include "serdes_diag_platform.h"
#include "string.h"
#include <xdc/runtime/System.h>


uint32_t diag_serdes_device_num = 0;
char filename_ber_scan[30];
char filename_eye_scan[30];
char filename_prbs_scan[30];
FILE *file_ber_scan;
FILE *file_eye_scan;
FILE *file_prbs_scan;
SERDES_DIAG_CMC1C2_T sweepcmc1c2;



SERDES_DIAG_STAT Serdes_Example_BERTest()
{

    uint32_t cm, c1, c2, swing, deemph, count =0;
    CSL_SERDES_TAP_OFFSETS_T pTapOffsets;
    SERDES_DIAG_TX_COEFF_T diag_tx_coeff;
    SERDES_DIAG_BER_INIT_T ber_init_params;

    memset(&sweepcmc1c2, 0, sizeof(sweepcmc1c2));
    memset (&ber_init_params, 0, sizeof(ber_init_params));
    memset(&diag_tx_coeff, 0, sizeof(diag_tx_coeff));
    memset (&pTapOffsets, 0, sizeof(pTapOffsets));

    
    diag_tx_coeff.cm_coeff_start = 0;
    diag_tx_coeff.cm_coeff_end = 1;
    diag_tx_coeff.c1_coeff_start = 0;
    diag_tx_coeff.c1_coeff_end = 1;
    diag_tx_coeff.c2_coeff_start = 0;
    diag_tx_coeff.c2_coeff_end = 1;
    diag_tx_coeff.tx_vreg_start = 4;
    diag_tx_coeff.tx_vreg_end = 4;
    diag_tx_coeff.tx_att_start = 12;
    diag_tx_coeff.tx_att_end = 12;

    ber_init_params.phy_type = SERDES_DIAG_TEST_PHY_TYPE;
    ber_init_params.base_addr = SERDES_DIAG_TEST_BASE_ADDR;
    ber_init_params.num_lanes = SERDES_DIAG_TEST_NUM_LANES;
    ber_init_params.lane_mask = SERDES_DIAG_TEST_LANE_MASK;
    ber_init_params.bertesttime = 1000000000;
    ber_init_params.timeoutloops = 100000000;
    ber_init_params.prbs_pattern = SERDES_PRBS_31;
    ber_init_params.bist_tx_clk_en = SERDES_DIAG_BIST_TX_CLK_ENABLED;
    ber_init_params.bist_rx_clk_en = SERDES_DIAG_BIST_RX_CLK_ENABLED;
    if(SERDES_DIAG_TEST_LOOPBACK_MODE == CSL_SERDES_LOOPBACK_DISABLED)
    {
        ber_init_params.loopback_type = SERDES_DIAG_NON_LOOPBACK;
    }
	else
    {
        ber_init_params.loopback_type = SERDES_DIAG_LOOPBACK;
    }

    if(ber_init_params.phy_type == SERDES_PCIe)
    {
    	if(SERDES_DIAG_TEST_LANE_RATE == CSL_SERDES_LANE_FULL_RATE)
    		ber_init_params.pcie_gen_type = SERDES_DIAG_PCIE_GEN2;
    	else
    		ber_init_params.pcie_gen_type = SERDES_DIAG_PCIE_GEN1;

    	diag_tx_coeff.tx_swing_start = 0;
        diag_tx_coeff.tx_swing_end = 1;
        diag_tx_coeff.tx_deemph_start = 0;
        diag_tx_coeff.tx_deemph_end = 1;
    }

    if(ber_init_params.loopback_type == SERDES_DIAG_FEP_LOOPBACK)
    {
        Serdes_Diag_SetFEPLoopback(&ber_init_params);
        printf("FEP Loopback Setup Complete! Exiting setup\n");
        return SERDES_DIAG_SETUP_OK;
    }

    
    
    if(ber_init_params.phy_type == SERDES_10GE)
    {
        CSL_SerdesGetAverageOffsets(ber_init_params.base_addr, SERDES_DIAG_MAX_LANES, ber_init_params.phy_type, ber_init_params.lane_mask, &pTapOffsets);
    }

    if(ber_init_params.phy_type != SERDES_PCIe)
    {
        Serdes_Diag_SweepCMC1C2(&ber_init_params, &sweepcmc1c2, &pTapOffsets, &diag_tx_coeff);
    }
    else
    {
        Serdes_Diag_SweepPCIE(&ber_init_params, &sweepcmc1c2, &pTapOffsets, &diag_tx_coeff);
    }

    sprintf(filename_ber_scan, "soc_device%d_ber_scanout.txt",diag_serdes_device_num);
    file_ber_scan = fopen(filename_ber_scan, "w");

    sprintf(filename_ber_scan, "soc_device%d_ber_scanout.txt",diag_serdes_device_num);
    printf("\n\nSaving results to file: %s\n\n", filename_ber_scan);

    if(ber_init_params.phy_type != SERDES_PCIe)
    {
    fprintf(file_ber_scan, "CM, C1, C2, LnErs0, Bist_Valid0, Att0, Boost0, DFE1_0, DFE2_0, DFE3_0, DFE4_0, DFE5_0, CDFE1_0, CDFE2_0, CDFE3_0, CDFE4_0, CDFE5_0, CDR0, PMA0, DLEVP0, DLEVN0, DLEAVG0, LnErs1, Bist_Valid1,     Att1, Boost1, DFE1_1, DFE2_1, DFE3_1, DFE4_1, DFE5_1, CDFE1_1, CDFE2_1, CDFE3_1, CDFE4_1, CDFE5_1, CDR1, PMA1, DLEVP1, DLEVN1, DLEAVG1, Testtime \n");
    for (cm = diag_tx_coeff.cm_coeff_start; cm <= diag_tx_coeff.cm_coeff_end; cm++)
    {
        for (c1 = diag_tx_coeff.c1_coeff_start; c1 <= diag_tx_coeff.c1_coeff_end; c1++)
        {
            for (c2 = diag_tx_coeff.c2_coeff_start; c2 <= diag_tx_coeff.c2_coeff_end; c2++)
            {
                fprintf(file_ber_scan, "%u, %u, %u, %u, %u,   %u, %u,    %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %u, %u,   %u, %u, %u,         %u, %u,   %u, %u,    %d, %d, %d, %d, %d, %d, %d, %d, %d, %d,   %u, %u,   %u, %u, %u,     %lld\n",
                        sweepcmc1c2.cm[count],
                        sweepcmc1c2.c1[count],
                        sweepcmc1c2.c2[count],
                        sweepcmc1c2.berlane0[count],
                        sweepcmc1c2.bist_valid0[count],
                        sweepcmc1c2.att0[count],
                        sweepcmc1c2.boost0[count],
                        sweepcmc1c2.dfe1_0[count],
                        sweepcmc1c2.dfe2_0[count],
                        sweepcmc1c2.dfe3_0[count],
                        sweepcmc1c2.dfe4_0[count],
                        sweepcmc1c2.dfe5_0[count],
                        sweepcmc1c2.cdfe1_0[count],
                        sweepcmc1c2.cdfe2_0[count],
                        sweepcmc1c2.cdfe3_0[count],
                        sweepcmc1c2.cdfe4_0[count],
                        sweepcmc1c2.cdfe5_0[count],
                        sweepcmc1c2.cdr0[count],
                        sweepcmc1c2.pma0[count],
                        sweepcmc1c2.dlevp0[count],
                        sweepcmc1c2.dlevn0[count],
                        sweepcmc1c2.dlevavg0[count],
                        sweepcmc1c2.berlane1[count],
                        sweepcmc1c2.bist_valid1[count],
                        sweepcmc1c2.att1[count],
                        sweepcmc1c2.boost1[count],
                        sweepcmc1c2.dfe1_1[count],
                        sweepcmc1c2.dfe2_1[count],
                        sweepcmc1c2.dfe3_1[count],
                        sweepcmc1c2.dfe4_1[count],
                        sweepcmc1c2.dfe5_1[count],
                        sweepcmc1c2.cdfe1_1[count],
                        sweepcmc1c2.cdfe2_1[count],
                        sweepcmc1c2.cdfe3_1[count],
                        sweepcmc1c2.cdfe4_1[count],
                        sweepcmc1c2.cdfe5_1[count],
                        sweepcmc1c2.cdr1[count],
                        sweepcmc1c2.pma1[count],
                        sweepcmc1c2.dlevp1[count],
                        sweepcmc1c2.dlevn1[count],
                        sweepcmc1c2.dlevavg1[count],
                        sweepcmc1c2.run_time[count]);

                count++;
            }
        }
    }
    }
    else 
    {
        fprintf(file_ber_scan, "Swing, Deemph, LnErs0, Bist_Valid0, Att0, Boost0, DFE1_0, DFE2_0, DFE3_0, DFE4_0, DFE5_0, CDFE1_0, CDFE2_0, CDFE3_0, CDFE4_0, CDFE5_0, CDR0, PMA0, DLEVP0, DLEVN0, DLEAVG0, LnErs1, Bist_Valid1,     Att1, Boost1, DFE1_1, DFE2_1, DFE3_1, DFE4_1, DFE5_1, CDFE1_1, CDFE2_1, CDFE3_1, CDFE4_1, CDFE5_1, CDR1, PMA1, DLEVP1, DLEVN1, DLEAVG1, Testtime \n");
        for (swing = diag_tx_coeff.tx_swing_start; swing <= diag_tx_coeff.tx_swing_end; swing++)
        {
            for (deemph = diag_tx_coeff.tx_deemph_start; deemph <= diag_tx_coeff.tx_deemph_end; deemph++)
            {
                fprintf(file_ber_scan, "%u, %u, %u, %u, %u,   %u, %u,    %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %u, %u,   %u, %u, %u,         %u, %u,   %u, %u,    %d, %d, %d, %d, %d, %d, %d, %d, %d, %d,   %u, %u,   %u, %u, %u,     %lld\n",
                            sweepcmc1c2.swing[count],
                            sweepcmc1c2.deemph[count],
                            sweepcmc1c2.berlane0[count],
                            sweepcmc1c2.bist_valid0[count],
                            sweepcmc1c2.att0[count],
                            sweepcmc1c2.boost0[count],
                            sweepcmc1c2.dfe1_0[count],
                            sweepcmc1c2.dfe2_0[count],
                            sweepcmc1c2.dfe3_0[count],
                            sweepcmc1c2.dfe4_0[count],
                            sweepcmc1c2.dfe5_0[count],
                            sweepcmc1c2.cdfe1_0[count],
                            sweepcmc1c2.cdfe2_0[count],
                            sweepcmc1c2.cdfe3_0[count],
                            sweepcmc1c2.cdfe4_0[count],
                            sweepcmc1c2.cdfe5_0[count],
                            sweepcmc1c2.cdr0[count],
                            sweepcmc1c2.pma0[count],
                            sweepcmc1c2.dlevp0[count],
                            sweepcmc1c2.dlevn0[count],
                            sweepcmc1c2.dlevavg0[count],
                            sweepcmc1c2.berlane1[count],
                            sweepcmc1c2.bist_valid1[count],
                            sweepcmc1c2.att1[count],
                            sweepcmc1c2.boost1[count],
                            sweepcmc1c2.dfe1_1[count],
                            sweepcmc1c2.dfe2_1[count],
                            sweepcmc1c2.dfe3_1[count],
                            sweepcmc1c2.dfe4_1[count],
                            sweepcmc1c2.dfe5_1[count],
                            sweepcmc1c2.cdfe1_1[count],
                            sweepcmc1c2.cdfe2_1[count],
                            sweepcmc1c2.cdfe3_1[count],
                            sweepcmc1c2.cdfe4_1[count],
                            sweepcmc1c2.cdfe5_1[count],
                            sweepcmc1c2.cdr1[count],
                            sweepcmc1c2.pma1[count],
                            sweepcmc1c2.dlevp1[count],
                            sweepcmc1c2.dlevn1[count],
                            sweepcmc1c2.dlevavg1[count],
                            sweepcmc1c2.run_time[count]);

                    count++;
                }
            }
        }

    
    if (ber_init_params.loopback_type == SERDES_DIAG_NON_LOOPBACK)
    {
        while(1)
        {
            serdes_diag_dss_test_complete_flag = serdes_diag_dss_test_complete_flag_set;
        }
    }

    printf("BER Sweep Complete!\n");
    return SERDES_DIAG_SETUP_OK;

}



SERDES_DIAG_STAT Serdes_Example_EYETest()
{

    SERDES_DIAG_EYE_OUTPUT_T eye_output;
    SERDES_DIAG_BER_INIT_T ber_init_params;
    SERDES_DIAG_EYE_INIT_T eye_init_params;
    CSL_SERDES_TAP_OFFSETS_T pTapOffsets;
    CSL_SERDES_DLEV_OUT_T pDLEVOut;
    uint32_t lane_num;

    memset(&eye_init_params, 0, sizeof(eye_init_params));
    memset(&ber_init_params, 0, sizeof(ber_init_params));
    memset (&pTapOffsets, 0, sizeof(pTapOffsets));
    memset(&pDLEVOut, 0, sizeof(pDLEVOut));

    
    ber_init_params.base_addr = SERDES_DIAG_TEST_BASE_ADDR;
    ber_init_params.num_lanes = SERDES_DIAG_TEST_NUM_LANES;
    ber_init_params.bertesttime = (uint64_t)100000000;
    ber_init_params.prbs_pattern = SERDES_PRBS_31;
    ber_init_params.phy_type = SERDES_DIAG_TEST_PHY_TYPE;
    ber_init_params.bist_tx_clk_en = SERDES_DIAG_BIST_TX_CLK_ENABLED;
    ber_init_params.bist_rx_clk_en = SERDES_DIAG_BIST_RX_CLK_ENABLED;
    ber_init_params.lane_mask = SERDES_DIAG_TEST_LANE_MASK;
    ber_init_params.phy_type = SERDES_DIAG_TEST_PHY_TYPE;
	if(SERDES_DIAG_TEST_LOOPBACK_MODE == CSL_SERDES_LOOPBACK_DISABLED)
    {
        ber_init_params.loopback_type = SERDES_DIAG_NON_LOOPBACK;
    }
	else
    {
        ber_init_params.loopback_type = SERDES_DIAG_LOOPBACK;
    }

    eye_init_params.base_addr = SERDES_DIAG_TEST_BASE_ADDR;
    eye_init_params.link_rate = SERDES_DIAG_TEST_LINK_RATE;
    eye_init_params.num_lanes = SERDES_DIAG_TEST_NUM_LANES;
    eye_init_params.phase_num = 0;
    eye_init_params.gatetime = (uint64_t)100000000;
    eye_init_params.v_offset = 1;
    eye_init_params.t_offset = 1;
    eye_init_params.calibrate_eye = 0;
    eye_init_params.phy_type = SERDES_DIAG_TEST_PHY_TYPE;
    eye_init_params.lane_mask = SERDES_DIAG_TEST_LANE_MASK;

    
    if(eye_init_params.phy_type == SERDES_10GE)
    {
    	CSL_Serdes_PHYB_Init_Config(eye_init_params.base_addr,
                                         SERDES_DIAG_MAX_LANES,
                                         eye_init_params.phy_type,
                                         eye_init_params.lane_mask,
                                         &pTapOffsets);
        printf("PHY-B init config Complete!\n");
    }

    
    if(eye_init_params.phy_type != SERDES_10GE)
    {
        if (eye_init_params.link_rate >= CSL_SERDES_LINK_RATE_9p8304G)
         {
            for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
            {
                if ((eye_init_params.lane_mask & (1<<lane_num))!=0)
                {
                	CSL_Serdes_PHYA_Init_Config(eye_init_params.base_addr, lane_num);
                }
            }
         }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((eye_init_params.lane_mask & (1<<lane_num))!=0)
        {
            CSL_SerdesClearTXIdle(eye_init_params.base_addr, lane_num,
                                  eye_init_params.phy_type);
        }
    }

    
    Serdes_Diag_BERTest_TX(&ber_init_params);

    
    CSL_SerdesWaitForRXValid(eye_init_params.base_addr, SERDES_DIAG_MAX_LANES, eye_init_params.lane_mask, eye_init_params.phy_type);
    printf("RX valid detected!\n");

    
    if(eye_init_params.phy_type != SERDES_10GE)
    {
    	CSL_SerdesRXAttConfig_PhyA(eye_init_params.base_addr,
                                         SERDES_DIAG_MAX_LANES,
                                         eye_init_params.phy_type,
                                         eye_init_params.lane_mask);

        for(lane_num=0; lane_num< SERDES_DIAG_MAX_LANES; lane_num++)
        {
            if ((eye_init_params.lane_mask & (1<<lane_num))!=0)
            {
            	CSL_SerdesRXBoostConfig_PhyA(eye_init_params.base_addr, lane_num,
                        eye_init_params.phy_type,
                        eye_init_params.lane_mask);
            }
        }
    }

    
    else
    {
        for(lane_num=0; lane_num< SERDES_DIAG_MAX_LANES; lane_num++)
        {
            if ((eye_init_params.lane_mask & (1<<lane_num))!=0)
            {
            	CSL_SerdesRXAttBoostConfig_PhyB(eye_init_params.base_addr, lane_num,
                                        eye_init_params.phy_type,
                                        eye_init_params.lane_mask);
            }
        }
    }
    printf("Attenuation Boost Configs complete!\n");

    
    if(eye_init_params.phy_type == SERDES_10GE)
    {
        for(lane_num=0; lane_num< SERDES_DIAG_MAX_LANES; lane_num++)
        {
            if ((eye_init_params.lane_mask & (1<<lane_num))!=0)
            {
            	CSL_Serdes_RX_Calibration_PHYB(eye_init_params.base_addr,lane_num, eye_init_params.phy_type,
                                  &pTapOffsets,&pDLEVOut);
            }
        }
        printf("RX Calibration complete!\n");
    }

    for(lane_num=0; lane_num< SERDES_DIAG_MAX_LANES; lane_num++)
    {
        if ((eye_init_params.lane_mask & (1<<lane_num))!=0)
        {
            eye_init_params.lane_num = lane_num;

            sprintf(filename_eye_scan, "soc_device%d_eye_scanout_lane%d.txt",diag_serdes_device_num, lane_num);
            file_eye_scan = fopen(filename_eye_scan, "w");

            sprintf(filename_eye_scan, "soc_device%d_eye_scanout_lane%d.txt",diag_serdes_device_num, lane_num);

            printf("Beginning Serdes_Diag_EyeMonitor for lane%d...\n", lane_num);

            
            Serdes_Diag_EyeMonitor(filename_eye_scan, eye_init_params, &eye_output);

            printf("Finished Serdes_Diag_EyeMonitor for lane%d!\n",lane_num);
        }
    }

    printf("Eye Scans complete!\n");

    
    if (ber_init_params.loopback_type == SERDES_DIAG_NON_LOOPBACK)
    {
        while(1)
        {
            serdes_diag_dss_test_complete_flag = serdes_diag_dss_test_complete_flag_set;
        }
    }

    return SERDES_DIAG_SETUP_OK;
}




SERDES_DIAG_STAT Serdes_Example_PRBSTest()
{

    SERDES_DIAG_BER_INIT_T ber_init_params;
    uint32_t lane_num, lane_status;
    CSL_SERDES_RX_COEFF_T pRxCoeff;
    CSL_SERDES_LANE_ENABLE_PARAMS_T serdes_lane_enable_params;
    CSL_SERDES_TAP_OFFSETS_T pTapOffsets;
    CSL_SERDES_DLEV_OUT_T pDLEVOut;

    memset(&ber_init_params, 0, sizeof(ber_init_params));
    memset(&serdes_lane_enable_params, 0, sizeof(serdes_lane_enable_params));
    memset (&pTapOffsets, 0, sizeof(pTapOffsets));
    memset(&pDLEVOut, 0, sizeof(pDLEVOut));


    
    ber_init_params.base_addr = serdes_lane_enable_params.base_addr = SERDES_DIAG_TEST_BASE_ADDR;
    ber_init_params.num_lanes = serdes_lane_enable_params.num_lanes = SERDES_DIAG_TEST_NUM_LANES;
    ber_init_params.lane_mask = serdes_lane_enable_params.lane_mask = SERDES_DIAG_TEST_LANE_MASK;
    ber_init_params.bertesttime = (uint64_t)100000000;
    ber_init_params.timeoutloops = (uint64_t)100000000;
    ber_init_params.prbs_pattern = SERDES_PRBS_31;
    ber_init_params.phy_type = serdes_lane_enable_params.phy_type = SERDES_DIAG_TEST_PHY_TYPE;
    ber_init_params.bist_tx_clk_en = SERDES_DIAG_BIST_TX_CLK_ENABLED;
    ber_init_params.bist_rx_clk_en = SERDES_DIAG_BIST_RX_CLK_ENABLED;
    if(SERDES_DIAG_TEST_LOOPBACK_MODE == CSL_SERDES_LOOPBACK_DISABLED)
    {
        ber_init_params.loopback_type = SERDES_DIAG_NON_LOOPBACK;
    }
	else
    {
        ber_init_params.loopback_type = SERDES_DIAG_LOOPBACK;
    }

    for(lane_num=0; lane_num< serdes_lane_enable_params.num_lanes; lane_num++)
    {
        ber_init_params.lane_ctrl_rate[lane_num] = serdes_lane_enable_params.lane_ctrl_rate[lane_num] = SERDES_DIAG_TEST_LANE_RATE;
    }

    
    if (diag_serdes_device_num == 0)
    {
        ber_init_params.polarity[0] = 0;
        ber_init_params.polarity[1] = 0;
        ber_init_params.polarity[2] = 0;
        ber_init_params.polarity[3] = 0;
    }
    else if (diag_serdes_device_num == 1)
    {
        ber_init_params.polarity[0] = 0;
        ber_init_params.polarity[1] = 0;
        ber_init_params.polarity[2] = 0;
        ber_init_params.polarity[3] = 0;
    }

    
    if(ber_init_params.phy_type == SERDES_10GE)
    {
    	CSL_Serdes_PHYB_Init_Config(ber_init_params.base_addr,
                                         ber_init_params.num_lanes,
                                         ber_init_params.phy_type,
                                         ber_init_params.lane_mask,
                                         &pTapOffsets);
    }



    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_init_params.lane_mask & (1<<lane_num))!=0)
        {
            CSL_SerdesClearTXIdle(ber_init_params.base_addr, lane_num,
                                  ber_init_params.phy_type);
        }
    }

    printf("Enabling TX PRBS Pattern\n");
    
    Serdes_Diag_BERTest_TX(&ber_init_params);

    if (ber_init_params.phy_type == SERDES_HYPERLINK)
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_init_params.lane_mask & (1<<lane_num))!=0)
            {
                CSL_SerdesSetLanePolarity(ber_init_params.base_addr, lane_num, ber_init_params.polarity[lane_num]);
            }
        }
    }

    System_printf("Checking for PRBS Bist Valid....\n");
    Serdes_Diag_PRBS_Check(&ber_init_params, &lane_status);
    serdes_lane_enable_params.lane_mask = lane_status;

    for (lane_num=0; lane_num<ber_init_params.num_lanes; lane_num++)
    {
        if (lane_status & 0x1)
        {
            System_printf("Lane %d PRBS check passed!\n", lane_num);
        }
        else
        {
            System_printf("Lane %d PRBS check timeout .........\n", lane_num);
        }
        lane_status >>= 1;
     }

    System_printf("Calibrating Attenuation Boost using PRBS.. with lane_mask 0x%x\n", serdes_lane_enable_params.lane_mask);

    Serdes_Diag_Att_Boost_Calibration(&serdes_lane_enable_params, &pRxCoeff);

    for(lane_num=0; lane_num<ber_init_params.num_lanes; lane_num++)
    {
        sprintf(filename_prbs_scan, "soc_device%d_prbs_scanout_lane%d.txt",diag_serdes_device_num, lane_num);
        file_prbs_scan = fopen(filename_prbs_scan, "w");

        sprintf(filename_prbs_scan, "soc_device%d_prbs_scanout_lane%d.txt",diag_serdes_device_num, lane_num);

        fprintf(file_prbs_scan,"att boost \n");

        fprintf(file_prbs_scan,"%d %d \n", pRxCoeff.rx_att[lane_num], pRxCoeff.rx_boost[lane_num]);

        fclose(file_prbs_scan);

        printf("att = %d for lane %d ", pRxCoeff.rx_att[lane_num], lane_num);
        printf("boost = %d for lane %d \n", pRxCoeff.rx_boost[lane_num], lane_num);
    }

    System_printf("Finished Attenuation Boost calibration!\n");
    printf("PRBS Scans complete!\n");

    
    if (ber_init_params.loopback_type == SERDES_DIAG_NON_LOOPBACK)
    {
        while(1)
        {
            serdes_diag_dss_test_complete_flag = serdes_diag_dss_test_complete_flag_set;
        }
    }

    return SERDES_DIAG_SETUP_OK;
}

