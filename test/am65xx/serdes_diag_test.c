/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/


#include <ti/diag/serdes_diag/src/am65xx/serdes_diag_k3.h>
#include <ti/csl/csl_serdes.h>
#include "serdes_diag_test.h"
#include "serdes_diag_platform.h"
#include "string.h"


uint32_t diag_serdes_device_num = 0;
char filename_ber_scan[30];
char filename_eye_scan[30];
char filename_prbs_scan[30];
FILE *file_ber_scan;
FILE *file_eye_scan;
FILE *file_prbs_scan;
SERDES_DIAG_SWEEP_T sweepber;



SERDES_DIAG_STAT Serdes_Example_BERTest()
{

    uint32_t count =0, laneNum, sweep1, sweep2;
    CSL_SerdesTapOffsets pTapOffsets;
    SERDES_DIAG_TX_COEFF_T diagTxCoeff;
    SERDES_DIAG_RX_COEFF_T diagRxCoeff;
    SERDES_DIAG_BER_INIT_T berParamsInit;

    memset(&sweepber, 0, sizeof(sweepber));
    memset (&berParamsInit, 0, sizeof(berParamsInit));
    memset(&diagTxCoeff, 0, sizeof(diagTxCoeff));
    memset (&pTapOffsets, 0, sizeof(pTapOffsets));

    berParamsInit.laneCtrlRate = SERDES_DIAG_TEST_LANE_RATE;
    berParamsInit.phyType = SERDES_DIAG_TEST_PHY_TYPE;
    berParamsInit.baseAddr = SERDES_DIAG_TEST_BASE_ADDR;
    berParamsInit.numLanes = SERDES_DIAG_TEST_NUM_LANES;
    berParamsInit.laneMask = SERDES_DIAG_TEST_LANE_MASK;
    berParamsInit.bertesttime = 2000000; 
    berParamsInit.timeoutLoops = 1000000;
    berParamsInit.prbsPattern = SERDES_DIAG_TEST_PRBS_PATTERN;
    berParamsInit.forceAttBoost = SERDES_DIAG_TEST_FORCEATTBOOST;
    berParamsInit.sweepTxRx = SERDES_DIAG_TEST_SWEEPTXRX;
    berParamsInit.loopbackType = SERDES_DIAG_TEST_LOOPBACK_MODE;
    berParamsInit.bistTxMode = SERDES_DIAG_TX_MODE_CONTINUOUS;
    berParamsInit.pcieGenType = SERDES_DIAG_TEST_PCIE_GEN_TYPE;

    diagRxCoeff.rxAttStart = 0;
    diagRxCoeff.rxAttEnd = 8;
    diagRxCoeff.rxBoostStart = 0;
    diagRxCoeff.rxBoostEnd = 16;

    if(berParamsInit.phyType == CSL_SERDES_PHY_TYPE_PCIe && berParamsInit.pcieGenType == SERDES_DIAG_PCIE_GEN3)
    {
        diagTxCoeff.presetStart = 0;
        diagTxCoeff.presetEnd = 9;
    }
    else if (berParamsInit.phyType == CSL_SERDES_PHY_TYPE_SGMII)
    {
        diagTxCoeff.presetStart = 0;
        diagTxCoeff.presetEnd = 9;
    }
    else
    {
        diagTxCoeff.presetStart = 0;
        diagTxCoeff.presetEnd = 1;
    }

    if(berParamsInit.sweepTxRx == SERDES_DIAG_SWEEP_TX)
    {
        berParamsInit.sweepStart1 = diagTxCoeff.presetStart;
        berParamsInit.sweepEnd1 = diagTxCoeff.presetEnd;
        berParamsInit.sweepStart2 = 0; //Dummy sweep to be compatible with rx 2d sweep
        berParamsInit.sweepEnd2 = 0; //Dummy sweep to be compatible with rx 2d sweep
    }
    else if(berParamsInit.sweepTxRx == SERDES_DIAG_SWEEP_RX)
    {
        berParamsInit.sweepStart1 = diagRxCoeff.rxAttStart;
        berParamsInit.sweepEnd1 = diagRxCoeff.rxAttEnd;
        berParamsInit.sweepStart2 = diagRxCoeff.rxBoostStart;
        berParamsInit.sweepEnd2 = diagRxCoeff.rxBoostEnd;
    }

    if(berParamsInit.forceAttBoost)
    {
        berParamsInit.forceAttVal = 3;
        berParamsInit.forceBoostVal = 12;
    }

    berParamsInit.bistTxClkEn = SERDES_DIAG_BIST_TX_CLK_ENABLED;
    berParamsInit.bistRxClkEn = SERDES_DIAG_BIST_RX_CLK_ENABLED;

#if (serdes_diag_test_phyType == TEST_SERDES_PCIe)
    berParamsInit.pcieGenType = SERDES_DIAG_TEST_PCIE_GEN_TYPE;
#endif

    printf("Setting Lane Ctrl Overrides \n");
    for(laneNum = 0; laneNum < berParamsInit.numLanes; laneNum++)
    {
        if ((berParamsInit.laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit.phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                Serdes_Diag_PCIeCtrlOverride(&berParamsInit);
            }
            else if(berParamsInit.phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                Serdes_Diag_USBCtrlOverride(&berParamsInit);
            }
            else if(berParamsInit.phyType == CSL_SERDES_PHY_TYPE_SGMII)
            {
                Serdes_Diag_SGMIICtrlOverride(&berParamsInit);
            }
        }
    }

//    for(laneNum = 0; laneNum < berParamsInit.numLanes; laneNum++)
//    {
//        CSL_SerdesInvertLanePolarity(berParamsInit.baseAddr,laneNum);
//    }

    if(berParamsInit.loopbackType == CSL_SERDES_LOOPBACK_FEP)
    {
        for(laneNum = 0; laneNum < berParamsInit.numLanes; laneNum++)
        {
            if ((berParamsInit.laneMask & (1<<laneNum))!=0)
            {
                Serdes_Diag_SetFEPLoopback(berParamsInit.baseAddr, laneNum);
            }
        }
        printf("FEP Loopback Setup Complete! Exiting setup\n");
        return SERDES_DIAG_SETUP_OK;
    }

    if(berParamsInit.loopbackType == CSL_SERDES_LOOPBACK_FEP1p5)
    {
        for(laneNum = 0; laneNum < berParamsInit.numLanes; laneNum++)
        {
            if ((berParamsInit.laneMask & (1<<laneNum))!=0)
            {
                Serdes_Diag_SetFEP1p5Loopback(berParamsInit.baseAddr, laneNum);
            }
        }
        printf("FEP1.5 Loopback Setup Complete! Exiting setup\n");
        return SERDES_DIAG_SETUP_OK;
    }

    if(berParamsInit.loopbackType == CSL_SERDES_LOOPBACK_FES)
    {
        for(laneNum = 0; laneNum < berParamsInit.numLanes; laneNum++)
        {
            if ((berParamsInit.laneMask & (1<<laneNum))!=0)
            {
                Serdes_Diag_SetFESLoopback(berParamsInit.baseAddr, laneNum);
            }
        }
        printf("FES Loopback Setup Complete! Exiting setup\n");
        return SERDES_DIAG_SETUP_OK;
    }

    if(berParamsInit.loopbackType == CSL_SERDES_LOOPBACK_TX_TO_BERT)
    {
        Serdes_Diag_BERTestTX(&berParamsInit);
        printf("TX to external Setup Complete! Exiting setup\n");
        return SERDES_DIAG_SETUP_OK;
    }

    if(berParamsInit.loopbackType == CSL_SERDES_LOOPBACK_PIPE)
    {
        printf("PIPE Loopback Setup Complete! Exiting setup\n");
        return SERDES_DIAG_SETUP_OK;
    }

    printf("Entering BER Sweep Setup \n");
    Serdes_Diag_SweepBER(&berParamsInit, &sweepber, &pTapOffsets, &diagTxCoeff);

    sprintf(filename_ber_scan, "soc_device%d_ber_scanout.txt",diag_serdes_device_num);
    file_ber_scan = fopen(filename_ber_scan, "w");

    sprintf(filename_ber_scan, "soc_device%d_ber_scanout.txt",diag_serdes_device_num);
    printf("\n\nSaving results to file: %s\n\n", filename_ber_scan);


    fprintf(file_ber_scan, "Sweep1, Sweep2,   LnErs0, Bist_Valid0,   Att0, Boost0,    DFE Slicer, DFE Tap, DFE Comp, DLEV, Gain, DFE DLEVA, DFE DLEVB, DFE Tap, CDR0, PMA0, Testtime \n");
    for (sweep1 = berParamsInit.sweepStart1; sweep1 <= berParamsInit.sweepEnd1; sweep1++)
    {
        for (sweep2 = berParamsInit.sweepStart2; sweep2 <= berParamsInit.sweepEnd2; sweep2++)
        {
            fprintf(file_ber_scan, "%u, %u,   %u, %u,   %u, %u,    %d, %d, %d, %d, %d, %d, %d, %d, %u, %u,  %lld\n",
                        sweepber.sweep1[count],
                        sweepber.sweep2[count],
                        sweepber.berlane0[count],
                        sweepber.bistValid0[count],
                        sweepber.att0[count],
                        sweepber.boost0[count],
                        sweepber.dfe1_0[count],
                        sweepber.dfe2_0[count],
                        sweepber.dfe3_0[count],
                        sweepber.dfe4_0[count],
                        sweepber.dfe5_0[count],
                        sweepber.cdfe1_0[count],
                        sweepber.cdfe2_0[count],
                        sweepber.cdfe3_0[count],
                        sweepber.cdr0[count],
                        sweepber.pma0[count],
                        sweepber.runTime[count]);

                count++;
        }
    }

    printf("BER Sweep Complete!\n");
    return SERDES_DIAG_SETUP_OK;

}

SERDES_DIAG_STAT Serdes_Example_EYETest()
{
    uint32_t laneNum;
    SERDES_DIAG_BER_INIT_T berParamsInit;
    memset (&berParamsInit, 0, sizeof(berParamsInit));

    berParamsInit.laneCtrlRate = SERDES_DIAG_TEST_LANE_RATE;
    berParamsInit.phyType = SERDES_DIAG_TEST_PHY_TYPE;
    berParamsInit.baseAddr = SERDES_DIAG_TEST_BASE_ADDR;
    berParamsInit.numLanes = SERDES_DIAG_TEST_NUM_LANES;
    berParamsInit.laneMask = SERDES_DIAG_TEST_LANE_MASK;
    berParamsInit.bertesttime = 2000000; //Equivalent to 10ms
    berParamsInit.timeoutLoops = 1000000;
    berParamsInit.prbsPattern = SERDES_DIAG_TEST_PRBS_PATTERN;
    berParamsInit.forceAttBoost = SERDES_DIAG_TEST_FORCEATTBOOST;
    berParamsInit.sweepTxRx = SERDES_DIAG_TEST_SWEEPTXRX;
    berParamsInit.loopbackType = SERDES_DIAG_TEST_LOOPBACK_MODE;
    berParamsInit.bistTxMode = SERDES_DIAG_TX_MODE_CONTINUOUS;
    berParamsInit.pcieGenType = SERDES_DIAG_TEST_PCIE_GEN_TYPE;

    if(berParamsInit.forceAttBoost)
    {
        berParamsInit.forceAttVal = 4;
        berParamsInit.forceBoostVal = 5;
    }

    berParamsInit.bistTxClkEn = SERDES_DIAG_BIST_TX_CLK_ENABLED;
    berParamsInit.bistRxClkEn = SERDES_DIAG_BIST_RX_CLK_ENABLED;

#if (serdes_diag_test_phyType == TEST_SERDES_PCIe)
    berParamsInit.pcieGenType = SERDES_DIAG_TEST_PCIE_GEN_TYPE;
#endif

    printf("Setting Lane Ctrl Overrides \n");
    for(laneNum = 0; laneNum < berParamsInit.numLanes; laneNum++)
    {
        if ((berParamsInit.laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit.phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                Serdes_Diag_PCIeCtrlOverride(&berParamsInit);
            }
            else if(berParamsInit.phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                Serdes_Diag_USBCtrlOverride(&berParamsInit);
            }
            else if(berParamsInit.phyType == CSL_SERDES_PHY_TYPE_SGMII)
            {
                Serdes_Diag_SGMIICtrlOverride(&berParamsInit);
            }
        }
    }



    Serdes_Diag_BERTestTX(&berParamsInit);

    //Then setup RX and measure EYE
    Serdes_Diag_EYETest(&berParamsInit,0,2,4);

    printf("On Chip Eye Measurement Complete\n");
    return SERDES_DIAG_SETUP_OK;

}

