/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*  
 * This file contains the prototypes for functions that interface with 
 * Serdes Diag APIs.  They are common among all tests/examples.
 */

#include <ti/csl/soc.h>
#include <ti/csl/csl_serdes.h>
#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_serdes_pcie.h>
#include <ti/csl/csl_serdes_usb.h>
#include <ti/csl/csl_serdes_ethernet.h>
#include <ti/csl/cslr_pcie.h>
#include "serdes_diag_test.h"
#include "serdes_diag_platform.h"
#include "stdlib.h"

#define KICK0 0x68EF3490ull
#define KICK1 0xD172BC5Aull

void serdes_diag_test_init()
{   

	CSL_SerdesResult status;
	uint32_t i;
	CSL_SerdesLaneEnableParams serdesLaneEnableParams;
	CSL_SerdesLaneEnableStatus laneRetVal = CSL_SERDES_LANE_ENABLE_NO_ERR;

	memset(&serdesLaneEnableParams, 0, sizeof(serdesLaneEnableParams));

	CSL_serdesPorReset(SERDES_DIAG_TEST_BASE_ADDR);


    /* Serdes Input Sel details found in Maxwell IP Integration Spec Serdes Signal Connections
     *
	SERDES0.sys_lane0_ip_sel[1:0]
	Input that selects which IP input is muxed into serdes output lane
	00 : USB
	01 : PCIE0 L0
	10 : ICSS2 L0
	11 : Reserved
	MAIN_CTRL_MMR.SERDES0_CTRL.LANE_FUNC_SEL

	SERDES1.sys_lane0_ip_sel[1:0]
	Input that selects which IP input is muxed into serdes
	output lane
	00 : PCIE1 L0
	01 : PCIE0 L1
	10 : ICSS2 L1
	11 : Reserved
	MAIN_CTRL_MMR.SERDES1_CTRL.LANE_FUNC_SEL

    Before writing to ip_sel, the main ctrl mmr kick registers need to be unlocked. lock1_kick0 and lock1_kick1 need to be unlocked.
    lock1_kick0 is at 0x100000 + 0x5008 = 0x68EF3490
    lock1_kick1 is at 0x100000 + 0x500c = 0xD172BC5A
    Reading bit 0 of these registers will give the lock status. if it is 1, then it is unlocked for the MMR to be writable
    */

	/* This application code assumes that PSC is enabled for the serdes */

	/* Unlock Lock1 Kick Registers */
	*(volatile uint64_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_LOCK1_KICK0) = KICK0;
	*(volatile uint64_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_LOCK1_KICK1) = KICK1;

	/* Unlock Lock2 Kick Registers */
	*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_LOCK2_KICK0) = KICK0;
	*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_LOCK2_KICK1) = KICK1;

	/* Select lane_func_sel based on the serdes type */
#if (serdes_diag_test_phy_type == TEST_SERDES_USB)
	CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES0_CTRL),1,0,0x0);
#elif (serdes_diag_test_phy_type == TEST_SERDES_PCIe)
	CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES0_CTRL),1,0,0x1);
#elif (serdes_diag_test_phy_type == TEST_SERDES_SGMII)
	CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES0_CTRL),1,0,0x2);
#endif

/* Select Left CML clock for serdes0 and Right CML clock for serdes1 */
#if (SERDES_DIAG_TEST_BASE_ADDR == CSL_SERDES0_BASE)
    CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES0_CTRL),7,4,0x4);
    CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES0_REFCLK_SEL),1,0,0x2);
#elif (SERDES_DIAG_TEST_BASE_ADDR == CSL_SERDES1_BASE)
    CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES1_CTRL),7,4,0x1);
    CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES1_REFCLK_SEL),1,0,0x2);
#endif

	/* Set the serdes0 refclk input select register  to main_pll_clkout */
	/* Make sure that main_pll_clkout is the right clk for PCIe/USB/SGMII */
	CSL_FINSR(*(volatile uint32_t *)(CSL_CTRL_MMR0_CFG0_BASE + CSL_MAIN_CTRL_MMR_CFG0_SERDES0_REFCLK_SEL),1,0,0x2);

	serdesLaneEnableParams.baseAddr = SERDES_DIAG_TEST_BASE_ADDR;
	serdesLaneEnableParams.refClock = SERDES_DIAG_TEST_REF_CLOCK;
	serdesLaneEnableParams.linkRate = SERDES_DIAG_TEST_LINK_RATE;
	serdesLaneEnableParams.numLanes = SERDES_DIAG_TEST_NUM_LANES;
	serdesLaneEnableParams.laneMask = SERDES_DIAG_TEST_LANE_MASK;
	serdesLaneEnableParams.phyType = SERDES_DIAG_TEST_PHY_TYPE;
	serdesLaneEnableParams.operatingMode = SERDES_DIAG_TEST_OPERATING_MODE;
	for(i=0; i< serdesLaneEnableParams.numLanes; i++)
	{
	  serdesLaneEnableParams.laneCtrlRate[i] = SERDES_DIAG_TEST_LANE_RATE;
	  serdesLaneEnableParams.loopbackMode[i] = SERDES_DIAG_TEST_LOOPBACK_MODE;
	}

	if(serdes_diag_test_phy_type == TEST_SERDES_PCIe)
	{
#ifdef QT
	    /* QT Patch for Gen3 */
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_TEST_BASE_ADDR + 0x248), 31, 0, 0x500);
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_TEST_BASE_ADDR + 0x1400), 31, 0, 0x8800003E);
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_TEST_BASE_ADDR + 0x1ffc), 31, 0, 0x05000014);
#endif
	}

	/*Load the Serdes Config File */
#if (serdes_diag_test_phy_type == TEST_SERDES_SGMII)
	status = CSL_serdesEthernetInit(serdesLaneEnableParams.baseAddr,
    		  	  	  	  	  	  	  serdesLaneEnableParams.numLanes,
                                      serdesLaneEnableParams.refClock,
                                      serdesLaneEnableParams.linkRate); //Use this for SGMII serdes config load
#elif (serdes_diag_test_phy_type == TEST_SERDES_PCIe)
	status = CSL_serdesPCIeInit(serdesLaneEnableParams.baseAddr,
    		  	  	  	  	  	serdesLaneEnableParams.numLanes,
                                serdesLaneEnableParams.refClock,
                                serdesLaneEnableParams.linkRate); //Use this for PCIe serdes config load
#elif (serdes_diag_test_phy_type == TEST_SERDES_USB)
	status = CSL_serdesUSBInit(serdesLaneEnableParams.baseAddr,
    		  	  	  	  	  	serdesLaneEnableParams.numLanes,
                                serdesLaneEnableParams.refClock,
                                serdesLaneEnableParams.linkRate); //Use this for PCIe serdes config load
#endif

      /* Return error if input params are invalid */
  	if (status != CSL_SERDES_NO_ERR)
    {
  	    if(serdes_diag_test_phy_type == TEST_SERDES_SGMII)
  	    {
  	        printf("\nInvalid SGMII SERDES Init Params \n");
  	    }
        if(serdes_diag_test_phy_type == TEST_SERDES_USB)
        {
            printf("\nInvalid USB SERDES Init Params \n");
        }
        if(serdes_diag_test_phy_type == TEST_SERDES_PCIe)
        {
            printf("\nInvalid PCIe SERDES Init Params \n");
        }
    }
  	else
  	{
        if(serdes_diag_test_phy_type == TEST_SERDES_SGMII)
        {
            printf("\nSGMII SERDES Config Load Complete \n");
        }
        if(serdes_diag_test_phy_type == TEST_SERDES_USB)
        {
            printf("\nUSB SERDES Config Load Complete \n");
        }
        if(serdes_diag_test_phy_type == TEST_SERDES_PCIe)
        {
            printf("\nPCIe SERDES Config Load Complete \n");
        }
  	}

	/* Common Lane Enable API for lane enable, pll enable etc */
  	laneRetVal = CSL_serdesLaneEnable(&serdesLaneEnableParams);

	if (laneRetVal != 0)
	{
        if(serdes_diag_test_phy_type == TEST_SERDES_SGMII)
        {
            printf("\nInvalid SGMII Lane Enable \n");
        }
        if(serdes_diag_test_phy_type == TEST_SERDES_USB)
        {
            printf("\nInvalid USB Lane Enable \n");
        }
        if(serdes_diag_test_phy_type == TEST_SERDES_PCIe)
        {
            printf("\nInvalid PCIe Lane Enable \n");
        }
	}
	else
	{
        if(serdes_diag_test_phy_type == TEST_SERDES_SGMII)
        {
            printf("SGMII Lane Enable Complete \n");
        }
        if(serdes_diag_test_phy_type == TEST_SERDES_USB)
        {
            printf("USB Lane Enable Complete \n");
        }
        if(serdes_diag_test_phy_type == TEST_SERDES_PCIe)
        {
            printf("PCIe Lane Enable Complete \n");
        }
	}

	/* Check CMU_OK */
	CSL_serdesWaitForCMUOK(serdesLaneEnableParams.baseAddr);

	/* Wait 20us for phystatus before switching to Diag Mode */
	CSL_serdesCycleDelay(20000);
}
