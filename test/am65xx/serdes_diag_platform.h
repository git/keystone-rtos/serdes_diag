/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*  
 * This file contains the prototypes for functions that interface with 
 * Serdes Diag APIs.  They are common among all tests/examples.
 */

#include <ti/csl/csl_serdes.h>

#define TEST_SERDES_USB            0
#define TEST_SERDES_PCIe           1
#define TEST_SERDES_SGMII          2

/* Set your serdes_diag_test_phy_type here */
//#define serdes_diag_test_phy_type TEST_SERDES_USB 		/* Enables USB serdes tests */
#define serdes_diag_test_phy_type TEST_SERDES_PCIe 		/* Enables PCIe serdes tests */
//#define serdes_diag_test_phy_type   TEST_SERDES_SGMII         /* Enables SGMII serdes tests */

#if defined(SOC_AM65XX)
 #if (serdes_diag_test_phy_type == TEST_SERDES_USB)
  #define SERDES_DIAG_TEST_BASE_ADDR        	CSL_SERDES0_BASE /* Serdes base address */
  #define SERDES_DIAG_TEST_REF_CLOCK        	CSL_SERDES_REF_CLOCK_100M /* Ref clock of serdes */
  #define SERDES_DIAG_TEST_LINK_RATE        	CSL_SERDES_LINK_RATE_5G /* Link rate of serdes */
  #define SERDES_DIAG_TEST_NUM_LANES        	1 /* Number of lanes to be tested */
  #define SERDES_DIAG_TEST_LANE_MASK        	0x1 /* All lanes are set */
  #define SERDES_DIAG_TEST_PHY_TYPE         	CSL_SERDES_PHY_TYPE_USB /* For running USB tests */
  #define SERDES_DIAG_TEST_PCIE_GEN_TYPE        SERDES_DIAG_PCIE_GEN3 /* For running PCIe tests */
  #define SERDES_DIAG_TEST_PRBS_PATTERN         SERDES_PRBS_7 /* prbs7 pattern */
  #define SERDES_DIAG_TEST_LANE_RATE        	CSL_SERDES_LANE_FULL_RATE /* Set to run at full rate of the link rate */
  #define SERDES_DIAG_TEST_LOOPBACK_MODE    	CSL_SERDES_LOOPBACK_NES /* For internal near end serial loopback tests */
  #define SERDES_DIAG_TEST_OPERATING_MODE   	CSL_SERDES_DIAGNOSTIC_MODE /* Should always be set to Diagnostic Mode for BER, EYE and PRBS tests */
  #define SERDES_DIAG_TEST_FORCEATTBOOST    	CSL_SERDES_FORCE_ATT_BOOST_DISABLED
  #define SERDES_DIAG_TEST_SWEEPTXRX            SERDES_DIAG_SWEEP_RX
#elif (serdes_diag_test_phy_type == TEST_SERDES_PCIe)
  #define SERDES_DIAG_TEST_BASE_ADDR        	CSL_SERDES0_BASE /* Serdes base address */
  #define SERDES_DIAG_TEST_REF_CLOCK        	CSL_SERDES_REF_CLOCK_100M /* Ref clock of serdes */
  #define SERDES_DIAG_TEST_LINK_RATE        	CSL_SERDES_LINK_RATE_8G /* Link rate of serdes */
  #define SERDES_DIAG_TEST_NUM_LANES        	1 /* Number of lanes to be tested */
  #define SERDES_DIAG_TEST_LANE_MASK        	0x1 /* All lanes are set */
  #define SERDES_DIAG_TEST_PHY_TYPE         	CSL_SERDES_PHY_TYPE_PCIe /* For running PCIe tests */
  #define SERDES_DIAG_TEST_PCIE_GEN_TYPE        SERDES_DIAG_PCIE_GEN3 /* For running PCIe tests */
  #define SERDES_DIAG_TEST_PRBS_PATTERN         SERDES_PRBS_7 /* prbs7 pattern */
  #define SERDES_DIAG_TEST_LANE_RATE        	CSL_SERDES_LANE_FULL_RATE /* Set to run at full rate of the link rate */
  #define SERDES_DIAG_TEST_LOOPBACK_MODE    	CSL_SERDES_LOOPBACK_NES /* For internal near end serial loopback tests */
  #define SERDES_DIAG_TEST_OPERATING_MODE   	CSL_SERDES_DIAGNOSTIC_MODE /* Should always be set to Diagnostic Mode for BER, EYE and PRBS tests */
  #define SERDES_DIAG_TEST_FORCEATTBOOST    	CSL_SERDES_FORCE_ATT_BOOST_DISABLED
  #define SERDES_DIAG_TEST_SWEEPTXRX            SERDES_DIAG_SWEEP_TX
#elif (serdes_diag_test_phy_type == TEST_SERDES_SGMII)
  #define SERDES_DIAG_TEST_BASE_ADDR        	CSL_SERDES0_BASE /* Serdes base address */
  #define SERDES_DIAG_TEST_REF_CLOCK        	CSL_SERDES_REF_CLOCK_156p25M  /* Ref clock of serdes */
  #define SERDES_DIAG_TEST_LINK_RATE        	CSL_SERDES_LINK_RATE_5G /* Set 5G for 1.25G, set 6p25G for 3.125G */
  #define SERDES_DIAG_TEST_NUM_LANES        	1 /* Number of lanes to be tested */
  #define SERDES_DIAG_TEST_LANE_MASK        	0x1 /* All lanes are set */
  #define SERDES_DIAG_TEST_PHY_TYPE         	CSL_SERDES_PHY_TYPE_SGMII /* For running SGMII tests */
  #define SERDES_DIAG_TEST_PRBS_PATTERN         SERDES_PRBS_7 /* prbs7 pattern */
  #define SERDES_DIAG_TEST_LANE_RATE        	CSL_SERDES_LANE_QUARTER_RATE /* Set to run at quarter rate for 1.25G and half rate for 3.125G */
  #define SERDES_DIAG_TEST_LOOPBACK_MODE    	CSL_SERDES_LOOPBACK_NES /* For internal near end serial loopback tests */
  #define SERDES_DIAG_TEST_OPERATING_MODE   	CSL_SERDES_DIAGNOSTIC_MODE /* Should always be set to Diagnostic Mode for BER, EYE and PRBS tests */
  #define SERDES_DIAG_TEST_FORCEATTBOOST    	CSL_SERDES_FORCE_ATT_BOOST_DISABLED
#endif
#endif
