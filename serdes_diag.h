/**  
 * @file serdes_diag.h
 *
 * @brief 
 *  Header file for functional layer of DIAGNOSTIC SERDES DIAG. 
 *
 *  It contains the various enumerations, structure definitions and function 
 *  declarations
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2015, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/** ============================================================================ 
 *
 * @defgroup SERDES_DIAG SERDES DIAG
 * @ingroup SERDES_DIAG_API
 *
 * @section Introduction
 *
 * @subsection xxx Overview
 * A bi-directional BER Test (TX and RX enabled at both ends) can be run across the various TX
 * filter coefficients (CM, C1 and C2) by using the Serdes_Diag_SweepCMC1C2 API in combination with
 * synchronizing the TX and RX using DSS. The Serdes Diag package contains serdes_dss.js which 
 * controls the TX/RX synchronization using DSS.   
 *
 * @subsection References
 *    
 * ============================================================================
 */   
#ifndef _DIAG_SERDES_H_
#define _DIAG_SERDES_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ti/csl/soc.h>
#include <ti/csl/csl.h>
#include <ti/csl/csl_serdes.h>
#include <ti/csl/csl_tsc.h>

#define SERDES_DIAG_MAX_COEFF_RANGE 512
#define SERDES_DIAG_MAX_LANES             4
#define SERDES_DIAG_MAX_DLY               128
#define SERDES_DIAG_MAX_ARRAY             256
#define SERDES_DIAG_AVERAGE_DELAY         100

#define SERDES_DIAG_CMU_MAP(base_addr, offset)            (base_addr + offset)
#define SERDES_DIAG_LANE_MAP(base_addr, lane_num, offset) ((base_addr) + 0x200*(lane_num + 1) + offset)
#define SERDES_DIAG_COMLANE_MAP(base_addr, offset)        (base_addr + 0xa00 + offset)

typedef struct SERDES_DIAG_CMC1C2
{
    uint32_t    cm[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    c1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    c2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint64_t    run_time[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    berlane0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    berlane1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    berlane2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    berlane3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    bist_valid0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    bist_valid1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    bist_valid2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    bist_valid3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    att0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    att1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    att2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    att3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    boost0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    pma0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    pma1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    pma2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    pma3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    cdr0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    cdr1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    cdr2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    cdr3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_0[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_1[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_2[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_0[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_1[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_2[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    dlevp0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevp1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevp2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevp3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    dlevn0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevn1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevn2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevn3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    dlevavg0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevavg1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevavg2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevavg3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dll_c0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_c1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_c2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_c3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dll_f0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_f1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_f2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_f3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     delay0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     delay1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     delay2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     delay3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     swing[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     deemph[SERDES_DIAG_MAX_COEFF_RANGE];
} SERDES_DIAG_CMC1C2_T;

typedef struct SERDES_DIAG_BER_VAL
{
    uint32_t    val[SERDES_DIAG_MAX_LANES];
    uint32_t    bist_valid[SERDES_DIAG_MAX_LANES];
    uint64_t    run_time[SERDES_DIAG_MAX_LANES];
    uint32_t    att[SERDES_DIAG_MAX_LANES];
    uint32_t    boost[SERDES_DIAG_MAX_LANES];
    uint32_t    pma[SERDES_DIAG_MAX_LANES];
    uint32_t    cdr[SERDES_DIAG_MAX_LANES];
    int32_t     dfe1[SERDES_DIAG_MAX_LANES];
    int32_t     dfe2[SERDES_DIAG_MAX_LANES];
    int32_t     dfe3[SERDES_DIAG_MAX_LANES];
    int32_t     dfe4[SERDES_DIAG_MAX_LANES];
    int32_t     dfe5[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe1[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe2[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe3[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe4[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe5[SERDES_DIAG_MAX_LANES];
    int32_t     dlevp[SERDES_DIAG_MAX_LANES];
    int32_t     dlevn[SERDES_DIAG_MAX_LANES];
    int32_t     dlevavg[SERDES_DIAG_MAX_LANES];
    int32_t     dll_c[SERDES_DIAG_MAX_LANES];
    int32_t     dll_f[SERDES_DIAG_MAX_LANES];
    int32_t     delay[SERDES_DIAG_MAX_LANES];
} SERDES_DIAG_BER_VAL_T;

typedef struct SERDES_DIAG_BOOST_ATT
{
    uint32_t    att[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    ber[SERDES_DIAG_MAX_COEFF_RANGE];
} SERDES_DIAG_BOOST_ATT_T;


typedef struct SERDES_DIAG_TX_COEFF
{
    uint32_t tx_att_start;
    uint32_t tx_att_end;
    uint32_t tx_vreg_start;
    uint32_t tx_vreg_end;
    uint32_t cm_coeff_start;
    uint32_t cm_coeff_end;
    uint32_t c1_coeff_start;
    uint32_t c1_coeff_end;
    uint32_t c2_coeff_start;
    uint32_t c2_coeff_end;
    uint32_t tx_swing_start;
    uint32_t tx_swing_end;
    uint32_t tx_deemph_start;
    uint32_t tx_deemph_end;
}SERDES_DIAG_TX_COEFF_T;

typedef struct SERDES_DIAG_EYE_OUTPUT
{
    
    uint32_t eye_width;
    
    uint32_t eye_height;
}SERDES_DIAG_EYE_OUTPUT_T;

typedef enum
{
    SERDES_DIAG_PCIE_GEN1 = 1,

    SERDES_DIAG_PCIE_GEN2 = 2
} SERDES_DIAG_PCIE_TYPE;

typedef enum
{
    
    SERDES_DIAG_SETUP_OK = 0,

    
    SERDES_DIAG_SETUP_TIME_OUT     = 1,

    
    SERDES_DIAG_INVALID_BUS_WIDTH      = 2,

    
    SERDES_DIAG_INVALID_PHY_TYPE       = 3
} SERDES_DIAG_STAT;

typedef enum
{
    
    SERDES_PRBS_7 = 0,

    
    SERDES_PRBS_15 = 1,

    
    SERDES_PRBS_23 = 2,

    
    SERDES_PRBS_31 = 3,

    
    SERDES_PRBS_UDP = 4,

    
    SERDES_PRBS_FEP_LPBK = 5
} SERDES_DIAG_PRBS_PATTERN;

typedef enum
{
    
    SERDES_DIAG_BIST_TX_CLK_DISABLED  =  0,
    
    
    SERDES_DIAG_BIST_TX_CLK_ENABLED   =  1
} SERDES_DIAG_BIST_TX_CLK;

typedef enum
{
    
    SERDES_DIAG_BIST_RX_CLK_DISABLED  =  0,
    
    
    SERDES_DIAG_BIST_RX_CLK_ENABLED   =  1
} SERDES_DIAG_BIST_RX_CLK;

typedef enum
{
    
    SERDES_DIAG_LOOPBACK  =  0,

    
    SERDES_DIAG_NON_LOOPBACK   =  1,

    
    SERDES_DIAG_FEP_LOOPBACK  = 2
} SERDES_DIAG_LOOPBACK_TYPE;

typedef struct SERDES_DIAG_BER_INIT
{
    
    uint32_t base_addr;
    
    uint32_t num_lanes;
    
    uint64_t bertesttime;
    
    SERDES_DIAG_PRBS_PATTERN prbs_pattern;
    
    uint64_t timeoutloops;
    
    csl_serdes_phy_type phy_type;
    
    SERDES_DIAG_PCIE_TYPE pcie_gen_type;
    
    SERDES_DIAG_BIST_TX_CLK bist_tx_clk_en;
    
    SERDES_DIAG_BIST_RX_CLK bist_rx_clk_en;
    
    SERDES_DIAG_LOOPBACK_TYPE loopback_type;
    
    uint32_t polarity[SERDES_DIAG_MAX_LANES];
    
    CSL_SERDES_LANE_CTRL_RATE lane_ctrl_rate[SERDES_DIAG_MAX_LANES];
    
    CSL_SERDES_TX_COEFF_T tx_coeff;
    
    uint8_t lane_mask;
} SERDES_DIAG_BER_INIT_T;

typedef struct SERDES_DIAG_EYE_INIT
{
    
    uint32_t base_addr;
    
    int32_t lane_num;
    
    int32_t num_lanes;
    
    int32_t phase_num;
    
    uint64_t gatetime;
    
    int32_t v_offset;
        
    int32_t t_offset;
        
    int32_t calibrate_eye;
        
    csl_serdes_phy_type phy_type;
        
    SERDES_DIAG_PCIE_TYPE pcie_gen_type;
    
    int32_t lane_mask;
    
    CSL_SERDES_LINK_RATE link_rate;
} SERDES_DIAG_EYE_INIT_T;


extern volatile uint32_t serdes_diag_dss_lock_key;
extern volatile uint32_t serdes_diag_dss_unlock_key;


extern volatile uint32_t serdes_diag_dss_stall_key0;

extern volatile uint32_t serdes_diag_dss_stall_key1;

extern volatile uint32_t serdes_diag_dss_stall_key2;

extern volatile uint32_t serdes_diag_dss_stall_flag_set;
extern volatile uint32_t serdes_diag_dss_stall_flag_clear;


extern volatile uint32_t serdes_diag_dss_stall_flag0;

extern volatile uint32_t serdes_diag_dss_stall_flag1;

extern volatile uint32_t serdes_diag_dss_stall_flag2;

extern volatile uint32_t serdes_diag_dss_test_complete_flag_set;

extern volatile uint32_t serdes_diag_dss_test_complete_flag;

extern volatile uint16_t serdes_diag_eye_scan_errors_array[SERDES_DIAG_MAX_ARRAY]
                                                         [SERDES_DIAG_MAX_DLY];

extern void Serdes_Diag_cycleDelay (uint64_t count);
extern void Serdes_Diag_Write_32_Mask(uint32_t base_addr,
                                     uint32_t mask_value,
                                     uint32_t set_value);




static inline SERDES_DIAG_STAT Serdes_Diag_UDPTest(const SERDES_DIAG_BER_INIT_T *ber_params_init)
{
    uint32_t lane_num;
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),5,5,0);
        }
    }

    
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),22,22,ber_params_init->bist_tx_clk_en);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),23,23,ber_params_init->bist_rx_clk_en);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),18,18,0);

            
            
            if (ber_params_init->phy_type == SERDES_PCIe)
            {
                
                if(ber_params_init->pcie_gen_type == SERDES_DIAG_PCIE_GEN1)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,1);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,0);
                }
                
                else if(ber_params_init->pcie_gen_type == SERDES_DIAG_PCIE_GEN2)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,1);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
                }
                
                else
                {
                    return SERDES_DIAG_INVALID_BUS_WIDTH;
                }
            }

            
            else if(ber_params_init->phy_type == SERDES_SGMII)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,0);
            }

            
            else if(ber_params_init->phy_type == SERDES_10GE)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,1);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
            }

            
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
            }
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),7,7,0);
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),27,27,0);
        }
    }

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),21,19,ber_params_init->prbs_pattern);
        }
    }

    
    
    
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(ber_params_init->base_addr, 0x14)),25,24,0);

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),31,24,(uint32_t)0x83);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 1,0,0x2);
        }
    }

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 17,8,0x17c);
        }
    }


    
    
    
    
    
    
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x38)) = 0xAAAAAAAA;
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x3c)), 7,0,170);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x40)) = 0;
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x44)), 27,0,0);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 4,2,0);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),30,28,0);
        }
    }

    
    Serdes_Diag_cycleDelay(50);

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 5,5,1);
        }
    }

    
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)), 7,6,2);
        }
    }
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)),22,20,0);
        }
    }

    
    Serdes_Diag_cycleDelay(50);

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 7,7,1);
        }
    }

    
    Serdes_Diag_cycleDelay(10000);
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x04)),2,1,0);
        }
    }
    Serdes_Diag_cycleDelay(10000);

    return SERDES_DIAG_SETUP_OK;
}



static inline void Serdes_Diag_SetFEPLoopback(const SERDES_DIAG_BER_INIT_T *ber_params_init)
{

      uint32_t lane_num;
      CSL_SERDES_DLEV_OUT_T pDLEVOut;
      CSL_SERDES_TAP_OFFSETS_T pTapOffsets;

      memset(&pDLEVOut, 0, sizeof(pDLEVOut));
      memset(&pTapOffsets, 0, sizeof(pTapOffsets));

      
      if(ber_params_init->phy_type == SERDES_10GE)
        {
    	  CSL_Serdes_PHYB_Init_Config(ber_params_init->base_addr,
                    SERDES_DIAG_MAX_LANES,
                    ber_params_init->phy_type,
                    ber_params_init->lane_mask,
                    &pTapOffsets);
        }

      
      if(ber_params_init->phy_type != SERDES_PCIe)
      {
          for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
          {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                 CSL_SerdesClearTXIdle(ber_params_init->base_addr, lane_num, ber_params_init->phy_type);
            }
          }
      }

      
      for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
      {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + 0x200*(lane_num+1) + 0x00),7,6,0);
        }
      }

      
      Serdes_Diag_cycleDelay(2000);


      
      for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
      {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
          CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + 0x200*(lane_num+1) + 0x00),22,20,5);
          CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + 0x200*(lane_num+1) + 0x00),23,23,1);
        }
      }

      if(ber_params_init->phy_type == SERDES_10GE)
      {
          CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + 0x200 + 0x98),14,14, 1); 
          CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + 0x200 + 0x98),14,14, 0); 
      }

      
      CSL_SerdesWaitForRXValid(ber_params_init->base_addr, SERDES_DIAG_MAX_LANES, ber_params_init->lane_mask, ber_params_init->phy_type);

      
      if(ber_params_init->phy_type != SERDES_10GE)
      {
    	  CSL_SerdesRXAttConfig_PhyA(ber_params_init->base_addr,
                                            SERDES_DIAG_MAX_LANES,
                                            ber_params_init->phy_type,
                                            ber_params_init->lane_mask);

            for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
            {
                if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                {
                	CSL_SerdesRXBoostConfig_PhyA(ber_params_init->base_addr, lane_num,
                            ber_params_init->phy_type,
                            ber_params_init->lane_mask);
                }
            }
        }
        
        else
        {
            for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
            {
                if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                {
                	CSL_SerdesRXAttBoostConfig_PhyB(ber_params_init->base_addr, lane_num,
                                                    ber_params_init->phy_type,
                                                    ber_params_init->lane_mask);
                }
            }
        }

        
        if(ber_params_init->phy_type == SERDES_10GE)
        {
            for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
            {
                if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                {
                	CSL_Serdes_RX_Calibration_PHYB(ber_params_init->base_addr,lane_num, ber_params_init->phy_type,
                                      &pTapOffsets,&pDLEVOut);
                }
              }
        }

}



static inline SERDES_DIAG_STAT Serdes_Diag_PRBS_Check(const SERDES_DIAG_BER_INIT_T *ber_params_init, uint32_t *lane_status)
{

    uint32_t lane_num, temp, bist_valid[SERDES_DIAG_MAX_LANES] = {0,0,0,0};
    uint64_t start_time;

    *lane_status = 0;

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x4)), 4,4,0x0);
        }
    }

    Serdes_Diag_cycleDelay(1000000);

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x4)), 3,3,0x1);
        }
    }

    start_time = CSL_tscRead();
    
    do
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                bist_valid[lane_num]= (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xC) & 0x0400) >> 10;

                if (((*(volatile uint32_t *)(ber_params_init->base_addr + 0x1fc0 + 0x34))&(1<<lane_num)) == 0)
                {
                    bist_valid[lane_num] = 0;
                }
            }

        }

        for(lane_num=0,temp=1;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                temp &= bist_valid[lane_num];
                *lane_status |= (bist_valid[lane_num] << lane_num);
            }
        }

        if ((CSL_tscRead() - start_time) > ber_params_init->timeoutloops) {
            break;
        }

    } while (!temp);

    return SERDES_DIAG_SETUP_OK;
}




static inline SERDES_DIAG_STAT Serdes_Diag_BERTest_TX(const SERDES_DIAG_BER_INIT_T *ber_params_init)
{
    uint32_t lane_num;

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),5,5,0);
        }
    }

    
    
    

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),
                                             22,22,ber_params_init->bist_tx_clk_en);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),
                                             23,23,ber_params_init->bist_rx_clk_en);

            if(ber_params_init->prbs_pattern == SERDES_PRBS_UDP)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),18,18,0);
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)), 18,18,1);
            }


            
            
            if (ber_params_init->phy_type == SERDES_PCIe)
            {
                
                if(ber_params_init->pcie_gen_type == SERDES_DIAG_PCIE_GEN1)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,1);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,0);
                }
                
                else if(ber_params_init->pcie_gen_type == SERDES_DIAG_PCIE_GEN2)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);

                    
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x50)),31,30,0);

                    
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x0C)),25,25,1);
                }
                
                else
                {
                    return SERDES_DIAG_INVALID_BUS_WIDTH;
                }
            }

            
            else if(ber_params_init->phy_type == SERDES_SGMII)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,0);
            }

            
            else if(ber_params_init->phy_type == SERDES_10GE)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,1);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
            }

            
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
            }
        }
    }
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),7,7,0);
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),27,27,0);
        }
    }

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),21,19,ber_params_init->prbs_pattern);
        }
    }

    
    
    
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(ber_params_init->base_addr, 0x14)),25,24,0);

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),31,24,(uint32_t)0x83);
        }
    }
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 1,0,0x2);
        }
    }

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 17,8,0x17c);
        }
    }


    
    
    
    
    
    
    
    
    if(ber_params_init->prbs_pattern == SERDES_PRBS_UDP)
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr +0x200*(lane_num+1)+0x30),18,18, 0);
                *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x38)) = 0xAAAAAAAA;
                CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr +0x200*(lane_num+1)+0x3C),7,0, 0xAA);
            }
        }
    }
    else
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x38)) = 0;
            }
        }

        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x3c)) = 0;
            }
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x40)) = 0;
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x44)), 27,0,0);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 4,2,0);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),30,28,0);
        }
    }

    
    Serdes_Diag_cycleDelay(50);

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 5,5,1);
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)), 23,23,0);
        }
    }

    
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)), 7,6,2);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)),22,20,0);
        }
    }

    
    Serdes_Diag_cycleDelay(50);

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 7,7,1);
        }
    }
    
    
    Serdes_Diag_cycleDelay(10000);
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x04)),2,1,0);
        }
    }
    Serdes_Diag_cycleDelay(10000);

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x04)), 6,3,((0)|(1<<1)|(2<<((ber_params_init->phy_type==SERDES_10GE)?3:2))));
        }
    }

    Serdes_Diag_cycleDelay(300000);

    return SERDES_DIAG_SETUP_OK;
}




static inline SERDES_DIAG_STAT Serdes_Diag_BERTest(const SERDES_DIAG_BER_INIT_T *ber_params_init,
                                                   SERDES_DIAG_BER_VAL_T *pBERval,
                                                   CSL_SERDES_TAP_OFFSETS_T *pTapOffsets)
{
    uint32_t lane_num,temp,any_lane_valid, stat;
    uint64_t llWaitTillTime;
    uint64_t llCurTscVal;
    uint32_t bist_chk_sync;
    CSL_SERDES_DLEV_OUT_T pDLEVOut;

    volatile uint32_t iDone;

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            pBERval->bist_valid[lane_num] = 0;
        }
    }
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),5,5,0);
        }
    }

    
    
    

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),22,22,ber_params_init->bist_tx_clk_en);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),23,23,ber_params_init->bist_rx_clk_en);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),18,18,1);

            
            
            if (ber_params_init->phy_type == SERDES_PCIe)
            {
                
                if(ber_params_init->pcie_gen_type == SERDES_DIAG_PCIE_GEN1)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,1);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,0);
                }
                
                else if(ber_params_init->pcie_gen_type == SERDES_DIAG_PCIE_GEN2)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
                    
                    
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x50)),29,28,0);

                    
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x50)),31,30,0);

                    
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x0C)),25,25,1);
                }
                else
                {
                
                    return SERDES_DIAG_INVALID_BUS_WIDTH;
                }
            }

            
            else if(ber_params_init->phy_type == SERDES_SGMII)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,0);
            }

            
            else if(ber_params_init->phy_type == SERDES_10GE)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,1);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
            }

            
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),16,16,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),6,6,1);
            }
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),7,7,0);
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),27,27,0);
        }
    }

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),21,19,ber_params_init->prbs_pattern);
        }
    }

    
    
    
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(ber_params_init->base_addr, 0x14)),25,24,0);

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x30)),31,24,(uint32_t)0x83);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 1,0,0x2);
        }
    }

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 17,8,0x17c);
        }
    }

    
    
    
    
    
    
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x38)) = 0;
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x3c)) = 0;
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x40)) = 0;
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x44)), 27,0,0);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 4,2,0);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)),30,28,0);
        }
    }

    
    Serdes_Diag_cycleDelay(50);

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 5,5,1);
        }
    }

    
    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)), 7,6,2);
        }
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)),22,20,0);
        }
    }

    
    Serdes_Diag_cycleDelay(50);

    if(ber_params_init->loopback_type == SERDES_DIAG_NON_LOOPBACK)
    {
        serdes_diag_dss_stall_flag2 = serdes_diag_dss_stall_flag_set;
        
        while(serdes_diag_dss_stall_key2 != serdes_diag_dss_unlock_key)
        {}
        
        serdes_diag_dss_stall_key2 = serdes_diag_dss_lock_key;
        
        serdes_diag_dss_stall_flag2 = serdes_diag_dss_stall_flag_clear;
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 7,7,1);
        }
    }
    
    
    Serdes_Diag_cycleDelay(10000);
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            
            if(ber_params_init->phy_type != SERDES_PCIe)
            CSL_Serdes_Force_Signal_Detect_Enable(ber_params_init->base_addr, lane_num);
        }
    }
    Serdes_Diag_cycleDelay(10000);

    
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x04)), 6,3,((0)|(1<<1)|(2<<((ber_params_init->phy_type==SERDES_10GE)?3:2))));
        }
    }

    Serdes_Diag_cycleDelay(300000); 
    
    llWaitTillTime = ber_params_init->timeoutloops;
    llWaitTillTime = CSL_tscRead() + ber_params_init->timeoutloops;

    if(ber_params_init->phy_type != SERDES_PCIe)
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                stat = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x2) & 0x020)>>5;

                while ((stat != 1) && (llWaitTillTime != 0))
                {
                    stat = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x2) & 0x020)>>5;

                    if(CSL_tscRead() > llWaitTillTime)
                    {
                        break;
                    } 
                }
            }
        }
    }
    else 
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                stat = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x2) & 0x008)>>3;

                while ((stat != 1) && (llWaitTillTime != 0))
                {
                    stat = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x2) & 0x008)>>3;

                    if(CSL_tscRead() > llWaitTillTime)
                    {
                        break;
                    } 
                }
            }
        }
    }
    
    
    if(ber_params_init->phy_type != SERDES_10GE)
    {
    	if(ber_params_init->phy_type != SERDES_PCIe)
    	{
    	    CSL_SerdesRXAttConfig_PhyA(ber_params_init->base_addr,
                                        ber_params_init->num_lanes,
                                        ber_params_init->phy_type,
                                        ber_params_init->lane_mask);

            for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
            {
                if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                {
            	    CSL_SerdesRXBoostConfig_PhyA(ber_params_init->base_addr, lane_num,
                        ber_params_init->phy_type,
                        ber_params_init->lane_mask);
                }
            }
    	}
    }
    else
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
            	CSL_SerdesRXAttBoostConfig_PhyB(ber_params_init->base_addr, lane_num,
                    ber_params_init->phy_type,
                    ber_params_init->lane_mask);
            }
        }
    }

    if(ber_params_init->phy_type == SERDES_10GE)
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
            	CSL_Serdes_RX_Calibration_PHYB(ber_params_init->base_addr,lane_num, ber_params_init->phy_type, pTapOffsets, &pDLEVOut);
            }
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x04)), 3,3,1);
        }
    }

    if(ber_params_init->loopback_type == SERDES_DIAG_NON_LOOPBACK)
    {
        serdes_diag_dss_stall_flag0 = serdes_diag_dss_stall_flag_set;
        
        while(serdes_diag_dss_stall_key0 != serdes_diag_dss_unlock_key)
        {}  
        
        serdes_diag_dss_stall_key0 = serdes_diag_dss_lock_key;
                
        serdes_diag_dss_stall_flag0 = serdes_diag_dss_stall_flag_clear;     
    }

    
    
    
    Serdes_Diag_cycleDelay(1000000);
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x04)), 4,4,0);
        }
    }

    
    
    llWaitTillTime = CSL_tscRead() + ber_params_init->timeoutloops;

    do
    {
        for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                pBERval->bist_valid[lane_num]= (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xC) & 0x0400) >> 10;

                if(ber_params_init->phy_type != SERDES_PCIe)
                {
                    if (((*(volatile uint32_t *)(ber_params_init->base_addr + 0x1fc0 + 0x34))&(1<<lane_num)) == 0)
                    {
                        pBERval->bist_valid[lane_num] = 0;
                    }
                }
            }

        }

        for(lane_num=0,temp=1;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
        {
            if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
            {
                temp &= pBERval->bist_valid[lane_num];
            }
        }

        llCurTscVal = CSL_tscRead();

    } while ((!temp) && (llWaitTillTime>llCurTscVal));

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            any_lane_valid |= pBERval->bist_valid[lane_num];
        }
    }

    
    pBERval->run_time[0] = CSL_tscRead();
    if (any_lane_valid)
    {
        Serdes_Diag_cycleDelay(ber_params_init->bertesttime);
    }

    llCurTscVal = CSL_tscRead();
    
    pBERval->run_time[0] = llCurTscVal - pBERval->run_time[0];

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            pBERval->pma[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x5) & 0x0ffc) >> 2;
            pBERval->cdr[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x6) & 0x07f8) >> 3;
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x04)), 3,3,0);
        }
    }

    if(ber_params_init->loopback_type == SERDES_DIAG_NON_LOOPBACK)
    {
        serdes_diag_dss_stall_flag1 = serdes_diag_dss_stall_flag_set;
        
        while(serdes_diag_dss_stall_key1 != serdes_diag_dss_unlock_key)
        {}  
        
        serdes_diag_dss_stall_key1 = serdes_diag_dss_lock_key;
            
        serdes_diag_dss_stall_flag1 = serdes_diag_dss_stall_flag_clear;     
    }

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x34)), 7,7,0);
        }
    }

    
    Serdes_Diag_cycleDelay(50);

    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            bist_chk_sync=(CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xc)) & 0x0400;

            while(bist_chk_sync != 0x0000)
            {
                bist_chk_sync=(CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xc)) & 0x0400;
            }
        }
    }

    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            if(ber_params_init->phy_type == SERDES_10GE)
            {
                if(pBERval->bist_valid[lane_num])
                {
                    pBERval->val[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xc) & 0x3ff) << 6;
                    pBERval->val[lane_num] |= (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xd) & 0x0fc0) >> 6;
                }
                else
                {
                    pBERval->val[lane_num] = 0x1ffff;
                }

                pBERval->att[lane_num] = CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x10);
                pBERval->boost[lane_num] = CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x10);
                pBERval->att[lane_num]   = (pBERval->att[lane_num]   >> 4) & 0x0f;
                pBERval->boost[lane_num] = (pBERval->boost[lane_num] >> 8) & 0x0f;

                pBERval->dfe1[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x16) & 0x00fe) >> 1;

                pBERval->dfe2[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x16) & 0x1) << 5;
                pBERval->dfe2[lane_num] |= (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x17) & 0x0f80) >> 7;

                pBERval->dfe3[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x17) & 0x07e) >> 1;

                pBERval->dfe4[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x17) & 0x01) << 5;
                pBERval->dfe4[lane_num] |= (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x18) & 0x0f80) >> 7;

                pBERval->dfe5[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x18) & 0x7e) >> 1;

                pBERval->dfe1[lane_num] = pBERval->dfe1[lane_num] - 64;
                pBERval->dfe2[lane_num] = pBERval->dfe2[lane_num] - 32;
                pBERval->dfe3[lane_num] = pBERval->dfe3[lane_num] - 32;
                pBERval->dfe4[lane_num] = pBERval->dfe4[lane_num] - 32;
                pBERval->dfe5[lane_num] = pBERval->dfe5[lane_num] - 32;


                pBERval->cdfe1[lane_num] = pDLEVOut.tap_values[lane_num][0];
                pBERval->cdfe2[lane_num] = pDLEVOut.tap_values[lane_num][1];
                pBERval->cdfe3[lane_num] = pDLEVOut.tap_values[lane_num][2];
                pBERval->cdfe4[lane_num] = pDLEVOut.tap_values[lane_num][3];
                pBERval->cdfe5[lane_num] = pDLEVOut.tap_values[lane_num][4];

                pBERval->dlevp[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x44) & 0xff0) >> 4;

                pBERval->dlevn[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x45) & 0x0ff);

                pBERval->dlevavg[lane_num] = (pBERval->dlevn[lane_num] - pBERval->dlevp[lane_num])/2;
                pBERval->delay[lane_num] = pDLEVOut.delay[lane_num];
            }
            else
            {
                if(pBERval->bist_valid[lane_num])
                {
                    pBERval->val[lane_num] = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xc) & 0x3ff) << 6;
                    pBERval->val[lane_num] |= (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0xd) & 0x0fc0) >> 6;
                }
                else
                {
                    pBERval->val[lane_num] = 0x1ffff;
                }

                pBERval->att[lane_num] = CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x11);
                pBERval->boost[lane_num] = CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x11);
                pBERval->att[lane_num]   = (pBERval->att[lane_num]   >> 4) & 0x0f;
                pBERval->boost[lane_num] = (pBERval->boost[lane_num] >> 8) & 0x0f;
            }
        }
    }


    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)), 7,7,0);
        }
    }
    
    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
    {
        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(ber_params_init->base_addr, lane_num, 0x00)),22,20,0);
        }
    }

    return SERDES_DIAG_SETUP_OK;
}




static inline void Serdes_Diag_SweepCMC1C2(const SERDES_DIAG_BER_INIT_T *ber_params_init,
                                            SERDES_DIAG_CMC1C2_T *pDiagCMC1C2, 
                                            CSL_SERDES_TAP_OFFSETS_T *pTapOffsets,
                                            const SERDES_DIAG_TX_COEFF_T *diag_tx_coeff)
{
    uint32_t cm, c1, c2, lane_num, count = 0;
    CSL_SERDES_RESULT diag_stat;
    SERDES_DIAG_BER_VAL_T pBERval;
    
    memset(&pBERval, 0, sizeof(pBERval));

    for (cm = diag_tx_coeff->cm_coeff_start; cm <= diag_tx_coeff->cm_coeff_end; cm++)
    { 
        for (c1 = diag_tx_coeff->c1_coeff_start; c1 <= diag_tx_coeff->c1_coeff_end; c1++)
        {
            for (c2 = diag_tx_coeff->c2_coeff_start; c2 <= diag_tx_coeff->c2_coeff_end; c2++)
            {
                CSL_Serdes_Assert_Reset(ber_params_init->base_addr, ber_params_init->phy_type, ber_params_init->lane_mask);
    
                for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
                {
                    if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                    {
                        CSL_SERDES_CONFIG_CM_C1_C2(ber_params_init->base_addr, lane_num, cm, c1, c2, ber_params_init->phy_type);
                    }
                }

                CSL_Serdes_Force_Signal_Detect_Low(ber_params_init->base_addr, SERDES_DIAG_MAX_LANES, ber_params_init->lane_mask);

                diag_stat = CSL_Serdes_Deassert_Reset(ber_params_init->base_addr, ber_params_init->phy_type, 1, SERDES_DIAG_MAX_LANES, ber_params_init->lane_mask);

                
                if (ber_params_init->phy_type == SERDES_10GE)
                {
                    CSL_SerdesWriteAverageOffsets(ber_params_init->base_addr,
                                                  SERDES_DIAG_MAX_LANES,
                                                  ber_params_init->lane_mask,
                                                  ber_params_init->phy_type, pTapOffsets);
                }

                if(ber_params_init->phy_type != SERDES_10GE)
                {
                    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
                    {
                        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                        {
                        	CSL_Serdes_PHYA_Init_Config(ber_params_init->base_addr, lane_num);
                        }
                    }
                }

                
                for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
                {
                    if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                    {
                        CSL_SerdesClearTXIdle(ber_params_init->base_addr, lane_num, ber_params_init->phy_type);
                    }
                }

                pDiagCMC1C2->cm[count] = cm;
                pDiagCMC1C2->c1[count] = c1;
                pDiagCMC1C2->c2[count] = c2;

                if(diag_stat == SERDES_DIAG_SETUP_OK)
                {

                    Serdes_Diag_BERTest(ber_params_init, &pBERval,pTapOffsets);

                    pDiagCMC1C2->run_time[count] = pBERval.run_time[0];
                    
                    for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
                    {
                        if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                        {
                        switch(lane_num)
                        {
                            case 0:
                            
                            pDiagCMC1C2->berlane0[count]     = pBERval.val[lane_num];
                            pDiagCMC1C2->bist_valid0[count]  = pBERval.bist_valid[lane_num];
                            pDiagCMC1C2->att0[count]         = pBERval.att[lane_num];
                            pDiagCMC1C2->boost0[count]       = pBERval.boost[lane_num];

                            pDiagCMC1C2->dfe1_0[count]       =  pBERval.dfe1[lane_num];
                            pDiagCMC1C2->dfe2_0[count]       =  pBERval.dfe2[lane_num];
                            pDiagCMC1C2->dfe3_0[count]       =  pBERval.dfe3[lane_num];
                            pDiagCMC1C2->dfe4_0[count]       =  pBERval.dfe4[lane_num];
                            pDiagCMC1C2->dfe5_0[count]       =  pBERval.dfe5[lane_num];

                            pDiagCMC1C2->cdfe1_0[count]      = pBERval.cdfe1[lane_num];
                            pDiagCMC1C2->cdfe2_0[count]      = pBERval.cdfe2[lane_num];
                            pDiagCMC1C2->cdfe3_0[count]      = pBERval.cdfe3[lane_num];
                            pDiagCMC1C2->cdfe4_0[count]      = pBERval.cdfe4[lane_num];
                            pDiagCMC1C2->cdfe5_0[count]      = pBERval.cdfe5[lane_num];

                            pDiagCMC1C2->cdr0[count]         = pBERval.cdr[lane_num];
                            pDiagCMC1C2->pma0[count]         = pBERval.pma[lane_num];

                            pDiagCMC1C2->dlevp0[count]       = pBERval.dlevp[lane_num];
                            pDiagCMC1C2->dlevn0[count]       = pBERval.dlevn[lane_num];
                            pDiagCMC1C2->dlevavg0[count]     = pBERval.dlevavg[lane_num];
                            pDiagCMC1C2->delay0[count]       = pBERval.delay[lane_num];
                            break;
                        
                            case 1:
                            
                            pDiagCMC1C2->berlane1[count]     = pBERval.val[lane_num];
                            pDiagCMC1C2->bist_valid1[count]  = pBERval.bist_valid[lane_num];
                            pDiagCMC1C2->att1[count]         = pBERval.att[lane_num];
                            pDiagCMC1C2->boost1[count]       = pBERval.boost[lane_num];

                            pDiagCMC1C2->dfe1_1[count]       =  pBERval.dfe1[lane_num];
                            pDiagCMC1C2->dfe2_1[count]       =  pBERval.dfe2[lane_num];
                            pDiagCMC1C2->dfe3_1[count]       =  pBERval.dfe3[lane_num];
                            pDiagCMC1C2->dfe4_1[count]       =  pBERval.dfe4[lane_num];
                            pDiagCMC1C2->dfe5_1[count]       =  pBERval.dfe5[lane_num];

                            pDiagCMC1C2->cdfe1_1[count]      = pBERval.cdfe1[lane_num];
                            pDiagCMC1C2->cdfe2_1[count]      = pBERval.cdfe2[lane_num];
                            pDiagCMC1C2->cdfe3_1[count]      = pBERval.cdfe3[lane_num];
                            pDiagCMC1C2->cdfe4_1[count]      = pBERval.cdfe4[lane_num];
                            pDiagCMC1C2->cdfe5_1[count]      = pBERval.cdfe5[lane_num];

                            pDiagCMC1C2->cdr1[count]         = pBERval.cdr[lane_num];
                            pDiagCMC1C2->pma1[count]         = pBERval.pma[lane_num];

                            pDiagCMC1C2->dlevp1[count]       = pBERval.dlevp[lane_num];
                            pDiagCMC1C2->dlevn1[count]       = pBERval.dlevn[lane_num];
                            pDiagCMC1C2->dlevavg1[count]     = pBERval.dlevavg[lane_num];
                            pDiagCMC1C2->delay1[count]       = pBERval.delay[lane_num];
                            break;
                        
                            case 2:
                            
                            pDiagCMC1C2->berlane2[count]     = pBERval.val[lane_num];
                            pDiagCMC1C2->bist_valid2[count]  = pBERval.bist_valid[lane_num];
                            pDiagCMC1C2->att2[count]         = pBERval.att[lane_num];
                            pDiagCMC1C2->boost2[count]       = pBERval.boost[lane_num];

                            pDiagCMC1C2->dfe1_2[count]       =  pBERval.dfe1[lane_num];
                            pDiagCMC1C2->dfe2_2[count]       =  pBERval.dfe2[lane_num];
                            pDiagCMC1C2->dfe3_2[count]       =  pBERval.dfe3[lane_num];
                            pDiagCMC1C2->dfe4_2[count]       =  pBERval.dfe4[lane_num];
                            pDiagCMC1C2->dfe5_2[count]       =  pBERval.dfe5[lane_num];

                            pDiagCMC1C2->cdfe1_2[count]      = pBERval.cdfe1[lane_num];
                            pDiagCMC1C2->cdfe2_2[count]      = pBERval.cdfe2[lane_num];
                            pDiagCMC1C2->cdfe3_2[count]      = pBERval.cdfe3[lane_num];
                            pDiagCMC1C2->cdfe4_2[count]      = pBERval.cdfe4[lane_num];
                            pDiagCMC1C2->cdfe5_2[count]      = pBERval.cdfe5[lane_num];

                            pDiagCMC1C2->cdr2[count]         = pBERval.cdr[lane_num];
                            pDiagCMC1C2->pma2[count]         = pBERval.pma[lane_num];

                            pDiagCMC1C2->dlevp2[count]       = pBERval.dlevp[lane_num];
                            pDiagCMC1C2->dlevn2[count]       = pBERval.dlevn[lane_num];
                            pDiagCMC1C2->dlevavg2[count]     = pBERval.dlevavg[lane_num];
                            pDiagCMC1C2->delay2[count]       = pBERval.delay[lane_num];
                            break;
                        
                            case 3:
                            
                            pDiagCMC1C2->berlane3[count]     = pBERval.val[lane_num];
                            pDiagCMC1C2->bist_valid3[count]  = pBERval.bist_valid[lane_num];
                            pDiagCMC1C2->att3[count]         = pBERval.att[lane_num];
                            pDiagCMC1C2->boost3[count]       = pBERval.boost[lane_num];

                            pDiagCMC1C2->dfe1_3[count]       =  pBERval.dfe1[lane_num];
                            pDiagCMC1C2->dfe2_3[count]       =  pBERval.dfe2[lane_num];
                            pDiagCMC1C2->dfe3_3[count]       =  pBERval.dfe3[lane_num];
                            pDiagCMC1C2->dfe4_3[count]       =  pBERval.dfe4[lane_num];
                            pDiagCMC1C2->dfe5_3[count]       =  pBERval.dfe5[lane_num];

                            pDiagCMC1C2->cdfe1_3[count]      = pBERval.cdfe1[lane_num];
                            pDiagCMC1C2->cdfe2_3[count]      = pBERval.cdfe2[lane_num];
                            pDiagCMC1C2->cdfe3_3[count]      = pBERval.cdfe3[lane_num];
                            pDiagCMC1C2->cdfe4_3[count]      = pBERval.cdfe4[lane_num];
                            pDiagCMC1C2->cdfe5_3[count]      = pBERval.cdfe5[lane_num];

                            pDiagCMC1C2->cdr3[count]         = pBERval.cdr[lane_num];
                            pDiagCMC1C2->pma3[count]         = pBERval.pma[lane_num];

                            pDiagCMC1C2->dlevp3[count]       = pBERval.dlevp[lane_num];
                            pDiagCMC1C2->dlevn3[count]       = pBERval.dlevn[lane_num];
                            pDiagCMC1C2->dlevavg3[count]     = pBERval.dlevavg[lane_num];
                            pDiagCMC1C2->delay3[count]       = pBERval.delay[lane_num];
                            break;
                        }
                        }
                    }
                }
                else
                {
                    pDiagCMC1C2->run_time[count] = 0xdeadbeef;
                    pDiagCMC1C2->berlane0[count] = 0xdeadbeef;
                    pDiagCMC1C2->berlane1[count] = 0xdeadbeef;
                    pDiagCMC1C2->berlane2[count] = 0xdeadbeef;
                    pDiagCMC1C2->berlane3[count] = 0xdeadbeef;
                }

                count++;
            }   
        }
    }
}



static inline void Serdes_Diag_SweepPCIE(const SERDES_DIAG_BER_INIT_T *ber_params_init,
                                            SERDES_DIAG_CMC1C2_T *pDiagCMC1C2,
                                            CSL_SERDES_TAP_OFFSETS_T *pTapOffsets,
                                            const SERDES_DIAG_TX_COEFF_T *diag_tx_coeff)
{
    uint32_t swing, deemph, lane_num, tbus_data, count = 0;
    CSL_SERDES_RESULT diag_stat = SERDES_DIAG_SETUP_OK;
    SERDES_DIAG_BER_VAL_T pBERval;

    memset(&pBERval, 0, sizeof(pBERval));

    for (swing = diag_tx_coeff->tx_swing_start; swing <= diag_tx_coeff->tx_swing_end; swing++)
    {
        for (deemph = diag_tx_coeff->tx_deemph_start; deemph <= diag_tx_coeff->tx_deemph_end; deemph++)
        {
            
            for(lane_num = 0; lane_num < ber_params_init->num_lanes; lane_num++)
            {
                
                CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),27,27, swing);

                
                CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),28,28, deemph);
            }

            
            for(lane_num = 0; lane_num < ber_params_init->num_lanes; lane_num++)
            {
                if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                {
                    
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),19,19, 0x1);
                    
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),29,29, 0x1);
                    
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),15,15, 0x1);

                    
                    tbus_data = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x2) & 0x10) >> 4;

                    while ( (tbus_data & 0x001 ) == 0x001)
                    {
                        tbus_data = (CSL_SerdesReadSelectedTbus(ber_params_init->base_addr, lane_num+1, 0x2) & 0x10) >> 4;
                    }

                    
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),19,19, 0x1);
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),16,16, 0x1);

                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),19,19, 0x1);
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),16,16, 0x1);

                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),19,19, 0x1);
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),16,16, 0x1);

                    
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),19,19, 0x0);
                    CSL_FINSR(*(volatile uint32_t *)(ber_params_init->base_addr + (lane_num * 0x200) + 0x200 + 0x028),16,16, 0x1);
                }

            }

            pDiagCMC1C2->swing[count] = swing;
            pDiagCMC1C2->deemph[count] = deemph;

            if(diag_stat == SERDES_DIAG_SETUP_OK)
            {

                Serdes_Diag_BERTest(ber_params_init, &pBERval,pTapOffsets);

                pDiagCMC1C2->run_time[count] = pBERval.run_time[0];

                for(lane_num=0;lane_num<SERDES_DIAG_MAX_LANES;lane_num++)
                {
                    if ((ber_params_init->lane_mask & (1<<lane_num))!=0)
                    {
                    switch(lane_num)
                    {
                        case 0:
                        
                        pDiagCMC1C2->berlane0[count]     = pBERval.val[lane_num];
                        pDiagCMC1C2->bist_valid0[count]  = pBERval.bist_valid[lane_num];
                        pDiagCMC1C2->att0[count]         = pBERval.att[lane_num];
                        pDiagCMC1C2->boost0[count]       = pBERval.boost[lane_num];

                        pDiagCMC1C2->dfe1_0[count]       =  pBERval.dfe1[lane_num];
                        pDiagCMC1C2->dfe2_0[count]       =  pBERval.dfe2[lane_num];
                        pDiagCMC1C2->dfe3_0[count]       =  pBERval.dfe3[lane_num];
                        pDiagCMC1C2->dfe4_0[count]       =  pBERval.dfe4[lane_num];
                        pDiagCMC1C2->dfe5_0[count]       =  pBERval.dfe5[lane_num];

                        pDiagCMC1C2->cdfe1_0[count]      = pBERval.cdfe1[lane_num];
                        pDiagCMC1C2->cdfe2_0[count]      = pBERval.cdfe2[lane_num];
                        pDiagCMC1C2->cdfe3_0[count]      = pBERval.cdfe3[lane_num];
                        pDiagCMC1C2->cdfe4_0[count]      = pBERval.cdfe4[lane_num];
                        pDiagCMC1C2->cdfe5_0[count]      = pBERval.cdfe5[lane_num];

                        pDiagCMC1C2->cdr0[count]         = pBERval.cdr[lane_num];
                        pDiagCMC1C2->pma0[count]         = pBERval.pma[lane_num];

                        pDiagCMC1C2->dlevp0[count]       = pBERval.dlevp[lane_num];
                        pDiagCMC1C2->dlevn0[count]       = pBERval.dlevn[lane_num];
                        pDiagCMC1C2->dlevavg0[count]     = pBERval.dlevavg[lane_num];
                        pDiagCMC1C2->delay0[count]       = pBERval.delay[lane_num];
                        break;

                        case 1:
                        
                        pDiagCMC1C2->berlane1[count]     = pBERval.val[lane_num];
                        pDiagCMC1C2->bist_valid1[count]  = pBERval.bist_valid[lane_num];
                        pDiagCMC1C2->att1[count]         = pBERval.att[lane_num];
                        pDiagCMC1C2->boost1[count]       = pBERval.boost[lane_num];

                        pDiagCMC1C2->dfe1_1[count]       =  pBERval.dfe1[lane_num];
                        pDiagCMC1C2->dfe2_1[count]       =  pBERval.dfe2[lane_num];
                        pDiagCMC1C2->dfe3_1[count]       =  pBERval.dfe3[lane_num];
                        pDiagCMC1C2->dfe4_1[count]       =  pBERval.dfe4[lane_num];
                        pDiagCMC1C2->dfe5_1[count]       =  pBERval.dfe5[lane_num];

                        pDiagCMC1C2->cdfe1_1[count]      = pBERval.cdfe1[lane_num];
                        pDiagCMC1C2->cdfe2_1[count]      = pBERval.cdfe2[lane_num];
                        pDiagCMC1C2->cdfe3_1[count]      = pBERval.cdfe3[lane_num];
                        pDiagCMC1C2->cdfe4_1[count]      = pBERval.cdfe4[lane_num];
                        pDiagCMC1C2->cdfe5_1[count]      = pBERval.cdfe5[lane_num];

                        pDiagCMC1C2->cdr1[count]         = pBERval.cdr[lane_num];
                        pDiagCMC1C2->pma1[count]         = pBERval.pma[lane_num];

                        pDiagCMC1C2->dlevp1[count]       = pBERval.dlevp[lane_num];
                        pDiagCMC1C2->dlevn1[count]       = pBERval.dlevn[lane_num];
                        pDiagCMC1C2->dlevavg1[count]     = pBERval.dlevavg[lane_num];
                        pDiagCMC1C2->delay1[count]       = pBERval.delay[lane_num];
                        break;

                        case 2:
                        
                        pDiagCMC1C2->berlane2[count]     = pBERval.val[lane_num];
                        pDiagCMC1C2->bist_valid2[count]  = pBERval.bist_valid[lane_num];
                        pDiagCMC1C2->att2[count]         = pBERval.att[lane_num];
                        pDiagCMC1C2->boost2[count]       = pBERval.boost[lane_num];

                        pDiagCMC1C2->dfe1_2[count]       =  pBERval.dfe1[lane_num];
                        pDiagCMC1C2->dfe2_2[count]       =  pBERval.dfe2[lane_num];
                        pDiagCMC1C2->dfe3_2[count]       =  pBERval.dfe3[lane_num];
                        pDiagCMC1C2->dfe4_2[count]       =  pBERval.dfe4[lane_num];
                        pDiagCMC1C2->dfe5_2[count]       =  pBERval.dfe5[lane_num];

                        pDiagCMC1C2->cdfe1_2[count]      = pBERval.cdfe1[lane_num];
                        pDiagCMC1C2->cdfe2_2[count]      = pBERval.cdfe2[lane_num];
                        pDiagCMC1C2->cdfe3_2[count]      = pBERval.cdfe3[lane_num];
                        pDiagCMC1C2->cdfe4_2[count]      = pBERval.cdfe4[lane_num];
                        pDiagCMC1C2->cdfe5_2[count]      = pBERval.cdfe5[lane_num];

                        pDiagCMC1C2->cdr2[count]         = pBERval.cdr[lane_num];
                        pDiagCMC1C2->pma2[count]         = pBERval.pma[lane_num];

                        pDiagCMC1C2->dlevp2[count]       = pBERval.dlevp[lane_num];
                        pDiagCMC1C2->dlevn2[count]       = pBERval.dlevn[lane_num];
                        pDiagCMC1C2->dlevavg2[count]     = pBERval.dlevavg[lane_num];
                        pDiagCMC1C2->delay2[count]       = pBERval.delay[lane_num];
                        break;

                        case 3:
                        
                        pDiagCMC1C2->berlane3[count]     = pBERval.val[lane_num];
                        pDiagCMC1C2->bist_valid3[count]  = pBERval.bist_valid[lane_num];
                        pDiagCMC1C2->att3[count]         = pBERval.att[lane_num];
                        pDiagCMC1C2->boost3[count]       = pBERval.boost[lane_num];

                        pDiagCMC1C2->dfe1_3[count]       =  pBERval.dfe1[lane_num];
                        pDiagCMC1C2->dfe2_3[count]       =  pBERval.dfe2[lane_num];
                        pDiagCMC1C2->dfe3_3[count]       =  pBERval.dfe3[lane_num];
                        pDiagCMC1C2->dfe4_3[count]       =  pBERval.dfe4[lane_num];
                        pDiagCMC1C2->dfe5_3[count]       =  pBERval.dfe5[lane_num];

                        pDiagCMC1C2->cdfe1_3[count]      = pBERval.cdfe1[lane_num];
                        pDiagCMC1C2->cdfe2_3[count]      = pBERval.cdfe2[lane_num];
                        pDiagCMC1C2->cdfe3_3[count]      = pBERval.cdfe3[lane_num];
                        pDiagCMC1C2->cdfe4_3[count]      = pBERval.cdfe4[lane_num];
                        pDiagCMC1C2->cdfe5_3[count]      = pBERval.cdfe5[lane_num];

                        pDiagCMC1C2->cdr3[count]         = pBERval.cdr[lane_num];
                        pDiagCMC1C2->pma3[count]         = pBERval.pma[lane_num];

                        pDiagCMC1C2->dlevp3[count]       = pBERval.dlevp[lane_num];
                        pDiagCMC1C2->dlevn3[count]       = pBERval.dlevn[lane_num];
                        pDiagCMC1C2->dlevavg3[count]     = pBERval.dlevavg[lane_num];
                        pDiagCMC1C2->delay3[count]       = pBERval.delay[lane_num];
                        break;
                    }
                    }
                }
            }
            else
            {
                pDiagCMC1C2->run_time[count] = 0xdeadbeef;
                pDiagCMC1C2->berlane0[count] = 0xdeadbeef;
                pDiagCMC1C2->berlane1[count] = 0xdeadbeef;
                pDiagCMC1C2->berlane2[count] = 0xdeadbeef;
                pDiagCMC1C2->berlane3[count] = 0xdeadbeef;
            }

            count++;
        }
    }
}



static inline void Serdes_Diag_EyeMonitor(char *fullfilename,
                                           SERDES_DIAG_EYE_INIT_T eye_init_params,
                                           SERDES_DIAG_EYE_OUTPUT_T *pEyeOutput)
{
    uint32_t comp_offset;
    uint32_t tbus_data;
    uint32_t eye_scan_errors;
    uint32_t array, delay, error_free = 0, max_error_free = 0, current_bin, previous_bin = 0;
    uint32_t count90=0, count=0, countmax=0, endvalue, inphase=0;
    uint32_t pcm_outs, pcm90, phase_min, phase_max;
    FILE *file;
    int iPhyA,iNotFourLaner;

    iNotFourLaner=(*(volatile uint32_t *)(eye_init_params.base_addr + 0x1fc0)>>16)&0x0ffff;
    iPhyA = (iNotFourLaner != 0x4eba);

    memset(&serdes_diag_eye_scan_errors_array, 0, sizeof(serdes_diag_eye_scan_errors_array));
    
    if (eye_init_params.v_offset==0)
    {
        eye_init_params.v_offset++;
    }
    if (eye_init_params.t_offset==0)
    {
        eye_init_params.t_offset++;
    }

    file = fopen(fullfilename,"w");

    if (eye_init_params.calibrate_eye == 1)
    {
        
        if(eye_init_params.phase_num == 1)
        {
            
            
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                     0xFF00FFFF, 0x00500000);
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30),
                                     0xFFFFFF00, 0x00000010);
            
        }
        else
        {
            
            
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                     0xFF00FFFF, 0x00900000);
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30),
                                     0xFFFFFF00, 0x00000008);
        }

        
        for (array = 0; array < SERDES_DIAG_MAX_DLY; array++)
        {
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                     0x00FFFFFF, array << 24);
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30),
                                     0xFFFFFFFC, array >> 8);
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30),
                                     0xFFFFFFFB, 0x00000004);
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30),
                                     0xFFFFFFFB, 0x00000000);

            count90 = 0;
            
            for (delay = 0; delay < SERDES_DIAG_AVERAGE_DELAY; delay++)
            {
                pcm_outs = CSL_SerdesReadSelectedTbus(eye_init_params.base_addr,
                                                      eye_init_params.lane_num+1,0x1D) & 0x0F;
                if (((pcm_outs & 0x4) >> 2) == 1)
                {
                    count90++;
                }
                
                Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30), 0xFFFFFFFB, 0x00000004);
                
                Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30), 0xFFFFFFFB, 0x00000000);
            }

            if (count90 >= 50)
            {
                pcm90 = 1;
            }
            else
            {
                pcm90 = 0;
            }

            
            if (pcm90 == 1)
            {
                inphase = 1;
                endvalue = array;
            }
            else
            {
                inphase = 0;
            }

            if (inphase == 1)
            {
                count++;
            }
            else
            {
                if (count > countmax)
                {
                    countmax  = count;
                    phase_max = endvalue;
                    phase_min = phase_max-count+1;
                }
                count=0;
            }
        }
    }
    else
    {
        phase_min = 0;
        phase_max = (SERDES_DIAG_MAX_DLY - 1);
    }

    
    if(eye_init_params.phase_num == 1)
    {
        
        
        
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                 0xFF00FFFF, 0x00700000);
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30),
                                 0xFFFFFF00, 0x00000051);
        
    }
    else
    {
        
        
        
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                 0xFF00FFFF, 0x00B00000);
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x30),
                                 0xFFFFFF00, 0x00000089);
    }

    
    
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                             0xFFFFFF00, 0x00000007);

    
    
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x0098), 0x00FFFFFF, 0x01000000);
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x009c), 0xFFFFFF00, 0x00000080);
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00b8), 0xFFFF00FF, 0x00004000);

    
    
    
    if(eye_init_params.phase_num == 1)
    {
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00b8), 0x0000FFFF, 0xFFEF0000);
        
    }
    else
    {
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00b8), 0x0000FFFF, 0xFFF70000);
    }
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00bc), 0xFFF00000, 0x000FFFFF);

    
    tbus_data = CSL_SerdesReadSelectedTbus(eye_init_params.base_addr,
                                           eye_init_params.lane_num+1,0x02);
    while ((tbus_data & 0x020 )!= 0x020)
    {
        tbus_data = CSL_SerdesReadTbusVal(eye_init_params.base_addr);
    }

    Serdes_Diag_cycleDelay(eye_init_params.gatetime);

    if(eye_init_params.phase_num == 1)
    {
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x008c),
                                 0xFF1FFFFF, 0x00000000 | (2<<21));
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00bc),
                                 0xFFBFFFFF, 0x00400000);
        comp_offset = (CSL_SerdesReadSelectedTbus(eye_init_params.base_addr,
        eye_init_params.lane_num+1,(iPhyA)?0x12:0x11)>>4)&0x0ff;
    }
    else
    {
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x008c), 0xFF1FFFFF, 0x00000000 | (4<<21));
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00bc), 0xFFBFFFFF, 0x00400000);
        comp_offset = (CSL_SerdesReadSelectedTbus(eye_init_params.base_addr,eye_init_params.lane_num+1,(iPhyA)?0x12:0x11)>>4)&0x0ff;
    }

    
    for (array = 0; array < SERDES_DIAG_MAX_ARRAY; array=array+eye_init_params.v_offset)
    {
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x009c), 0xFFFFFF00, array);
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                 0xFFFFFBFF, 0x00000400);
        
        Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                 0xFFFFFBFF, 0x00000000);

        
        for (delay = 0; delay < SERDES_DIAG_MAX_DLY; delay=delay+eye_init_params.t_offset)
        { 
            serdes_diag_eye_scan_errors_array[array][delay]=1;
        }

        for (delay = phase_min; delay <= phase_max; delay=delay+eye_init_params.t_offset)
        {
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                                     0x00FFFFFF, (delay & 0x0ff) << 24);
            Serdes_Diag_cycleDelay(5000);
            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00b8),
                                     0xFFFF00FF, 0x0000C000);

            
            Serdes_Diag_cycleDelay(500000);
            
            eye_scan_errors  = (CSL_SerdesReadSelectedTbus(eye_init_params.base_addr,
            eye_init_params.lane_num+1,(iPhyA)?0x1A:0x19) << 4);
            eye_scan_errors |= ((CSL_SerdesReadSelectedTbus(eye_init_params.base_addr,
            eye_init_params.lane_num+1,(iPhyA)?0x1B:0x1A) & 0x0F00) >> 8);

            serdes_diag_eye_scan_errors_array[array][delay] = eye_scan_errors;

            
            Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00b8),
                                     0xFFFF00FF, 0x00004000);
        }
    }

    
    for(array = 0; array < SERDES_DIAG_MAX_ARRAY; array=array+eye_init_params.v_offset)
    {
        for (delay = 0; delay < SERDES_DIAG_MAX_DLY; delay=delay+eye_init_params.t_offset)
        {
            fprintf(file,"%d ", serdes_diag_eye_scan_errors_array[array][delay]);
        }
        fprintf(file,"\n");
    }

    
    max_error_free = 0;
    
    for (array = 0; array < SERDES_DIAG_MAX_ARRAY; array=array+eye_init_params.v_offset)
    {
        error_free=current_bin=previous_bin=0;
        for (delay = 0; delay < SERDES_DIAG_MAX_DLY; delay=delay+eye_init_params.t_offset)
        {
            current_bin = serdes_diag_eye_scan_errors_array[array][delay];
            if (current_bin == 0)
            {
                if (previous_bin == current_bin)
                {
                    error_free++;
                    previous_bin = current_bin;
                    if (error_free > max_error_free)
                    {
                        max_error_free = error_free;
                    }
                }
                else
                {
                   error_free++;
                   previous_bin = current_bin;
                }
            }
            else
            {
                error_free = 0;
                previous_bin = current_bin;
            }
        }
    }

    pEyeOutput->eye_width = max_error_free*eye_init_params.t_offset*2;

    
    max_error_free = 0;
    for (delay = 0; delay < SERDES_DIAG_MAX_DLY; delay=delay+eye_init_params.t_offset)
    {
        error_free=current_bin=previous_bin=0;
        for (array = 0; array < SERDES_DIAG_MAX_ARRAY; array=array+eye_init_params.v_offset)
        {
            current_bin = serdes_diag_eye_scan_errors_array[array][delay];
            if (current_bin == 0)
            {
                if (previous_bin == current_bin)
                {
                    error_free++;
                    previous_bin = current_bin;
                    if (error_free > max_error_free)
                    {
                       max_error_free = error_free;
                    }
                }
                else
                {
                    error_free++;
                    previous_bin = current_bin;
                }
            }
            else
            {
                error_free = 0;
                previous_bin = current_bin;
            }
        }
    }

    pEyeOutput->eye_height = max_error_free*eye_init_params.v_offset*2;

    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x009c),
                             0xFFFFFF00, comp_offset);
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                             0xFFFFFBFF, 0x00000400);
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                             0xFFFFFBFF, 0x00000000);
    

    
    Serdes_Diag_Write_32_Mask(eye_init_params.base_addr+
                             0x200*(eye_init_params.lane_num+1)+0x0030, 0xFFFFFF00, 0x00000000);
    
    Serdes_Diag_Write_32_Mask(eye_init_params.base_addr+
                             0x200*(eye_init_params.lane_num+1)+0x002C, 0xFFFFFF00, 0x00000003);
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x0098),
                             0xFEFFFFFF, 0x00000000);
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_COMLANE_MAP(eye_init_params.base_addr, 0x00b8),
                             0xFFFF3FFF, 0x00000000);
    
    Serdes_Diag_Write_32_Mask(SERDES_DIAG_LANE_MAP(eye_init_params.base_addr, eye_init_params.lane_num, 0x2c),
                             0xFF3FFFFF, 0x00300000);

    fclose(file);
}




static inline void Serdes_Diag_Att_Boost_Calibration(const CSL_SERDES_LANE_ENABLE_PARAMS_T *serdes_lane_enable_params,
                                                      CSL_SERDES_RX_COEFF_T *pRxCoeff)
{
    uint32_t repeat_index, lane_num;
    int32_t att = 0, num_att = 0, boost = 0, num_boost = 0;
    int32_t att_array[CSL_SERDES_ATT_BOOST_NUM_REPEAT][CSL_SERDES_MAX_LANES];
    int32_t boost_array[CSL_SERDES_ATT_BOOST_NUM_REPEAT][CSL_SERDES_MAX_LANES];
    int32_t final_att[CSL_SERDES_MAX_LANES];
    int32_t final_boost[CSL_SERDES_MAX_LANES];    


    for(repeat_index=0; repeat_index<CSL_SERDES_ATT_BOOST_NUM_REPEAT; repeat_index++)
    {
        for(lane_num=0; lane_num<CSL_SERDES_MAX_LANES; lane_num++)
        {
            if ((serdes_lane_enable_params->lane_mask & (1<<lane_num))!=0)
            {
                
                CSL_SerdesWaitForRXValid(serdes_lane_enable_params->base_addr,
                                         CSL_SERDES_MAX_LANES,
                                         serdes_lane_enable_params->lane_mask,
                                         serdes_lane_enable_params->phy_type);

                CSL_SerdesRXAttBoostConfig(serdes_lane_enable_params->base_addr, lane_num,
                                    serdes_lane_enable_params->num_lanes,
                                    serdes_lane_enable_params->phy_type,
                                    serdes_lane_enable_params->lane_mask);

                
                att = boost = CSL_SerdesReadSelectedTbus(serdes_lane_enable_params->base_addr,
                                                     lane_num + 1,
                            (serdes_lane_enable_params->phy_type==SERDES_10GE)?0x10:0x11);
                att = (att >> 4) & 0x0f;
                boost = (boost >> 8) & 0x0f;
                att_array[repeat_index][lane_num] = att;
                boost_array[repeat_index][lane_num] = boost;
            }
        }

        
        CSL_Serdes_Force_Signal_Detect_Low(serdes_lane_enable_params->base_addr,
                                           CSL_SERDES_MAX_LANES,
                                           serdes_lane_enable_params->lane_mask);

        CSL_Serdes_CycleDelay(10000);

        
        for(lane_num=0; lane_num<CSL_SERDES_MAX_LANES; lane_num++)
        {
            if ((serdes_lane_enable_params->lane_mask & (1<<lane_num))!=0)
            {
                CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0x200*(lane_num+1) + 0x04),2,1, 0x0);
            }
        }
    }

    
    for(lane_num=0; lane_num<CSL_SERDES_MAX_LANES; lane_num++)
    {
        if ((serdes_lane_enable_params->lane_mask & (1<<lane_num))!=0)
        {
            att = boost = num_att = num_boost = 0;

            for(repeat_index=0; repeat_index<CSL_SERDES_ATT_BOOST_NUM_REPEAT; repeat_index++)
            {
                if((att_array[repeat_index][lane_num]>0) && 
                   (att_array[repeat_index][lane_num]<CSL_SERDES_ATT_BOOST_REPEAT_MEAN))
                {
                    att += att_array[repeat_index][lane_num];
                    num_att++;
                }

                if((boost_array[repeat_index][lane_num]>0) && 
                   (boost_array[repeat_index][lane_num]<CSL_SERDES_ATT_BOOST_REPEAT_MEAN))
                {
                boost += boost_array[repeat_index][lane_num];
                num_boost++;
                }
            }

        final_att[lane_num] = (((float)att)/num_att)+0.5;
        final_boost[lane_num] = (((float)boost)/num_boost)+0.5;
        }
    }

    
    
    for(lane_num=0; lane_num<CSL_SERDES_MAX_LANES; lane_num++)
    {
        if ((serdes_lane_enable_params->lane_mask & (1<<lane_num))!=0)
        {
            
            CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0x1fc0 + 0x20 + (lane_num*0x04)),15,15, 0x1);
            CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0x1fc0 + 0x20 + (lane_num*0x04)),14,13, 0x0);

            
            if (serdes_lane_enable_params->phy_type != SERDES_PCIe)
            {
                if (serdes_lane_enable_params->lane_ctrl_rate[lane_num] == CSL_SERDES_LANE_FULL_RATE)
                {
                    CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0xa00 + 0x84),10,10, 0x0); 
                }
                else if (serdes_lane_enable_params->lane_ctrl_rate[lane_num] == CSL_SERDES_LANE_HALF_RATE)
                {
                    CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0xa00 + 0x84),9,9, 0x0); 
                }
                else if (serdes_lane_enable_params->lane_ctrl_rate[lane_num] == CSL_SERDES_LANE_QUARTER_RATE)
                {
                   CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0xa00 + 0x84),8,8, 0x0); 
                }
            }

            if (att!=-1)
            {
                
                CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0xa00 + 0x84),0,0, 0x0); 
                CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0xa00 + 0x8c),24,24, 0x0); 
                CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0x200*(lane_num+1) + 0x8c),11,8, final_att[lane_num]); 
            }

            if (boost!=-1)
            {
                CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0xa00 + 0x84),1,1, 0x0); 
                CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0xa00 + 0x8c),25,25, 0x0); 
                CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0x200*(lane_num+1) + 0x8c),15,12, final_boost[lane_num]); 
            }

            
            CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0x1fc0 + 0x20 + (lane_num*0x04)),15,15, 0x1);
            CSL_FINSR(*(volatile uint32_t *)(serdes_lane_enable_params->base_addr + 0x1fc0 + 0x20 + (lane_num*0x04)),14,13, 0x3);

            CSL_Serdes_CycleDelay(100000);

            att = boost = CSL_SerdesReadSelectedTbus(serdes_lane_enable_params->base_addr,
                                lane_num + 1,
                                (serdes_lane_enable_params->phy_type==SERDES_10GE)?0x10:0x11);

            pRxCoeff->rx_att[lane_num]   = (att   >> 4) & 0x0f;
            pRxCoeff->rx_boost[lane_num] = (boost >> 8) & 0x0f;
       }
    }
}

#endif


