@REM ******************************************************************************
@REM * FILE PURPOSE: Environment Setup for building Serdes Batch File
@REM ******************************************************************************
@REM * FILE NAME: serdes_dss_ber_k2e.bat
@REM *
@REM * DESCRIPTION: 
@REM *  Configures and sets up the Build Environment for K2E BER Test. 
@REM *
@REM * USAGE:
@REM *  serdes_dss_ber_k2e.bat
@REM *
@REM * Copyright (C) 2015, Texas Instruments, Inc.
@REM *****************************************************************************
@echo off
@REM *******************************************************************************
@REM ********************** GET PARAMETERS PASSED THROUGH ARGUMENT   ***************
@REM *******************************************************************************
@REM Parameter Validation: Check if the argument was passed to the batch file and
@REM if so we use that else we default to the working directory where the batch 
@REM file was invoked from

if not defined PDK_INSTALL_PATH set PDK_INSTALL_PATH=..\..\..\
@echo PDK BUILD ENVIRONMENT CONFIGURED
echo PDK Directory %PDK_INSTALL_PATH%
@echo *******************************************************************************

C:\ti\ccsv7\ccs_base\scripting\bin\dss.bat serdes_dss_ber_k2e.js

GOTO DONE

:DONE



