/**  
 * @file serdes_diag.h
 *
 * @brief 
 *  Header file for functional layer of DIAGNOSTIC SERDES DIAG. 
 *
 *  It contains the various enumerations, structure definitions and function 
 *  declarations
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2019, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/** ============================================================================ 
 *
 * @defgroup SERDES_DIAG SERDES DIAG
 * @ingroup SERDES_DIAG_API
 *
 * @section Introduction
 *
 * @subsection xxx Overview
 * A bi-directional BER Test (TX and RX enabled at both ends) can be run across the various TX
 * filter coefficients (CM, C1 and C2) by using the Serdes_Diag_SweepCMC1C2 API in combination with
 * synchronizing the TX and RX using DSS. The Serdes Diag package contains serdes_dss.js which 
 * controls the TX/RX synchronization using DSS.   
 *
 * @subsection References
 *    
 * ============================================================================
 */   

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ti/csl/soc.h>
#include <ti/csl/csl.h>
#include <ti/csl/csl_serdes.h>
#include <ti/diag/serdes_diag/src/am65xx/serdes_diag_k3.h>

extern void CSL_serdesCycleDelay (uint64_t count);

/* Global Variables used for Synchronization */
volatile uint16_t serdes_diag_eye_scan_errors_array[SERDES_DIAG_MAX_ARRAY][SERDES_DIAG_MAX_DLY];

volatile uint32_t serdes_diag_dss_lock_key      = 0xDEADBEEF;
volatile uint32_t serdes_diag_dss_unlock_key    = 0xBEEFFACE;

/* start locked */
volatile uint32_t serdes_diag_dss_stall_key0    = 0xDEADBEEF;
/* start locked */
volatile uint32_t serdes_diag_dss_stall_key1    = 0xDEADBEEF;
/* start locked */
volatile uint32_t serdes_diag_dss_stall_key2    = 0xDEADBEEF;
/* start locked */
volatile uint32_t serdes_diag_dss_stall_key3    = 0xDEADBEEF;

volatile uint32_t serdes_diag_dss_stall_flag_set    = 0xCAFECAFE;
volatile uint32_t  serdes_diag_dss_stall_flag_clear = 0xCAFEDEAD;

/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag0      = 0xCAFEDEAD;   
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag1      = 0xCAFEDEAD;
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag2      = 0xCAFEDEAD;
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag3      = 0xCAFEDEAD;

volatile uint32_t  serdes_diag_dss_test_complete_flag_set   = 0xA5A5A5A5;
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_test_complete_flag       = 0x5A5A5A5A;

CSL_SerdesTbusDump      tbusLaneDump;
CSL_SerdesTbusDump      tbusCMUDump;

/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_SetFESLoopback
 *
 *   @b Description
 *   @n Function to set the Serdes in FEP Loopback mode
 *
 *   @b Arguments
 *   @verbatim
        baseAddr     Serdes IP base address
 *
 *   <b> Return Value  </b>
 *   @n  None
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim

    @endverbatim
 * ===========================================================================
 */
void Serdes_Diag_SetFESLoopback(uint32_t baseAddr, uint32_t laneNum)
{

   CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x54),16,16, (uint32_t)0x1);
}

/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_SetFEPLoopback
 *
 *   @b Description
 *   @n Function to set the Serdes in FEP Loopback mode
 *
 *   @b Arguments
 *   @verbatim
        baseAddr     Serdes IP base address
 *
 *   <b> Return Value  </b>
 *   @n  None
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim

    @endverbatim
 * ===========================================================================
 */
void Serdes_Diag_SetFEPLoopback(uint32_t baseAddr, uint32_t laneNum)
{

            /* rxclk_lb_ena_o to 1 */
            /* dmux_txb_sel_o_2_0 = 5 */
            /* ahb_tx_clk_brch1_div_sel_o = 0 */
            /* ahb_tx_clk_brch1_src_sel_o = 2 */
            /* ahb_tx_pcs_clk_div_en_o =0 */
            /* clock_gen_override_o = 1 */
            CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x54),18,18, (uint32_t)0x1);

            CSL_serdesCycleDelay(2000);

            CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x12c),28,26,5);
            CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x98),15,14,0);
            CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x0),2,0,2);
            CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x3c),24,24,1);
}

/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_SetENCLoopback
 *
 *   @b Description
 *   @n Function to set the Serdes in ENC Loopback mode
 *
 *   @b Arguments
 *   @verbatim
        baseAddr     Serdes IP base address
 *
 *   <b> Return Value  </b>
 *   @n  None
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim

    @endverbatim
 * ===========================================================================
 */
void Serdes_Diag_SetENCLoopback(uint32_t baseAddr, uint32_t laneNum)
{

    /*rx_src_o = 0x1
    ahb_rx_clk_brch1_src_sel_o = 1
    ahb_rx_clk_brch2_src_sel_o = 1
    ahb_rx_clk_brch3_src_sel_o = 3
    ahb_rx_clk_brch4_src_sel_o = 3
    clock_gen_override_o = 1 */

    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x12c),8,8,1);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x0),10,8,1);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x0),14,12,1);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x0),18,16,3);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x0),22,20,3);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x3c),24,24,1);
}

/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_SetFEP1p5Loopback
 *
 *   @b Description
 *   @n Function to set the Serdes in FEP1p5 Loopback mode
 *
 *   @b Arguments
 *   @verbatim
        baseAddr     Serdes IP base address
 *
 *   <b> Return Value  </b>
 *   @n  None
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim

    @endverbatim
 * ===========================================================================
 */
void Serdes_Diag_SetFEP1p5Loopback(uint32_t baseAddr, uint32_t laneNum)
{

    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x04c),23,22,0); //Mode = 0 to disable symbol alignment

    //    rxclk_lb_ena_o = 1
    //    dmux_txb_sel_o = 6
    //    ahb_tx_clk_brch1_div_sel_o = 0
    //    ahb_tx_clk_brch1_src_sel_o = 2
    //    clock_gen_override_o = 1
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x54),18,18, (uint32_t)0x1);
    CSL_serdesCycleDelay(2000);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x12c),28,26,6);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x98),15,14,0);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x0),2,0,2);
    CSL_FINSR(*(volatile uint32_t *)(baseAddr + 0x200*(laneNum+1) + 0x3c),24,24,1);
}

void Serdes_Diag_PCIeCtrlOverride(const SERDES_DIAG_BER_INIT_T *berParamsInit)
{

    int laneNum, stat;
    /* override value for ln_ctrl_i[25:0]:
     lnX_txdetectrx_lb_i(lnX_ctrl_i[0]) - 0x1(PIPE LOOPBACK - Put PCIe in Loopback once phystatus is done) - not needed,
     lnX_txelecidle_i(lnX_ctrl_i[1]) - 0x1(lnX_txelecidle_i must be asserted in P0s and P1),
     lnX_txswing_i(lnX_ctrl_i[7]) - 0x0(Full swing),
     lnX_block_align_control_i(lnX_ctrl_i[8]) - 0x1(Controls whether the PHY performs Block alignment in PCIe Gen3 mode),
     lnX_txstart_block_i(lnX_ctrl_i[9]) - 0x1(This signal allows the MAC to tell the PHY the starting byte for a 128b block) - not needed,
     lnX_txdata_valid_i(lnX_ctrl_i[11]) - 0x1(indicates the PHY will use the data),
     lnX_txdeemph_i(lnX_ctrl_i[34:17]) - Gen3:TxDeemph[5:0] - C-1 precursor coefficient */

//   CSL_serdesAssertReset(berParamsInit->baseAddr, berParamsInit->phyType, berParamsInit->laneMask);
//
//   CSL_SerdesConfigCMC1C2(berParamsInit->baseAddr, laneNum, cm,
//                          6, c2, berParamsInit->phyType);
//
//   CSL_serdesDeassertReset(berParamsInit->baseAddr,
//                                                 berParamsInit->phyType,
//                                                 berParamsInit->laneMask);
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 31, 6, (uint32_t)0x0010902);

            /* Override values for ln_ctrl_i[42:26] */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x144)), 15, 0, (uint32_t)0x0);

            /* override value for ln_ctrl_i[43] */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 20, 20, (uint32_t)0x0);

            /* override value for ln_ctrl_i[47] - lnX_rx_standby_i - Puts Rx into standby mode in P0 state */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 8, 8, (uint32_t)0x0);

            if (berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN3)
            {
                /* Set lnX_ctrl_i[46:44] PCIe clock freq of PCS-MAC to 250MHz */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 23,21,2);
                /* Set lnX_ctrl_i[49:48] bus width for 32 bit */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 25,24,2);
            }
            else if (berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN2)
            {
                /* Set lnX_ctrl_i[46:44] PCIe clock freq of PCS-MAC to 250MHz */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 23,21,2);
                /* Set lnX_ctrl_i[49:48] bus width for 32 bit */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 25,24,1);
            }
            else if (berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN1)
            {
                /* Set lnX_ctrl_i[46:44] PCIe clock freq of PCS-MAC to 250MHz */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 23,21,2);
                /* Set lnX_ctrl_i[49:48] bus width for 32 bit */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 25,24,0);
            }

            /* Set lnX_pd_i[2:0] to P1 for PCIe mode */
//            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 5,4,2);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 5,4,0);

            /* Set lnX_rate_i[1:0] for PCIe initial bringup rate */
//            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 3,2,0);

            /* Make sure ln_rstn is still in reset state 0x0 */
            do
            {
                stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x1) & 0x040)>>6;
            } while(stat == 1);

            /* Set lnX_rate_i[1:0] for PCIe Gen2 data rate */
            if (berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN3)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 3,2,2);
            }
            else if (berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN2)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 3,2,1);
            }
            else if (berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN1)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 3,2,0);
            }

            CSL_serdesEnableLanes(berParamsInit->baseAddr, laneNum);
            CSL_serdesCycleDelay(300000);

            /* override ln_rstn to 0x1 to bring it out of reset */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 1,1,1);

            /* Set Lane override enable to override all the settings */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 0,0,1);
            CSL_serdesCycleDelay(300000);

            /* check �ln_ok� status, for PCIe and USB we wait for de-assertion of �phystatus */
            do
            {
                stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x7) & 0x002)>>1;
            } while(stat == 1);

//            /* Set lnX_pd_i[2:0] to P0 for BER mode PCIe and USB only */
//            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 5,4,0);
//            CSL_serdesCycleDelay(300000);

//            /* Force Receiver in Reset to prevent adaptation */
//            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x13c)), 17,16,2); /* Reset Receiver */

            /* Add 300us delay */
            CSL_serdesCycleDelay(300000);
        }
    }
}

void Serdes_Diag_USBCtrlOverride(const SERDES_DIAG_BER_INIT_T *berParamsInit)
{
    int stat, laneNum;
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            /* override value for ln_ctrl_i[25:0]: */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 31, 6, (uint32_t)0x0000802);

            /* Override values for ln_ctrl_i[42:26] */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x144)), 15, 0, (uint32_t)0x0);

            /* override value for ln_ctrl_i[43] */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 20, 20, (uint32_t)0x0);

            /* override value for ln_ctrl_i[46:44] - lnX_pclk_rate_i - select 250Mhz for PCIe */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 23, 21, (uint32_t)0x2); //test 0x1 for 125MHz

            /* override value for ln_ctrl_i[47] - lnX_rx_standby_i - Puts Rx into standby mode in P0 state */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 8, 8, (uint32_t)0x0);

            /* Set lnX_ctrl_i[49:48] bus width for 16 bit */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 25,24,1);

            /* Set lnX_ctrl_i[56] and [51] */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 9,9,0x1);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x148)), 14,14,0x1);

            /* Set lnX_pd_i[2:0] to P2 for USB mode */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 5,4,3);

            /* Set lnX_rate_i[1:0] for USB initial bringup rate */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 3,2,1);

            /* FEP and FES specific settings */
            if (berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_FEP || berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_FES)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x3C)), 24,24,1); // clock gen override default 0
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x00)), 18,16,1); // RX_CLK_BRCH3_SRC_sel default 0
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x00)), 19,19,1); // RX_CLK_BRCH3_div_sel default 1
            }

            /* Make sure ln_rstn is still in reset state 0x0 */
            do
            {
                stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x1) & 0x040)>>6;
            } while(stat == 1);

            CSL_serdesEnableLanes(berParamsInit->baseAddr, laneNum);
            CSL_serdesCycleDelay(300000);

            /* override ln_rstn to 0x1 to bring it out of reset */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 1,1,1);

            /* Set Lane override enable to override all the settings */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 0,0,1);
            CSL_serdesCycleDelay(300000);

            /* check �ln_ok� status, for PCIe and USB we wait for de-assertion of �phystatus */
            do
            {
                stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x7) & 0x002)>>1;
            } while(stat == 1);

            /* Set lnX_pd_i[2:0] to P0 for BER mode PCIe and USB only */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 5,4,0);
            CSL_serdesCycleDelay(300000);

            /* PIPE loopback settings */
            if(berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_PIPE)
            {
                /* override value for ln_ctrl_i[0] for PIPE loopback */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 6, 6, (uint32_t)0x1);
            }
         }
    }
}

void Serdes_Diag_SGMIICtrlOverride(const SERDES_DIAG_BER_INIT_T *berParamsInit)
{
       int iTemp = berParamsInit->laneCtrlRate, stat, laneNum;
       if(iTemp ==2) iTemp = 3;

       for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
       {
           if ((berParamsInit->laneMask & (1<<laneNum))!=0)
           {
               /* Set ln_ctrl_i[3:0] bit 0 for 10b data */
               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 6,6,1);
               /* Set ln_ctrl_i[7:6] for 10b data */
               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 13,12,0);
               /* Set lnX_rate_i[1:0] for SGMII data rate */
               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 20,18,iTemp);
               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 3,2,iTemp);
               /* Set lnX_pd_i[2:0] to P0 for SGMII mode */
               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 5,4,0);

               /* Make sure ln_rstn is still in reset state 0x0 */
               do
               {
                   stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x1) & 0x040)>>6;
               } while(stat == 1);

            // CSL_serdesEnableLanes(berParamsInit->baseAddr, laneNum);
               CSL_serdesCycleDelay(300000);

               /* Set Lane override enable to override all the settings */
               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 1,0,1);
               CSL_serdesCycleDelay(300000);

               /* override ln_rstn to 0x1 to bring it out of reset */
               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 1,1,1);

               /* check �ln_ok� status, for PCIe and USB we wait for de-assertion of �phystatus */
               do
               {
                   stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x8) & 0x010)>>4;
               } while(stat == 0);

	       //Hardcoding AHB_TX_LN_DIV_SEL_O for quarter rate (only needed for quarter rate)
               if(iTemp == 3)
	       {
                   CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x1A0)), 31,29,0x7);
               }
	   }
       }
}


/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_BERTest_TX
 *
 *   @b Description
 *   @n This API is used for setting up the Transmitter PRBS generator.
 *
 *   @b Arguments
 *   @verbatim
     berParamsInit      Structure to the BER Initialization Parameters
 *
 *   <b> Return Value  </b>
 *   @n  None
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim

     Serdes_Diag_BERTest_TX(&berParamsInit);

    @endverbatim
 * ===========================================================================
 */
SERDES_DIAG_STAT Serdes_Diag_BERTestTX(const SERDES_DIAG_BER_INIT_T *berParamsInit)
{
    uint32_t laneNum, stat;

    /* Clear Bit 29 of lane 004 to 0 bist_gen_cdn */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)),29,29,0);
        }
    }

    /* Set 28 [bist_tx_clock_enable] of lane 004 to 1 */
    /* Set 31 [bist_rx_clock_enable] of lane 00c to 1 */
    /* Set 27 [bist_chk_data_mode] of lane 00c to 1 for PRBS pattern */

    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),28,28,berParamsInit->bistTxClkEn);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),31,31,berParamsInit->bistRxClkEn);
            if (berParamsInit->prbsPattern == SERDES_UDP)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),27,27,0); //UDP mode
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),27,27,1); //PRBS mode
            }

            /* [bist_gen_mode8B] */
            /* [bist_rate_o] */
            /* [bist_gen_word] */
            if (berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                if (berParamsInit->prbsPattern == SERDES_UDP || berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_TX_TO_BERT)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,0); //UDP mode
                }
                else
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,1); //PRBS mode
                }

                /* Set Bus Width to 8 bit interface for PCIe GEN 1 */
                if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN1)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,0);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,0);
                }
                /* Set BUS Width to 16 bit interface for PCIe GEN 2 */
                else if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN2)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,1);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,1);
                }
                /* Set BUS Width to 32 bit interface for PCIe GEN 3 */
                else if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN3)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,2);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,1);
                }
            }

            /* Set BUS Width to 10 bit interface for SGMII */
            else if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_SGMII)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,0);
            }

            /* Set BUS Width to 20 bit interface for USB */
            else if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,1);
            }
        }
    }

    /* Clear Bit 31 of lane 004 to 0 bist_gen_en_o */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)),31,31,0);
        }
    }

    /* Set Bit 29:28 to PRBS pattern on LANE 00c - Field 0-PRBS7, 1-PRBS15, 2-PRBS23, 3-PRBS31 */
    /* bist_chk_lfsr_length */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if (!(berParamsInit->prbsPattern == SERDES_UDP))
            {
		CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),29,28,berParamsInit->prbsPattern);
	    }
        }
    }

    /* SET BIT 9 and bit 8 of comlane 000 to 1 */
    /* 1 bist_gen_inv_prbs */
    /* 1 bist_chk_inv_prbs */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x0)),9,9,1);
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x0)),8,8,1);

    /* Set LANE 10 bits 9:0 to 0x283 */
    /* bist_chk_pream0 */
    /* Set LANE 10 bits 25:16 to 0x17C */
    /* bist_chk_pream1 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB || berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),9,0,0x1bc);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),25,16,0x1bc);
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),9,0,0x283);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),25,16,0x17c);
            }
        }
    }

    /*bist_chk_udp Lane 0x14 bits 31:0 and lane 0x18 bits 7:0 to 0x0 */
    /*bist_gen_en_low Lane 0x08 bits 23:8 to 0x0 */
    /*bist_gen_en_high Lane 0x08 and lane 0x0c bits 31:24, bits 7:0 to 0x0 */
    /*bist_chk_insert_word Lane 0x18 and lane 0x1c bits 31:8, bits 15:0 to 0x0 */
    /*bist_chk_insert_length Lane 0x10 bits 12:10 to 0x0 */
    /*bist_gen_insert_count Lane 0x08 bits 6:4 to 0x0 */
    /*bist_gen_insert_delay Lane 0xc bits 19:16 to 0x0 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if (berParamsInit->prbsPattern == SERDES_UDP)
            {
                *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x14)) = (uint32_t)0xAAAAAAAA;
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18)), 7,0,0xAA);
            }
            else
            {
                *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x14)) = (uint32_t)0x0;
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18)), 7,0,0x0);
            }

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x08)), 23,8,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x08)), 31,24,0x0);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 7,0,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18)), 31,8,0x0);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x1c)), 15,0,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)), 12,10,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)), 6,4,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0xc)), 19,8,0x0);
    }
    }

    /* Wait a minimum of 20ns let us put 50ns just in case */
    CSL_serdesCycleDelay(50);

    /* Step 4. Deassert bist_gen_cdn by setting 1 on bit 29 of lane 004 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)), 29,29,1);
        }
    }

    /* Step 5. Set Lane 0x12c */
    /* dmux_txa_sel Lane 0x12c [13:12] 0x3 */
    /* dmux_txb_sel Lane 0x12c [28:26] 0x0 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN1 || berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN2)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,3);
		    
                    if (berParamsInit->prbsPattern == SERDES_UDP || berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_TX_TO_BERT)
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
                    }
                    else
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,0);
                    }
                }
                else if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN3)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,2);
                    if (berParamsInit->prbsPattern == SERDES_UDP || berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_TX_TO_BERT)
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
                    }
                    else
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,0);
                    }
                }
            }
            else if (berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,3);
                if (berParamsInit->prbsPattern == SERDES_UDP || berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_TX_TO_BERT)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
                }
                else
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,0);
                }
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,2);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
            }
        }
    }

    /* Step 6a. Wait a minimum of 20ns let us put 50ns just in case */
    CSL_serdesCycleDelay(50);

    /* Step 6.b Assert bist_gen_en by setting bit 31 of lane 004 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)), 31,31,1);
        }
    }

    /* If NES loopback mode, set the NES bit
     * This is used for EYE diagram test in NES mode */
    if (berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_NES)
    {
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x54)),17,17, (uint32_t)0x1);
            }
        }
    }

    /* For USB mode, after asserting bist_gen_en, it begins to send the LFPS burst. Poll for bist_lfps_done */
    if (berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
    {
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 9, 9);
                while(stat !=1)
                {
                    stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 9, 9);
                }
            }
        }

        /* assert bist_gen_send_pream to begin sending training preamble */
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)),3,3,1);
            }

            /* poll for bist_pream_started */
            for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
            {
                if ((berParamsInit->laneMask & (1<<laneNum))!=0)
                {
                    stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 10, 10);
                    while(stat !=1)
                    {
                        stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 10, 10);
                    }
                }
            }
        }
    }

    /* Step 7. Wait a miniumum of 2us for the analog TX/RX paths to obtain the bit lock on to the training pattern */
    /* While this is ocurring set bchk_en lane 00c bit 21 to 0, bchk_clr bit 22 to 1, bchk_src bits 25:24 to 3 [ONLY FOR PHYB 6:5 is 0x3; else 0x2] */
    CSL_serdesCycleDelay(2000);
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 21,21,0);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 22,22,1);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 25,24,3);
    }
    }

    CSL_serdesCycleDelay(5000000); /* adding the rest of the delay ~ 30us */

    /* Clear bist_gen_send_pream to switch to transmitting PRBS data for USB */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                /* ahb_pma_ln_dr_sd_usb_mode = 0x1 */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x194)),11,10,1);
                /* qd_clk_lane_clk_sel_o = 0x1 */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x34)),20,20,1);

                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)),3,3,0);
            }
        }
    }

    return SERDES_DIAG_SETUP_OK;
}


/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_BERTest
 *
 *   @b Description
 *   @n This API is used for running the BER test for a specific duration.
 *
 *   @b Arguments
 *   @verbatim
     berParamsInit      Structure to the BER Initialization Parameters
     *pBERval             Pointer to the BER values structure
 *
 *   <b> Return Value  </b>
 *   @n  None
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim

     Serdes_Diag_BERTest(&berParamsInit, &pBERval);

    @endverbatim
 * ===========================================================================
 */
SERDES_DIAG_STAT Serdes_Diag_BERTest(const SERDES_DIAG_BER_INIT_T *berParamsInit,
                                                   SERDES_DIAG_BER_VAL_T *pBERval,
                                                   CSL_SerdesTapOffsets *pTapOffsets)
{
    uint32_t laneNum,temp,any_lane_valid=0, stat;
    uint64_t llWaitTillTime;
    uint64_t llCurTscVal;

    volatile uint32_t iDone;

    /* Clear Bist Valid */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            pBERval->bistValid[laneNum] = 0;
        }
    }
    
    /* Clear Bit 29 of lane 004 to 0 bist_gen_cdn */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->bistTxMode == SERDES_DIAG_TX_MODE_CONTINUOUS)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)),29,29,1);
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)),29,29,0);
            }
        }
    }

    /* Set 28 [bist_tx_clock_enable] of lane 004 to 1 */
    /* Set 31 [bist_rx_clock_enable] of lane 00c to 1 */
    /* Set 27 [bist_chk_data_mode] of lane 00c to 1 for PRBS pattern */

    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),28,28,berParamsInit->bistTxClkEn);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),31,31,berParamsInit->bistRxClkEn);
            if (berParamsInit->prbsPattern == SERDES_UDP)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),27,27,0); //UDP mode
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),27,27,1); //PRBS mode
            }

            /* [bist_gen_mode8B] */
            /* [bist_rate_o] */
            /* [bist_gen_word] */
            if (berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {

                if (berParamsInit->prbsPattern == SERDES_UDP)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,0); //UDP mode
                }
                else
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,1); //PRBS mode
                }

                /* Set Bus Width to 8 bit interface for PCIe GEN 1 */
                if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN1)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,0);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,0);
            	}
	            /* Set BUS Width to 16 bit interface for PCIe GEN 2 */
        	    else if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN2)
            	{
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,1);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,1);
                }
                /* Set BUS Width to 32 bit interface for PCIe GEN 3 */
                else if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN3)
            	{
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,2);
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,1);
                }
            }

            /* Set BUS Width to 10 bit interface for SGMII */
            else if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_SGMII)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,0);
            }

            /* Set BUS Width to 20 bit interface for USB */
            else if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                if (berParamsInit->prbsPattern == SERDES_UDP)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,0); //UDP mode
                }
                else
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),26,26,1); //PRBS mode
                }

                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),25,24,0);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x04)),30,30,1);
            }
    	}
    }

    /* Clear Bit 31 of lane 004 to 0 bist_gen_en_o */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(!(berParamsInit->bistTxMode == SERDES_DIAG_TX_MODE_CONTINUOUS))
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)),31,31,0);
            }
        }
    }

    /* SET BIT 3 [bist_gen_send_pream]of lane 008 to 0 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)),3,3,1);
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)),3,3,0);
            }
        }
    }

    /* Set Bit 29:28 to PRBS pattern on LANE 00c - Field 0-PRBS7, 1-PRBS15, 2-PRBS23, 3-PRBS31 */
    /* bist_chk_lfsr_length */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
	    if (!(berParamsInit->prbsPattern == SERDES_UDP))
            {
	        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)),29,28,berParamsInit->prbsPattern);
	    }
        }
    }

    /* SET BIT 9 and bit 8 of comlane 000 to 1 */
    /* 1 bist_gen_inv_prbs */
    /* 1 bist_chk_inv_prbs */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x0)),9,9,1);
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x0)),8,8,1);

    /* Set LANE 10 bits 9:0 to 0x283 */
    /* bist_chk_pream0 */
    /* Set LANE 10 bits 25:16 to 0x17C */
    /* bist_chk_pream1 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB || berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),9,0,0x1bc);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),25,16,0x1bc);
            }
            else
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),9,0,0x283);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)),25,16,0x17c);
            }
        }
    }

    /*bist_chk_udp Lane 0x14 bits 31:0 and lane 0x18 bits 7:0 to 0x0 */
    /*bist_gen_en_low Lane 0x08 bits 23:8 to 0x0 */
    /*bist_gen_en_high Lane 0x08 and lane 0x0c bits 31:24, bits 7:0 to 0x0 */
    /*bist_chk_insert_word Lane 0x18 and lane 0x1c bits 31:8, bits 15:0 to 0x0 */
    /*bist_chk_insert_length Lane 0x10 bits 12:10 to 0x0 */
    /*bist_gen_insert_count Lane 0x08 bits 6:4 to 0x0 */
    /*bist_gen_insert_delay Lane 0xc bits 19:16 to 0x0 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if (berParamsInit->prbsPattern == SERDES_UDP)
            {
                *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x14)) = (uint32_t)0xAAAAAAAA;
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18)), 7,0,0xAA);
            }
            else
            {
                *(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x14)) = (uint32_t)0x0;
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18)), 7,0,0x0);
            }

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x08)), 23,8,0x0);
            
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x08)), 31,24,0x0);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 7,0,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18)), 31,8,0x0);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x1c)), 15,0,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x10)), 12,10,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)), 6,4,0x0);

            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0xc)), 19,8,0x0);
	}
    }


    /*Force att and boost if flag is set */
    if (berParamsInit->forceAttBoost)
    {
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0xC)), 23,16,0x4C); /* calibration gen3 default FF*/
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x34)), 18,16,berParamsInit->forceAttVal); /* att Gen 3 default 2*/ //0
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x4c)), 3,0,berParamsInit->forceBoostVal); /* Gen 3 boost start default C*/
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x8)), 7,0,0x4C); /* csr_rxeq_init_run_rate3_o*/
    }

    /* Wait a minimum of 20ns let us put 50ns just in case */
    CSL_serdesCycleDelay(5000000);

    /* Step 4. Deassert bist_gen_cdn by setting 1 on bit 29 of lane 004 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)), 29,29,1);
        }
    }

    /* Step 5. Set Lane 0x12c */
    /* dmux_txa_sel Lane 0x12c [13:12] 0x3 */
    /* dmux_txb_sel Lane 0x12c [28:26] 0x0 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
	        {
                if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN1 || berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN2)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,3);
                    if (berParamsInit->prbsPattern == SERDES_UDP)
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
                    }
                    else
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,0);
                    }
                }
                else if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN3)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,2);
                    if (berParamsInit->prbsPattern == SERDES_UDP)
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
                    }
                    else
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,0);
                    }
                }
	        }
            else if (berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,3);

                if (berParamsInit->prbsPattern == SERDES_UDP)
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
                }
                else
                {
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,0);
                }
            }
	        else
	        {
		        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,2);
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,7);
	        }
        }
    }

    /* Step 6a. Wait a minimum of 20ns let us put 50ns just in case */
    CSL_serdesCycleDelay(5000000);

    /* Step 6.b Assert bist_gen_en by setting bit 31 of lane 004 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)), 31,31,1);
        }
    }

    /* If NES loopback mode, set the NES bit
     * This is used for EYE diagram test in NES mode */
    if (berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_NES)
    {
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x54)),17,17, (uint32_t)0x1);
            }
        }
    }

    /* For USB mode, after asserting bist_gen_en, it begins to send the LFPS burst. Poll for bist_lfps_done */
    if (berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
    {
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 9, 9);
                while(stat !=1)
                {
                    stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 9, 9);
                }
            }
        }

        /* assert bist_gen_send_pream to begin sending training preamble */
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)),3,3,1);
            }
        }

        /* poll for bist_pream_started */
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 10, 10);
                while(stat !=1)
                {
                    stat = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x040)), 10, 10);
                }
            }
        }
    }

    /* Step 7. Wait a miniumum of 2us for the analog TX/RX paths to obtain the bit lock on to the training pattern */
    /* While this is ocurring set bchk_en lane 00c bit 21 to 0, bchk_clr bit 22 to 1, bchk_src bits 25:24 to 3 [ONLY FOR PHYB 6:5 is 0x3; else 0x2] */
    CSL_serdesCycleDelay(500000);
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 21,21,0);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 22,22,1);
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 25,24,3);
	}
    }

    llWaitTillTime = berParamsInit->timeoutLoops;
//    llWaitTillTime = CSL_tscRead() + berParamsInit->timeoutLoops;

    CSL_serdesCycleDelay(500000); /* adding the rest of the delay ~ 30us */

    /*Step 9. Assert bchk_en by setting bit 21 to 1 of LANE 00c */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 21,21,1);
        }
    }

    /* Step 9A Toggle SIGDET Overlay so the we reset the receiver to the new BIST pattern */
    if(!(berParamsInit->loopbackType == CSL_SERDES_LOOPBACK_NES))
    {
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                CSL_serdesResetReceiver(berParamsInit->baseAddr, laneNum);
            }
        }
    }


    /* Read ln_stat_o RX Valid signal */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
            {
                stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x7) & 0x80)>>7;

                while ((stat != 1))// && (llWaitTillTime != 0))
                {
                    stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x7) & 0x80)>>7;
//                    llWaitTillTime--;
//
//                    if(llWaitTillTime==0)
//                    {
//                        break;
//                    } /*current time exceeded timeoutLoops offset */
                }
            }
            else if (berParamsInit->phyType == CSL_SERDES_PHY_TYPE_SGMII)
            {
                stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x7) & 0x10)>>4;

                while ((stat != 1))// && (llWaitTillTime != 0))
                {
                    stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x7) & 0x10)>>4;
//                    llWaitTillTime--;
//
//                    if(llWaitTillTime==0)
//                    {
//                        break;
//                    } /*current time exceeded timeoutLoops offset */
                }
            }
        }
    }

    /* Clear bist_gen_send_pream to switch to transmitting PRBS data for USB */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
            {
                /* ahb_pma_ln_dr_sd_usb_mode = 0x1 */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x194)),11,10,1);
                /* qd_clk_lane_clk_sel_o = 0x1 */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x34)),20,20,1);
                /* pcs_rate_o = 0x1 */
 //               CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_CMU_MAP(berParamsInit->baseAddr, 0x7c)),26,25,0);     //CODE_CHANGE Do not enable

                CSL_serdesCycleDelay(50000);
            }

            if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe || berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
            {
	        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x8)),3,3,0);
	    }
        }
    }

    /* Step 9B. Force an RXEQ re-calibration with PRBS data to ensure that sufficient
     * patterns are observed for the EBB
     */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x0)),6,6,1);
    CSL_serdesCycleDelay(300000);
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x0)),6,6,0);


    CSL_serdesTbusLaneDump(berParamsInit->baseAddr, &tbusLaneDump);
    CSL_serdesTbusCMUDump(berParamsInit->baseAddr, &tbusCMUDump);

    /*Step 10. The PHY will indicate that it has succesfully detected PRBS 
    when bist_rx_synch is asserted (read bit 1 of tbus data at address 0xAD) */
    /*Wait 20mS max. Anything greater than 10uS seems to hi ber anyway. */
    llWaitTillTime = berParamsInit->timeoutLoops;//CSL_tscRead() + berParamsInit->timeoutLoops;
    llCurTscVal = llWaitTillTime;
    uint8_t temp_counter = 0;

    do
    {
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
	    {
	        pBERval->bistValid[laneNum]= (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xAD) & 0x002) >> 1;
            
//                if (((*(volatile uint32_t *)(berParamsInit->baseAddr + 0x1fc0 + 0x34))&(1<<laneNum)) == 0)
//                {
//                    pBERval->bistValid[laneNum] = 0;
//                }
	    }
        }

        for(laneNum=0,temp=1;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
	        {
		        temp &= pBERval->bistValid[laneNum];
	        }
        }

        llCurTscVal--;// = CSL_tscRead();
        if(temp != 1)
        {
            if(temp_counter >10)
            {
                temp = 1;
            }
            CSL_serdesCycleDelay(300000);
            temp_counter = temp_counter + 1;
        }

    } while ((!temp) && llCurTscVal);//(llWaitTillTime>llCurTscVal));

    /* If bist_rx_synch = 1 for any of the lanes, then any_lane_valid is set 
    and we will go through the BER sweep for all lane*/
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            any_lane_valid |= pBERval->bistValid[laneNum];
	}
    }

    /*Step 8. Deassert bchk_clr by clearing bit 22 to 0 of LANE 00c */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x0c)), 22,22,0);
        }
    }

    /*11. Waiting for testtime to measure BER. 
    Don't wait for invalid lanes or if all lanes hit max errors */
//    pBERval->runTime[0] = CSL_tscRead();
    if (any_lane_valid)
    {
        CSL_serdesCycleDelay(berParamsInit->bertesttime);
    }

//    llCurTscVal = CSL_tscRead();
//    /*Capture run time for test */
//    pBERval->runTime[0] = llCurTscVal - pBERval->runTime[0];

//    CSL_SerdesTbusDump  pTbusDump;
//    CSL_serdesTbusDump(berParamsInit->baseAddr, &pTbusDump);

    /*Get the DLPF values */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            pBERval->pma[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x29) & 0x03ff) >> 0;
            pBERval->cdr[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x28) & 0x01fe) >> 1;
	}
    }

    /*14. Veriy that pBERval->val is 0; tbus address is 0C, bit locations are */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(pBERval->bistValid[laneNum])
            {
                pBERval->val[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xaf) & 0x00f) << 12;
		        pBERval->val[laneNum] |= (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xae) & 0xfff);
            }
            else
            {
                pBERval->val[laneNum] = 0x1ffff;
            }

            pBERval->att[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x40) & 0x070) >> 4;
            pBERval->boost[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x44) & 0x078) >> 3;
            pBERval->dfe1[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x4D) & 0x00ff) >> 0; //dfe slicer tap value
            pBERval->dfe2[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x4C) & 0x00ff) >> 0; //tap value scaled
            pBERval->dfe3[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x4A) & 0x007) >> 0; //dfe computation state
            pBERval->dfe4[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x4A) & 0x03F8) >> 3; //dfe computation dlev
            pBERval->dfe5[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x40) & 0x180) >> 7;  //gain

            pBERval->cdfe1[laneNum] = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x154)), 7, 0); //csr_dfe_dlev_a_i
            pBERval->cdfe2[laneNum] = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x154)), 15, 8); //csr_dfe_dlev_b_i
            pBERval->cdfe3[laneNum] = CSL_FEXTR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x170)), 7, 0); //csr_dfe_tap_val_rate3_i
        }
    }

    /* To verify bist works, insert bist errors intentionally */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)),27,27,1);
        }
    }

    CSL_serdesCycleDelay(50);
    temp = 1, temp_counter = 0;

    /* Within 50ns the PHY will indicate a successful exit from the BIST by deasserting bist_rx_synch_o.
     * At this point, bist_rx_errors_o is guaranteed to be static */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            pBERval->bistValidTemp[laneNum]= (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xAD) & 0x002) >> 1;
            while(pBERval->bistValidTemp[laneNum] !=1 && (!temp))
            {
                pBERval->bistValidTemp[laneNum]= (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xAD) & 0x002) >> 1;

                if(temp != 1)
                {
                    if(temp_counter >10)
                    {
                        temp = 1;
                    }
                    CSL_serdesCycleDelay(300000);
                    temp_counter = temp_counter + 1;
                }
            }
        }
    }

    /* Verify bist_rx_errors_0 is non-zero(check after BISt error insertion).
     * This verifies that no bit errors were measured during the testing sequence */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            pBERval->valTemp[laneNum] = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xae) & 0xfff);
            pBERval->valTemp[laneNum] |= (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xaf) & 0x00f) << 12;
        }
    }


    /*15. To return to normal mode set dmux_txa_sel and dmux_txb_sel to 0 
          by setting bits 13:12 of lane 12c and 28:26 of lane 12c respectively */
    /*12.  Deassert bchk_en by clearing bit 21 of LANE 00c */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x0c)), 21,21,0);
        }
    }

    /* Clear bist_gen_en by clearing bit 31 of lane 004 */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(!(berParamsInit->bistTxMode == SERDES_DIAG_TX_MODE_CONTINUOUS))
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)), 31,31,0);
            }
        }
    }

    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x4)),27,27,0); //turn off err gen
        }
    }

    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(!(berParamsInit->bistTxMode == SERDES_DIAG_TX_MODE_CONTINUOUS))
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)), 13,12,0);
            }
        }
    }
    
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            if(!(berParamsInit->bistTxMode == SERDES_DIAG_TX_MODE_CONTINUOUS))
            {
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x12c)),28,26,0);
            }
        }
    }

    return SERDES_DIAG_SETUP_OK;
}

void Serdes_Diag_SweepBER(const SERDES_DIAG_BER_INIT_T *berParamsInit,
                                            SERDES_DIAG_SWEEP_T *pDiagSweep,
                                            CSL_SerdesTapOffsets *pTapOffsets,
                                            const SERDES_DIAG_TX_COEFF_T *diag_tx_coeff)
{
    uint32_t laneNum, count = 0, sweep1, sweep2;
    SERDES_DIAG_BER_VAL_T pBERval;

    memset(&pBERval, 0, sizeof(pBERval));

    /* sweep through tx parameters and find the best BER range */
    for (sweep1 = berParamsInit->sweepStart1; sweep1 <= berParamsInit->sweepEnd1; sweep1++)
    {
        for (sweep2 = berParamsInit->sweepStart2; sweep2 <= berParamsInit->sweepEnd2; sweep2++)
        {
            for(laneNum = 0; laneNum < berParamsInit->numLanes; laneNum++)
            {
                if ((berParamsInit->laneMask & (1<<laneNum))!=0)
                {
                    if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_PCIe)
                    {
                        if(berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN1 || berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN2)
                        {
                            if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_TX)
                            {
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 23, 23, (uint32_t)sweep1);//c1);
                            }
                            else if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_RX)
                            {
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0xc)), 15, 8, 0x4c);
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x34)), 10, 8, (uint32_t)sweep1); //attenuation on sweep1
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x48)), 3, 0, (uint32_t)sweep2); //boost on sweep2
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x4)), 31, 24, 0x4c);
                            }
                        }
                        else if (berParamsInit->pcieGenType == SERDES_DIAG_PCIE_GEN3)
                        {
                            if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_TX)
                            {
                                printf("sweep CM, C1 presets are %d %d \n", SERDES_DIAG_PRESET_CM_MAP(sweep1), SERDES_DIAG_PRESET_C1_MAP(sweep1));
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 28, 23, (uint32_t)SERDES_DIAG_PRESET_CM_MAP(sweep1));
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x144)), 8, 3, (uint32_t)SERDES_DIAG_PRESET_C1_MAP(sweep1));
                            }
                            else if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_RX)
                            {
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0xc)), 23, 16, 0x4c);
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x34)), 18, 16, (uint32_t)sweep1); //attenuation on sweep1
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x4c)), 3, 0, (uint32_t)sweep2); //boost on sweep2
                                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x8)), 7, 0, 0x4c);
                            }

                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x6c)), 5, 3, 0x0);      //Default is 7      ahb_pma_ln_agc_thsel_gen3_o
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0xc)), 23, 16, 0x4f);
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x8)), 7, 0, 0x4f);
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x1C)), 15, 12, 0x0);           //Default is 1      csr_final_boost_adjust_val_o
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x88)), 23,22, 0x0);            //Default is 3      csr_rxeq_sr_rx_att_boost_norm_o
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr,  0x4c)), 3, 0, 0x9);            //boost on sweep2
                        }
                    }
                    if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
                    {
                        if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_TX)
                        {
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x140)), 23, 23, (uint32_t)sweep1);//c1
                        }
                        else if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_RX)
                        {
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0xC)), 7,0,0xC); /* calibration Rate1 default FF*/
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x34)), 2,0,sweep1); /* att Rate1 default 2*/ //0
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x44)), 3,0,sweep2); /* Rate1 boost start default C*/
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x4)), 23,16,0xC); /* csr_rxeq_init_run_rate1_o*/

                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0xC)), 15,8,0xC); /* calibration Rate2 default FF*/
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x34)), 10,8,sweep1); /* att Rate2 default 2*/ //0
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x48)), 3,0,sweep2); /* Rate2 boost start default C*/
                            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x4)), 31,24,0xC); /* csr_rxeq_init_run_rate2_o*/
                        }
                    }
                    if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_SGMII)
                    {
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x07C)), 15, 12, (uint32_t)SERDES_DIAG_PRESET_CM_MAP(sweep1));
                        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum,  0x07C)), 7, 3, (uint32_t)SERDES_DIAG_PRESET_C1_MAP(sweep1));
                    }
                }
            }

            CSL_serdesCycleDelay(200000);

            if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_TX)
            {
                if(berParamsInit->phyType == CSL_SERDES_PHY_TYPE_USB)
                {
                    pDiagSweep->sweep1[count] = sweep1;
                    pDiagSweep->sweep2[count] = sweep1;
                }
                else
                {
                    pDiagSweep->sweep1[count] = SERDES_DIAG_PRESET_CM_MAP(sweep1);
                    pDiagSweep->sweep2[count] = SERDES_DIAG_PRESET_C1_MAP(sweep1);
                }
            }
            else if(berParamsInit->sweepTxRx == SERDES_DIAG_SWEEP_RX)
            {
                pDiagSweep->sweep1[count] = sweep1;
                pDiagSweep->sweep2[count] = sweep2;
            }

            Serdes_Diag_BERTest(berParamsInit, &pBERval,pTapOffsets);

            pDiagSweep->runTime[count] = pBERval.runTime[0];

            for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
            {
                if ((berParamsInit->laneMask & (1<<laneNum))!=0)
                {
                    switch(laneNum)
                    {
                        case 0:

                        pDiagSweep->berlane0[count]     = pBERval.val[laneNum];
                        pDiagSweep->bistValid0[count]  = pBERval.bistValid[laneNum];
                        pDiagSweep->att0[count]         = pBERval.att[laneNum];
                        pDiagSweep->boost0[count]       = pBERval.boost[laneNum];

                        pDiagSweep->dfe1_0[count]       =  pBERval.dfe1[laneNum];
                        pDiagSweep->dfe2_0[count]       =  pBERval.dfe2[laneNum];
                        pDiagSweep->dfe3_0[count]       =  pBERval.dfe3[laneNum];
                        pDiagSweep->dfe4_0[count]       =  pBERval.dfe4[laneNum];
                        pDiagSweep->dfe5_0[count]       =  pBERval.dfe5[laneNum];

                        pDiagSweep->cdfe1_0[count]      = pBERval.cdfe1[laneNum];
                        pDiagSweep->cdfe2_0[count]      = pBERval.cdfe2[laneNum];
                        pDiagSweep->cdfe3_0[count]      = pBERval.cdfe3[laneNum];
                        pDiagSweep->cdfe4_0[count]      = pBERval.cdfe4[laneNum];
                        pDiagSweep->cdfe5_0[count]      = pBERval.cdfe5[laneNum];

                        pDiagSweep->cdr0[count]         = pBERval.cdr[laneNum];
                        pDiagSweep->pma0[count]         = pBERval.pma[laneNum];

                        pDiagSweep->dlevp0[count]       = pBERval.dlevp[laneNum];
                        pDiagSweep->dlevn0[count]       = pBERval.dlevn[laneNum];
                        pDiagSweep->dlevavg0[count]     = pBERval.dlevavg[laneNum];
                        pDiagSweep->delay0[count]       = pBERval.delay[laneNum];
                        break;

                        case 1:

                        pDiagSweep->berlane1[count]     = pBERval.val[laneNum];
                        pDiagSweep->bistValid1[count]  = pBERval.bistValid[laneNum];
                        pDiagSweep->att1[count]         = pBERval.att[laneNum];
                        pDiagSweep->boost1[count]       = pBERval.boost[laneNum];

                        pDiagSweep->dfe1_1[count]       =  pBERval.dfe1[laneNum];
                        pDiagSweep->dfe2_1[count]       =  pBERval.dfe2[laneNum];
                        pDiagSweep->dfe3_1[count]       =  pBERval.dfe3[laneNum];
                        pDiagSweep->dfe4_1[count]       =  pBERval.dfe4[laneNum];
                        pDiagSweep->dfe5_1[count]       =  pBERval.dfe5[laneNum];

                        pDiagSweep->cdfe1_1[count]      = pBERval.cdfe1[laneNum];
                        pDiagSweep->cdfe2_1[count]      = pBERval.cdfe2[laneNum];
                        pDiagSweep->cdfe3_1[count]      = pBERval.cdfe3[laneNum];
                        pDiagSweep->cdfe4_1[count]      = pBERval.cdfe4[laneNum];
                        pDiagSweep->cdfe5_1[count]      = pBERval.cdfe5[laneNum];

                        pDiagSweep->cdr1[count]         = pBERval.cdr[laneNum];
                        pDiagSweep->pma1[count]         = pBERval.pma[laneNum];

                        pDiagSweep->dlevp1[count]       = pBERval.dlevp[laneNum];
                        pDiagSweep->dlevn1[count]       = pBERval.dlevn[laneNum];
                        pDiagSweep->dlevavg1[count]     = pBERval.dlevavg[laneNum];
                        pDiagSweep->delay1[count]       = pBERval.delay[laneNum];
                        break;

                        case 2:

                        pDiagSweep->berlane2[count]     = pBERval.val[laneNum];
                        pDiagSweep->bistValid2[count]  = pBERval.bistValid[laneNum];
                        pDiagSweep->att2[count]         = pBERval.att[laneNum];
                        pDiagSweep->boost2[count]       = pBERval.boost[laneNum];

                        pDiagSweep->dfe1_2[count]       =  pBERval.dfe1[laneNum];
                        pDiagSweep->dfe2_2[count]       =  pBERval.dfe2[laneNum];
                        pDiagSweep->dfe3_2[count]       =  pBERval.dfe3[laneNum];
                        pDiagSweep->dfe4_2[count]       =  pBERval.dfe4[laneNum];
                        pDiagSweep->dfe5_2[count]       =  pBERval.dfe5[laneNum];

                        pDiagSweep->cdfe1_2[count]      = pBERval.cdfe1[laneNum];
                        pDiagSweep->cdfe2_2[count]      = pBERval.cdfe2[laneNum];
                        pDiagSweep->cdfe3_2[count]      = pBERval.cdfe3[laneNum];
                        pDiagSweep->cdfe4_2[count]      = pBERval.cdfe4[laneNum];
                        pDiagSweep->cdfe5_2[count]      = pBERval.cdfe5[laneNum];

                        pDiagSweep->cdr2[count]         = pBERval.cdr[laneNum];
                        pDiagSweep->pma2[count]         = pBERval.pma[laneNum];

                        pDiagSweep->dlevp2[count]       = pBERval.dlevp[laneNum];
                        pDiagSweep->dlevn2[count]       = pBERval.dlevn[laneNum];
                        pDiagSweep->dlevavg2[count]     = pBERval.dlevavg[laneNum];
                        pDiagSweep->delay2[count]       = pBERval.delay[laneNum];
                        break;

                        case 3:

                        pDiagSweep->berlane3[count]     = pBERval.val[laneNum];
                        pDiagSweep->bistValid3[count]  = pBERval.bistValid[laneNum];
                        pDiagSweep->att3[count]         = pBERval.att[laneNum];
                        pDiagSweep->boost3[count]       = pBERval.boost[laneNum];

                        pDiagSweep->dfe1_3[count]       =  pBERval.dfe1[laneNum];
                        pDiagSweep->dfe2_3[count]       =  pBERval.dfe2[laneNum];
                        pDiagSweep->dfe3_3[count]       =  pBERval.dfe3[laneNum];
                        pDiagSweep->dfe4_3[count]       =  pBERval.dfe4[laneNum];
                        pDiagSweep->dfe5_3[count]       =  pBERval.dfe5[laneNum];

                        pDiagSweep->cdfe1_3[count]      = pBERval.cdfe1[laneNum];
                        pDiagSweep->cdfe2_3[count]      = pBERval.cdfe2[laneNum];
                        pDiagSweep->cdfe3_3[count]      = pBERval.cdfe3[laneNum];
                        pDiagSweep->cdfe4_3[count]      = pBERval.cdfe4[laneNum];
                        pDiagSweep->cdfe5_3[count]      = pBERval.cdfe5[laneNum];

                        pDiagSweep->cdr3[count]         = pBERval.cdr[laneNum];
                        pDiagSweep->pma3[count]         = pBERval.pma[laneNum];

                        pDiagSweep->dlevp3[count]       = pBERval.dlevp[laneNum];
                        pDiagSweep->dlevn3[count]       = pBERval.dlevn[laneNum];
                        pDiagSweep->dlevavg3[count]     = pBERval.dlevavg[laneNum];
                        pDiagSweep->delay3[count]       = pBERval.delay[laneNum];
                        break;
                    }
                }
            }
            count++;
        }
    }
}

SERDES_DIAG_STAT Serdes_Diag_EYETest(const SERDES_DIAG_BER_INIT_T *berParamsInit, uint32_t odd, uint32_t x_step_size, uint32_t y_step_size)
{
    uint32_t laneNum, stat, x_offset;
    int x_range_top, y_range_top, x_offset_loop, y_offset_loop, ber_val, y_offset;

    // Calculate range(s) for setting BER array size
    x_range_top = 64/x_step_size;
    y_range_top = 176/y_step_size;

    FILE *file1;

    // NOTE: named this "flipped" since the {X,Y} are written in inverted order
    file1 = fopen("OnChipEye.txt","w");


    /* CSR_TXEQ_ADAPT_EN_O */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x34)),24,24,1);
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            /* AHB_MSM_PMA_LN_PD_SLICER_IMON_BIAS_O */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x17C)),7,6,0b01);
            /* AHB_MSM_PMA_LN_RST_SLICER_IMON_N_O */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x17C)),21,20,0b01);
            /* AHB_MSM_PMA_LN_RST_SLICER_IMON_N_O */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x17C)),21,20,0b11);
            /* AHB_MSM_PMA_LN_RST_S2P_IMON_ASYNC_O */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x17C)),31,30,0b01);
        }
    }

    /* Wait 1s (very approximately) */
    CSL_serdesCycleDelay(1000000);

    /* Poll for TBUS rxeq_ctrl_rxeq_done_i */
    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            do
            {
                stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x51) & 0x800)>>11;
            } while(stat == 0);
        }
    }

    // verify RXEQ finishing and RXSD status depending on the input argument; need the capability to skip
    // these steps for running eye monitor in NESLB
    if(berParamsInit->loopbackType != CSL_SERDES_LOOPBACK_NES)
    {
        /* Poll for TBUS pma_ln_rxsd_i */
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                do
                {
                    stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0x24) & 0x002)>>1;
                } while(stat == 0);
            }
        }
    }


    for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
    {
        if ((berParamsInit->laneMask & (1<<laneNum))!=0)
        {
            /* PMA_LN_EYE_DLY_OVR_EN_O */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0xFC)),29,29,1);
            /* PMA_LN_EYE_SGN_RST_OVR_EN_O */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0xFC)),30,30,1);
        }
    }

    // Input sanity checking for even/odd data eye
    if ((odd != 0) && (odd != 1))
    {
        printf("Error: Input \"odd\" must = 1 or 0\n");
        return SERDES_DIAG_SETUP_OK;
    }

    /* CSR_SLICER_EYE_O_EB_O */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMRXEQ_MAP(berParamsInit->baseAddr, 0x88)),7,7,odd);
    /* EYE_SCAN_WAIT_LEN_O_7_0 */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x34)),15,8,0x0);
    /* EYE_SCAN_WAIT_LEN_O_11_8 */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x34)),19,16,0x4);
    /* EYE_SCAN_COUNTER_EN_O */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),8,8,0x1);

    /* Set eye_scan_mask according to odd or even */
    if (odd == 0)
    {
        /* EYE_SCAN_MASK_O_7_0 */
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),23,16,0xFB);
        /* EYE_SCAN_MASK_O_15_8 */
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),31,24,0xFF);
        /* EYE_SCAN_MASK_O_19_16 */
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),31,24,0xF);
    }
    else
    {
        /* EYE_SCAN_MASK_O_7_0 */
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),23,16,0xFD);
        /* EYE_SCAN_MASK_O_15_8 */
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),31,24,0xFF);
        /* EYE_SCAN_MASK_O_19_16 */
        CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),31,24,0xF);
    }

    /* EYE_SCAN_EYE_DATA_SHIFT_O */
    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),13,10,0xA);

    // Top level {X,Y} loops start here
    for (x_offset_loop = 0; x_offset_loop < x_range_top; x_offset_loop++)
    {
        x_offset = x_offset_loop * x_step_size;
        for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
        {
            if ((berParamsInit->laneMask & (1<<laneNum))!=0)
            {
                /* PMA_LN_EYE_DLY_O_7_0 */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x64)),7,0,x_offset);
                /* PMA_LN_EYE_DLY_O_8_8 */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x64)),8,8,0x1);
                /* PMA_LN_EYE_SGN_RST_O */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x64)),10,10,0x1);
                /* PMA_LN_EYE_SGN_RST_O */
                CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x64)),10,10,0x0);
            }
        }

        for (y_offset_loop = 80/y_step_size; y_offset_loop < y_range_top; y_offset_loop++)
        {
            y_offset = y_offset_loop * y_step_size;
            for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
            {
                if ((berParamsInit->laneMask & (1<<laneNum))!=0)
                {
                    /* AHB_PMA_LN_DR_SLICER_HOLD_TAP_O */
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x184)),7,6,0b11);
                    /* AHB_PMA_LN_DR_SLICER_VSCAN_0_O */
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18C)),16,16,0b01);
                    /* AHB_PMA_LN_DR_SLICER_VSCAN_8_1_O */
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x18C)),15,8,y_offset);
                    /* AHB_PMA_LN_DR_SLICER_HOLD_TAP_O */
                    CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_LANE_MAP(berParamsInit->baseAddr, laneNum, 0x184)),7,6,0b01);
                }
            }

            /* EYE_SCAN_RUN_O = 0x1 */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),9,9,0x1);

            /* Poll for TBUS eye_scan_csr_eye_scan_cntr_ready_o */
            for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
            {
                if ((berParamsInit->laneMask & (1<<laneNum))!=0)
                {
                    do
                    {
                        stat = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xB2) & 0x001)>>0;
                    } while(stat == 0);
                }
            }

            /* EYE_SCAN_RUN_O = 0x0 */
            CSL_FINSR(*(volatile uint32_t *)(SERDES_DIAG_COMLANE_MAP(berParamsInit->baseAddr, 0x30)),9,9,0x0);
            for(laneNum=0;laneNum<SERDES_DIAG_MAX_LANES;laneNum++)
            {
                if ((berParamsInit->laneMask & (1<<laneNum))!=0)
                {
                    ber_val = (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xB1) & 0x0FF)<<12;
                    ber_val |= (CSL_serdesReadSelectedTbus(berParamsInit->baseAddr, laneNum+1, 0xB0) & 0xFFF);
                }
            }

            y_offset -= 80;
            if (y_offset < 0)
            {
                y_offset += 176;
            }

            // Print the raw BER array to the file
            fprintf(file1,"%d, ",ber_val);
        }

        // Newline as we increment the X offset
        fprintf(file1,"\n");

    }

    // Add an extra newline so we can see where each eye monitor print out has ended and manually parse it
    fprintf(file1,"\n");

    fclose(file1);

    return SERDES_DIAG_SETUP_OK;
}

/* @} */
