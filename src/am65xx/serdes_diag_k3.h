/**  
 * @file serdes_diag.h
 *
 * @brief 
 *  Header file for functional layer of DIAGNOSTIC SERDES DIAG. 
 *
 *  It contains the various enumerations, structure definitions and function 
 *  declarations
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2014, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/** ============================================================================ 
 *
 * @defgroup SERDES_DIAG SERDES DIAG
 * @ingroup SERDES_DIAG_API
 *
 * @section Introduction
 *
 * @subsection xxx Overview
 * A bi-directional BER Test (TX and RX enabled at both ends) can be run across the various TX
 * filter coefficients (CM, C1 and C2) by using the Serdes_Diag_SweepCMC1C2 API in combination with
 * synchronizing the TX and RX using DSS. The Serdes Diag package contains serdes_dss.js which 
 * controls the TX/RX synchronization using DSS.   
 *
 * @subsection References
 *    
 * ============================================================================
 */   
#ifndef _DIAG_SERDES_H_
#define _DIAG_SERDES_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ti/csl/soc.h>
#include <ti/csl/csl.h>
#include <ti/csl/csl_serdes.h>

#define SERDES_DIAG_MAX_COEFF_RANGE 512
#define SERDES_DIAG_MAX_LANES             1
#define SERDES_DIAG_MAX_DLY               128
#define SERDES_DIAG_MAX_ARRAY             256
#define SERDES_DIAG_AVERAGE_DELAY         100

static const uint8_t sweepcm[] =
{ 0, 0, 0, 0, 0, 4, 5, 7, 5, 6};
static const uint8_t sweepc1[] =
{ 9, 6, 7, 5, 0, 0, 0, 7, 5, 0};

#define SERDES_DIAG_PRESET_CM_MAP(preset) (sweepcm[preset])
#define SERDES_DIAG_PRESET_C1_MAP(preset) (sweepc1[preset])

#define SERDES_DIAG_CMU_MAP(baseAddr, offset)            (baseAddr + offset)
#define SERDES_DIAG_LANE_MAP(baseAddr, laneNum, offset) ((baseAddr) + 0x200*(laneNum + 1) + offset)
#define SERDES_DIAG_COMLANE_MAP(baseAddr, offset)        (baseAddr + 0xa00 + offset)
#define SERDES_DIAG_COMRXEQ_MAP(baseAddr, offset)        (baseAddr + 0x1400 + offset)

typedef struct SERDES_DIAG_SWEEP
{
    uint32_t    sweep1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    sweep2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    sweep3[SERDES_DIAG_MAX_COEFF_RANGE];
    uint64_t    runTime[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    berlane0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    berlane1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    berlane2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    berlane3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    bistValid0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    bistValid1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    bistValid2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    bistValid3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    att0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    att1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    att2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    att3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    boost0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    pma0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    pma1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    pma2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    pma3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    cdr0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    cdr1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    cdr2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    cdr3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_0[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_1[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_2[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dfe1_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe2_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe3_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe4_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dfe5_3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_0[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_1[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_2[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     cdfe1_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe2_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe3_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe4_3[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     cdfe5_3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    dlevp0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevp1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevp2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevp3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    dlevn0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevn1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevn2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevn3[SERDES_DIAG_MAX_COEFF_RANGE];

    uint32_t    dlevavg0[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevavg1[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevavg2[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    dlevavg3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dll_c0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_c1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_c2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_c3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     dll_f0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_f1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_f2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     dll_f3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     delay0[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     delay1[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     delay2[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     delay3[SERDES_DIAG_MAX_COEFF_RANGE];

    int32_t     swing[SERDES_DIAG_MAX_COEFF_RANGE];
    int32_t     deemph[SERDES_DIAG_MAX_COEFF_RANGE];
} SERDES_DIAG_SWEEP_T;

typedef struct SERDES_DIAG_BER_VAL
{
    uint32_t    val[SERDES_DIAG_MAX_LANES];
    uint32_t    valTemp[SERDES_DIAG_MAX_LANES];
    uint32_t    bistValid[SERDES_DIAG_MAX_LANES];
    uint32_t    bistValidTemp[SERDES_DIAG_MAX_LANES];
    uint64_t    runTime[SERDES_DIAG_MAX_LANES];
    uint32_t    att[SERDES_DIAG_MAX_LANES];
    uint32_t    boost[SERDES_DIAG_MAX_LANES];
    uint32_t    pma[SERDES_DIAG_MAX_LANES];
    uint32_t    cdr[SERDES_DIAG_MAX_LANES];
    int32_t     dfe1[SERDES_DIAG_MAX_LANES];
    int32_t     dfe2[SERDES_DIAG_MAX_LANES];
    int32_t     dfe3[SERDES_DIAG_MAX_LANES];
    int32_t     dfe4[SERDES_DIAG_MAX_LANES];
    int32_t     dfe5[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe1[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe2[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe3[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe4[SERDES_DIAG_MAX_LANES];
    int32_t     cdfe5[SERDES_DIAG_MAX_LANES];
    int32_t     dlevp[SERDES_DIAG_MAX_LANES];
    int32_t     dlevn[SERDES_DIAG_MAX_LANES];
    int32_t     dlevavg[SERDES_DIAG_MAX_LANES];
    int32_t     dllC[SERDES_DIAG_MAX_LANES];
    int32_t     dllF[SERDES_DIAG_MAX_LANES];
    int32_t     delay[SERDES_DIAG_MAX_LANES];
} SERDES_DIAG_BER_VAL_T;

typedef struct SERDES_DIAG_BOOST_ATT
{
    uint32_t    att[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    boost[SERDES_DIAG_MAX_COEFF_RANGE];
    uint32_t    ber[SERDES_DIAG_MAX_COEFF_RANGE];
} SERDES_DIAG_BOOST_ATT_T;

/** ============================================================================
 * @brief
 *
 *  SERDES PHY Transmitter Coefficients Structure */
typedef struct SERDES_DIAG_TX_COEFF
{
    uint32_t txAttStart;
    uint32_t txAttEnd;
    uint32_t txVregStart;
    uint32_t txVregEnd;
    uint32_t cmCoeffStart;
    uint32_t cmCoeffEnd;
    uint32_t c1CoeffStart;
    uint32_t c1CoeffEnd;
    uint32_t c2CoeffStart;
    uint32_t c2CoeffEnd;
    uint32_t txSwingStart;
    uint32_t txSwingEnd;
    uint32_t txDeemphStart;
    uint32_t txDeemphEnd;
    uint32_t presetStart;
    uint32_t presetEnd;
}SERDES_DIAG_TX_COEFF_T;

/** ============================================================================
 * @brief
 *
 *  SERDES PHY Receiver Coefficients Structure */
typedef struct SERDES_DIAG_RX_COEFF
{
    uint32_t rxAttStart;
    uint32_t rxAttEnd;
    uint32_t rxBoostStart;
    uint32_t rxBoostEnd;
}SERDES_DIAG_RX_COEFF_T;

typedef struct SERDES_DIAG_EYE_OUTPUT
{
    /**< Eye Width in pico seconds */
    uint32_t eyeWidth;
    /**< Eye Height in mV */
    uint32_t eyeHeight;
}SERDES_DIAG_EYE_OUTPUT_T;

typedef enum
{
    SERDES_DIAG_PCIE_GEN1 = 1,

    SERDES_DIAG_PCIE_GEN2 = 2,

    SERDES_DIAG_PCIE_GEN3 = 3
} SERDES_DIAG_PCIE_TYPE;

typedef enum
{
    SERDES_DIAG_SWEEP_TX = 1,

    SERDES_DIAG_SWEEP_RX = 2
} SERDES_DIAG_SWEEP_TX_RX;

typedef enum
{
    /** BER Setup OK */
    SERDES_DIAG_SETUP_OK = 0,

    /** BER Setup Timeout */
    SERDES_DIAG_SETUP_TIME_OUT     = 1,

    /* BER Invalid Bus Width Setup */
    SERDES_DIAG_INVALID_BUS_WIDTH      = 2,

    /* BER Invalid PHY TYPE */
    SERDES_DIAG_INVALID_PHY_TYPE       = 3
} SERDES_DIAG_STAT;

typedef enum
{
    /** PRBS 7 Sequence */
    SERDES_PRBS_7 = 0,

    /** PRBS 15 Sequence */
    SERDES_PRBS_15 = 1,

    /* PRBS 23 Sequence */
    SERDES_PRBS_23 = 2,

    /* PRBS 31 Sequence */
    SERDES_PRBS_31 = 3,

    /* UDP Pattern */
    SERDES_UDP = 4
} SERDES_DIAG_PRBS_PATTERN;

typedef enum
{
    /** BIST TX CLK Disable */
    SERDES_DIAG_BIST_TX_CLK_DISABLED  =  0,
    
    /** BIST TX CLK Enable */
    SERDES_DIAG_BIST_TX_CLK_ENABLED   =  1
} SERDES_DIAG_BIST_TX_CLK;

typedef enum
{
    /** BIST RX CLK Disable */
    SERDES_DIAG_BIST_RX_CLK_DISABLED  =  0,
    
    /** BIST RX CLK Enable */
    SERDES_DIAG_BIST_RX_CLK_ENABLED   =  1
} SERDES_DIAG_BIST_RX_CLK;

typedef enum
{
    SERDES_DIAG_TX_MODE_CONTINUOUS = 1,

    SERDES_DIAG_TX_MODE_NORMAL = 0
}SERDES_DIAG_TX_MODE;

typedef struct SERDES_DIAG_BER_INIT
{
    /**< Serdes Base Address */
    uint32_t baseAddr;
    /**< Number of lanes to be configured */
    uint32_t numLanes;
    /**< BER Test Duration */
    uint64_t bertesttime;
    /**< PRBS Pattern Generator used */
    SERDES_DIAG_PRBS_PATTERN prbsPattern;
    /**< Time Out value before test iteration exits */
    uint64_t timeoutLoops;
    /**< Serdes PHY Type enumerator */
    /**< PCIe Gen type Enumerator */
    CSL_SerdesPhyType phyType;
    SERDES_DIAG_PCIE_TYPE pcieGenType;
    /**< BIST Generator Transmit Enable */
    SERDES_DIAG_BIST_TX_CLK bistTxClkEn;
    /**< BIST Generator Receiver Enable */
    SERDES_DIAG_BIST_RX_CLK bistRxClkEn;
    /**< Type of BIST Test Setup */
    CSL_SerdesLoopback loopbackType;
    /**< Polarity */
    uint32_t polarity[SERDES_DIAG_MAX_LANES];
    /**< Lane Control Rate */
    CSL_SerdesLaneCtrlRate laneCtrlRate;
    /**< Lane Mask */
    uint8_t laneMask;
    /**< Force Attenuation Boost Flag */
    uint8_t forceAttBoost;
    /**< Force Attenuation Value */
    uint8_t forceAttVal;
    /**< Force Boost Value */
    uint8_t forceBoostVal;
    /**< Sweep TX or RX flag */
    SERDES_DIAG_SWEEP_TX_RX sweepTxRx;
    /**< Sweep Start value */
    uint32_t sweepStart1;
    /**< Sweep End Value */
    uint32_t sweepEnd1;
    /**< Sweep Start value */
    uint32_t sweepStart2;
    /**< Sweep End Value */
    uint32_t sweepEnd2;
    /**< Tx Mode Value */
    SERDES_DIAG_TX_MODE bistTxMode;
} SERDES_DIAG_BER_INIT_T;

typedef struct SERDES_DIAG_EYE_INIT
{
    /**< Serdes Base Address */
    uint32_t baseAddr;
    /**< Lane Number to be configured */
    int32_t laneNum;
    /**< Number of lanes to be configured */
    int32_t numLanes;
    /**< Serdes Phase Degree */
    int32_t phaseNum;
    /**< Serdes Test Gating Time */
    uint64_t gateTime;
    /**< Serdes voltage incremental offset */
    int32_t vOffset;
        /**< Serdes Time incremental offset */
    int32_t tOffset;
        /**< Serdes Eye Calibration Flag */
    int32_t calibrateEye;
        /**< Serdes PHY Type enumerator */
    CSL_SerdesPhyType phyType;
        /**< PCIe Gen type Enumerator */
    SERDES_DIAG_PCIE_TYPE pcieGenType;
    int32_t laneMask;
    CSL_SerdesLinkRate linkRate;
} SERDES_DIAG_EYE_INIT_T;


extern volatile uint32_t serdes_diag_dss_lock_key;
extern volatile uint32_t serdes_diag_dss_unlock_key;

/* start locked */
extern volatile uint32_t serdes_diag_dss_stall_key0;
/* start locked */
extern volatile uint32_t serdes_diag_dss_stall_key1;
/* start locked */
extern volatile uint32_t serdes_diag_dss_stall_key2;

extern volatile uint32_t serdes_diag_dss_stall_flag_set;
extern volatile uint32_t serdes_diag_dss_stall_flag_clear;

/* start with flag cleared */
extern volatile uint32_t serdes_diag_dss_stall_flag0;
/* start with flag cleared */
extern volatile uint32_t serdes_diag_dss_stall_flag1;
/* start with flag cleared */
extern volatile uint32_t serdes_diag_dss_stall_flag2;

extern volatile uint32_t serdes_diag_dss_test_complete_flag_set;
/* start with flag cleared */
extern volatile uint32_t serdes_diag_dss_test_complete_flag;

extern volatile uint16_t serdes_diag_eye_scan_errors_array[SERDES_DIAG_MAX_ARRAY]
                                                         [SERDES_DIAG_MAX_DLY];

extern void CSL_serdesCycleDelay (uint64_t count);
extern void Serdes_Diag_SetFESLoopback(uint32_t baseAddr, uint32_t laneNum);
extern void Serdes_Diag_SetFEPLoopback(uint32_t baseAddr, uint32_t laneNum);
extern void Serdes_Diag_SetFEP1p5Loopback(uint32_t baseAddr, uint32_t laneNum);
extern void Serdes_Diag_PCIeCtrlOverride(const SERDES_DIAG_BER_INIT_T *berParamsInit);
extern void Serdes_Diag_USBCtrlOverride(const SERDES_DIAG_BER_INIT_T *berParamsInit);
extern void Serdes_Diag_SGMIICtrlOverride(const SERDES_DIAG_BER_INIT_T *berParamsInit);
extern SERDES_DIAG_STAT Serdes_Diag_BERTestTX(const SERDES_DIAG_BER_INIT_T *berParamsInit);

extern SERDES_DIAG_STAT Serdes_Diag_BERTest(const SERDES_DIAG_BER_INIT_T *berParamsInit,
                                                   SERDES_DIAG_BER_VAL_T *pBERval,
                                                   CSL_SerdesTapOffsets *pTapOffsets);

extern void Serdes_Diag_SweepBER(const SERDES_DIAG_BER_INIT_T *berParamsInit,
                                            SERDES_DIAG_SWEEP_T *pDiagCMC1C2,
                                            CSL_SerdesTapOffsets *pTapOffsets,
                                            const SERDES_DIAG_TX_COEFF_T *diag_tx_coeff);
extern SERDES_DIAG_STAT Serdes_Diag_EYETest(const SERDES_DIAG_BER_INIT_T *berParamsInit,
                                            uint32_t odd,
                                            uint32_t x_step_size,
                                            uint32_t y_step_size);


#endif

/* @} */
