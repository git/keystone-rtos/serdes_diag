/*
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 *  File Name: serdes_diag.c
 *
 *  Processing/configuration functions for the Serdes Diag Utility.
 *
 */

#include <ti/diag/serdes_diag/serdes_diag.h>
#include <ti/csl/csl_serdes.h>

/* Global Variables used for Synchronization */
volatile uint16_t serdes_diag_eye_scan_errors_array[SERDES_DIAG_MAX_ARRAY][SERDES_DIAG_MAX_DLY];

volatile uint32_t serdes_diag_dss_lock_key      = 0xDEADBEEF;
volatile uint32_t serdes_diag_dss_unlock_key    = 0xBEEFFACE;

/* start locked */
volatile uint32_t serdes_diag_dss_stall_key0    = 0xDEADBEEF;
/* start locked */
volatile uint32_t serdes_diag_dss_stall_key1    = 0xDEADBEEF;
/* start locked */
volatile uint32_t serdes_diag_dss_stall_key2    = 0xDEADBEEF;
/* start locked */
volatile uint32_t serdes_diag_dss_stall_key3    = 0xDEADBEEF;

volatile uint32_t serdes_diag_dss_stall_flag_set    = 0xCAFECAFE;
volatile uint32_t  serdes_diag_dss_stall_flag_clear = 0xCAFEDEAD;

/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag0      = 0xCAFEDEAD;   
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag1      = 0xCAFEDEAD;
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag2      = 0xCAFEDEAD;
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_stall_flag3      = 0xCAFEDEAD;

volatile uint32_t  serdes_diag_dss_test_complete_flag_set   = 0xA5A5A5A5;
/* start with flag cleared */
volatile uint32_t  serdes_diag_dss_test_complete_flag       = 0x5A5A5A5A;

/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_getTimestamp
 *
 *   @b Description
 *   @n Get CPU Time Stamp.
 *
 *   @b Arguments
 *
 *   <b> Return Value  </b>
 *   @n  uint64_t 
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None 
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim
 
     Serdes_Diag_getTimestamp();

    @endverbatim
 * ===========================================================================
 */  
/*function that returns the cycle count */
uint64_t Serdes_Diag_getTimestamp()
{
#if defined(_TMS320C6X)
    return CSL_tscRead();
#elif defined(__ARMv7)
    /* arm_read_ccntr() */
    return ((unsigned long long)clock());
#endif
}

/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_cycleDelay
 *
 *   @b Description
 *   @n CPU Delay.
 *
 *   @b Arguments   count
 *
 *   <b> Return Value  </b>
 *   @n  None 
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None 
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim
 
     Serdes_Diag_cycleDelay(50);

    @endverbatim
 * ===========================================================================
 */       
void Serdes_Diag_cycleDelay (uint64_t count)
{
  uint64_t sat = 0;

  if (count <= 0)
  return;

  sat = Serdes_Diag_getTimestamp() + (uint64_t)count;

  while (Serdes_Diag_getTimestamp() < sat);
}

/** @addtogroup SERDES_DIAG
 @{ */
/** ============================================================================
 *   @n@b Serdes_Diag_Write_32_Mask
 *
 *   @b Description
 *   @n Write 32 bit using MASK values.
 *
 *   @b Arguments
 *   @verbatim
     base_addr      Serdes Base Address
     mask_value     Register bit fields to be masked
     set_value      Value to be set for the masked bits
 *
 *   <b> Return Value  </b>
 *   @n  None
 *
 *   <b> Pre Condition </b>
 *   @n  None
 *
 *   <b> Post Condition </b>
 *   @n  None
 *
 *   @b Reads
 *   @n None
 *
 *  <b> Usage Constraints: </b>
 *  @n  None
 *
 *   @b Example
 *   @verbatim

     Serdes_Diag_Write_32_Mask(CSL_HYPERLINK_0_SERDES_CFG_REGS, 0xFFFFFF00, 0x00000010);

    @endverbatim
 * ===========================================================================
 */
void Serdes_Diag_Write_32_Mask(uint32_t base_addr,
                              uint32_t mask_value,
                              uint32_t set_value)
{
  uint32_t temp;

  temp=(*(volatile uint32_t *)(base_addr));
  temp &= mask_value;
  temp |= ((~mask_value)&set_value);
  *(volatile uint32_t *)(base_addr) = temp;
}
