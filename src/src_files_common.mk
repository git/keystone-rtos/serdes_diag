
ifeq ($(SOC),$(filter $(SOC), k2h k2k k2l k2e k2g))

PACKAGE_SRCS_COMMON += src/serdes_diag.c
PACKAGE_SRCS_COMMON += serdes_diag.h

SRCDIR = . src src/..
INCDIR = . src src/..

endif

ifeq ($(SOC),$(filter $(SOC), am65xx))

PACKAGE_SRCS_COMMON += src/am65xx/serdes_diag_k3.h

SRCS_COMMON += serdes_diag_k3.c

SRCDIR = . src src/am65xx
INCDIR = . src src/am65xx

endif

