/******************************************************************************
 * FILE PURPOSE: Serdes Debug Test files.
 ******************************************************************************
 * FILE NAME: module.xs
 *
 * DESCRIPTION: 
 *  This file contains the module specification for Serdes Debug Unit Test
 *  Files
 *
 * Copyright (C) 2015, Texas Instruments, Inc.
 *****************************************************************************/

/* Load the library utility. */
var libUtility = xdc.loadCapsule ("../build/buildlib.xs");

var otherFiles = [
    "tools/ber_diagram_tool/BerDiagramViewer.jar",
    "tools/ber_diagram_tool/lib/BERViewerInterface.dll",
    "tools/ber_diagram_tool/lib/libBERViewerInterface.so",
    "tools/eye_monitor_tool/EyeDiagramViewer.jar",
    "tools/eye_monitor_tool/resources/ColorMapTable.txt",
    "tools/eye_monitor_tool/resources/EyeMaskSummary.txt",
];

/**************************************************************************
 * FUNCTION NAME : modBuild
 **************************************************************************
 * DESCRIPTION   :
 *  The function is used to add all the source files in the example 
 *  directory into the package.
 **************************************************************************/
function modBuild() 
{
    /* Add all the other files to the release package. */
    for (var k = 0 ; k < otherFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = otherFiles[k];
}


