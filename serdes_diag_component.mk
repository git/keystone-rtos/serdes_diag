#
# Copyright (c) 2019, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# File: serdes_diag_component.mk
#       This file is component include make file of Serdes Diag library.
# List of variables set in this file and their purpose:
# <mod>_RELPATH        - This is the relative path of the module, typically from
#                        top-level directory of the package
# <mod>_PATH           - This is the absolute path of the module. It derives from
#                        absolute path of the top-level directory (set in env.mk)
#                        and relative path set above
# <mod>_INCLUDE        - This is the path that has interface header files of the
#                        module. This can be multiple directories (space separated)
# <mod>_PKG_LIST       - Names of the modules (and sub-modules) that are a part
#                        part of this module, including itself.
# <mod>_BOARD_DEPENDENCY - "yes": means the code for this module depends on
#                             board and the compiled obj/lib has to be kept
#                             under <board> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no board dependent code and hence
#                             the obj/libs are not kept under <board> dir.
# <mod>_CORE_DEPENDENCY     - "yes": means the code for this module depends on
#                             core and the compiled obj/lib has to be kept
#                             under <core> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no core dependent code and hence
#                             the obj/libs are not kept under <core> dir.
# <mod>_APP_STAGE_FILES     - List of source files that belongs to the module
#                             <mod>, but that needs to be compiled at application
#                             build stage (in the context of the app). This is
#                             primarily for link time configurations or if the
#                             source file is dependent on options/defines that are
#                             application dependent. This can be left blank or
#                             not defined at all, in which case, it means there
#                             no source files in the module <mod> that are required
#                             to be compiled in the application build stage.
#
ifeq ($(serdes_diag_component_make_include), )

# under other list
drvserdes_diag_BOARDLIST       = am65xx_evm am65xx_idk 
drvserdes_diag_SOCLIST         = am65xx 
drvserdes_diag_am574x_CORELIST = c66x a15_0 ipu1_0
drvserdes_diag_am572x_CORELIST = c66x a15_0 ipu1_0
drvserdes_diag_am571x_CORELIST = c66x a15_0 ipu1_0
drvserdes_diag_am437x_CORELIST = a9host
drvserdes_diag_am335x_CORELIST = a8host
drvserdes_diag_k2g_CORELIST    = c66x a15_0
drvserdes_diag_dra72x_CORELIST = a15_0 ipu1_0
drvserdes_diag_dra75x_CORELIST = a15_0 ipu1_0
drvserdes_diag_dra78x_CORELIST = ipu1_0
drvserdes_diag_omapl137_CORELIST = c674x arm9_0
drvserdes_diag_omapl138_CORELIST = c674x arm9_0
drvserdes_diag_c6657_CORELIST  = c66x
drvserdes_diag_am65xx_CORELIST = mcu1_0

############################
# serdes_diag package
# List of components included under serdes_diag lib
# The components included here are built and will be part of serdes_diag lib
############################
serdes_diag_LIB_LIST = serdes_diag
drvserdes_diag_LIB_LIST = $(serdes_diag_LIB_LIST)

############################
# serdes_diag examples
# List of examples under serdes_diag
# All the tests mentioned in list are built when test target is called
# List below all examples for allowed values
############################
serdes_diag_EXAMPLE_LIST =  serdes_diag_BER_app serdes_diag_EYE_app
drvserdes_diag_EXAMPLE_LIST = $(serdes_diag_EXAMPLE_LIST)

#
# serdes_diag Modules
#

# serdes_diag LIB
serdes_diag_COMP_LIST = serdes_diag
serdes_diag_RELPATH = ti/diag/serdes_diag
serdes_diag_PATH = $(PDK_SERDES_DIAG_COMP_PATH)
serdes_diag_LIBNAME = ti.diag.serdes_diag
serdes_diag_LIBPATH = $(serdes_diag_PATH)/lib
serdes_diag_OBJPATH = $(serdes_diag_RELPATH)/serdes_diag
serdes_diag_MAKEFILE = -f build/makefile.mk
serdes_diag_BOARD_DEPENDENCY = no
serdes_diag_CORE_DEPENDENCY = no
serdes_diag_SOC_DEPENDENCY = yes
serdes_diag_PKG_LIST = serdes_diag
serdes_diag_INCLUDE = $(serdes_diag_PATH)
serdes_diag_SOCLIST = $(drvserdes_diag_SOCLIST)
serdes_diag_$(SOC)_CORELIST = $(drvserdes_diag_$(SOC)_CORELIST)
export serdes_diag_COMP_LIST
export serdes_diag_RELPATH
export serdes_diag_PATH
export serdes_diag_LIBNAME
export serdes_diag_LIBPATH
export serdes_diag_OBJPATH
export serdes_diag_MAKEFILE
export serdes_diag_BOARD_DEPENDENCY
export serdes_diag_CORE_DEPENDENCY
export serdes_diag_SOC_DEPENDENCY
export serdes_diag_PKG_LIST
export serdes_diag_INCLUDE
export serdes_diag_SOCLIST
export serdes_diag_$(SOC)_CORELIST

# serdes_diag unit test app BER
serdes_diag_BER_app_COMP_LIST = serdes_diag_BER_app
serdes_diag_BER_app_RELPATH = ti/diag/serdes_diag/test/am65xx
serdes_diag_BER_app_PATH = $(PDK_SERDES_DIAG_COMP_PATH)/test/am65xx
serdes_diag_BER_app_BOARD_DEPENDENCY = yes
serdes_diag_BER_app_CORE_DEPENDENCY = no
serdes_diag_BER_app_XDC_CONFIGURO = no
serdes_diag_BER_app_MAKEFILE = -f makefile_BER BAREMETAL=yes
serdes_diag_BER_app_PKG_LIST = serdes_diag_BER_app
serdes_diag_BER_app_INCLUDE = $(serdes_diag_BER_app_PATH)
serdes_diag_BER_app_BOARDLIST = am65xx_evm am65xx_idk
serdes_diag_BER_app_$(SOC)_CORELIST = $(drvserdes_diag_$(SOC)_CORELIST)

export serdes_diag_BER_app_COMP_LIST
export serdes_diag_BER_app_$(SOC)_CORELIST
export serdes_diag_BER_app_RELPATH
export serdes_diag_BER_app_PATH
export serdes_diag_BER_app_BOARD_DEPENDENCY
export serdes_diag_BER_app_CORE_DEPENDENCY
export serdes_diag_BER_app_XDC_CONFIGURO
export serdes_diag_BER_app_MAKEFILE
export serdes_diag_BER_app_PKG_LIST
export serdes_diag_BER_app_INCLUDE
export serdes_diag_BER_app_BOARDLIST

# serdes_diag unit test app EYE
serdes_diag_EYE_app_COMP_LIST = serdes_diag_EYE_app
serdes_diag_EYE_app_RELPATH = ti/diag/serdes_diag/test/am65xx
serdes_diag_EYE_app_PATH = $(PDK_SERDES_DIAG_COMP_PATH)/test/am65xx
serdes_diag_EYE_app_BOARD_DEPENDENCY = yes
serdes_diag_EYE_app_CORE_DEPENDENCY = no
serdes_diag_EYE_app_XDC_CONFIGURO = no
serdes_diag_EYE_app_MAKEFILE = -f makefile_EYE BAREMETAL=yes
serdes_diag_EYE_app_PKG_LIST = serdes_diag_EYE_app
serdes_diag_EYE_app_INCLUDE = $(serdes_diag_EYE_app_PATH)
serdes_diag_EYE_app_BOARDLIST = am65xx_evm am65xx_idk
serdes_diag_EYE_app_$(SOC)_CORELIST = $(drvserdes_diag_$(SOC)_CORELIST)

export serdes_diag_EYE_app_COMP_LIST
export serdes_diag_EYE_app_RELPATH
export serdes_diag_EYE_app_PATH
export serdes_diag_EYE_app_BOARD_DEPENDENCY
export serdes_diag_EYE_app_CORE_DEPENDENCY
export serdes_diag_EYE_app_XDC_CONFIGURO
export serdes_diag_EYE_app_MAKEFILE
export serdes_diag_EYE_app_PKG_LIST
export serdes_diag_EYE_app_INCLUDE
export serdes_diag_EYE_app_BOARDLIST
export serdes_diag_EYE_app_$(SOC)_CORELIST



export drvserdes_diag_LIB_LIST
export serdes_diag_LIB_LIST
export serdes_diag_EXAMPLE_LIST
export drvserdes_diag_EXAMPLE_LIST

serdes_diag_component_make_include := 1
endif
