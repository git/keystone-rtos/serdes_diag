/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the Serdes Diag library
 *
 * Copyright (C) 2019, Texas Instruments, Inc.
 *****************************************************************************/

package ti.diag.serdes_diag[1,0,0,13] {
    module Settings;
}
